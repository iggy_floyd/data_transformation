'''
	Logistic Regressions: 
        taken from http://blog.yhathq.com/posts/logistic-regression-and-python.html
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
from sklearn import preprocessing
import matplotlib.pyplot as pl
import prettytable
import statsmodels.api as sm
from sklearn import linear_model
from sklearn.cross_validation import cross_val_score
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn.metrics import classification_report
import statsmodels.api as sm
import Normalizer
import seaborn as  sns
import interpolate_histogram as ih
import sklearn_roc_curves as skroc
import scipy
from sklearn.metrics import roc_curve, auc,precision_recall_curve


class logistic_regression_twoclass(object):
 '''Does logistic analysis'''

 def __init__(self,df,features):

     self.df = df
     self.features = features
     # manually add the intercept
     self.df['intercept'] = 1.0 
     
     pass
    
 def  plot_dataframe(self): 
     self.df.hist()
     pl.show()
     return
 
 def transform_dataframe(self):
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]
         
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 #self.df[name_feature].apply(lambda x: transformator(x))

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 #print self.df[name_feature]
                 #transformator.fit(self.df[name_feature].as_matrix()) 
                 #print data
                 def f(x):
                     print x                  
                     print transformator.transform(x)
                     return transformator.transform(x)

                 #for x in self.df[name_feature]: f(x)
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
                 #self.df[name_feature] = self.df[name_feature] .apply(lambda x: f([x]))
    return self           

 def fit(self,target):
     self.train_cols=[feature[0] for feature in self.features]
     self.train_cols.remove(target)
     if 'intercept' in self.df: self.train_cols+=['intercept']
     print self.train_cols
     self.logit = sm.Logit(self.df[target],self.df[self.train_cols])
     self.fit_result = self.logit.fit()
     
     # find coefficents
     # get predictions from logit
     self.train_features = [ x for x in self.train_cols]
     if 'intercept' in self.df: self.train_features.remove('intercept')
     return self

 def create_coefficients(self,target='probas_OK'):
     self.linear_mode = target + ' ~ '
     for feature in self.train_features:
        self.linear_mode+=feature+' +'
     self.linear_mode=self.linear_mode[:-2]
     
     grouped = self.df.groupby('status')
     import statsmodels.formula.api as sf
     self.coefs = grouped.apply(lambda d: sf.ols(self.linear_mode, d).fit().params).T     
     self.coefs=zip(self.train_features,self.coefs.loc[:,0].as_matrix())
     return self

     
 def summary(self):
     print self.fit_result.summary()
     print '---'*20
     print self.fit_result.conf_int()
     print '---'*20
     params = self.fit_result.params
     conf = self.fit_result.conf_int()
     conf['OR'] = params
     conf.columns = ['2.5%', '97.5%', 'OR']
     print np.exp(conf) 
 
 
 def create_probas_pdf(self,target):

     self.df['probas'] = self.fit_result.predict(self.df[[feature for feature in self.train_cols]])
     self.proba_vals_class1=self.df.loc[self.df[self.df[target] == 1].index,['probas']].as_matrix()
     self.proba_vals_class0=self.df.loc[self.df[self.df[target] == 0].index,['probas']].as_matrix()
     counts1, bins1 = np.histogram(self.proba_vals_class1, bins=50)
     counts0, bins0 = np.histogram(self.proba_vals_class0, bins=50)
     
     hi_1 = ih.histogram_interpolate(counts1,bins1).create_interpolation()
     hi_0 = ih.histogram_interpolate(counts0,bins0).create_interpolation()
     self.pdfs_provider = ih.TwoClassPLEProbability(hi_1,hi_0)
     
     return self








def isolate_and_plot(combos,value_name,variable_name,group_name,label,transformer=None):

 if group_name is None:
    grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name], aggfunc=np.mean)
 else:   
    grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name, group_name], aggfunc=np.mean)


 colors = 'rbgyrbgy'
 if (group_name):
  for col in sorted(combos[group_name].unique()):
   plt_data = grouped.ix[grouped.index.get_level_values(1)==col]
   pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],  color=colors[int(col)])
 else:
   plt_data = grouped.ix[grouped.index.get_level_values(0)]
   pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],   color=colors[int(0)])
 if (transformer is not None):     
     print transformer
     print variable_name
     #print plt_data.index.get_level_values(0)
     #print [ transformer.inverse_transform([i]) for i in plt_data.index.get_level_values(0) ]
     tcks = sorted(plt_data.index.get_level_values(0))
     lbls =  [ transformer.inverse_transform([i])[0] for i in tcks ]
     #pl.xticks( plt_data.index.get_level_values(0), [ transformer.inverse_transform([i])[0] for i in plt_data.index.get_level_values(0) ],rotation=60)
     pl.xticks( tcks,lbls,rotation=60)
     
 pl.xlabel(variable_name)
 pl.ylabel("P(%s)"%value_name)
 if (group_name): pl.legend([str(i) for i in range(int(combos[group_name].min()),int(combos[group_name].max()+1))], loc='upper left', title=group_name)
 pl.title("Prob(%s) isolating %s variable and group %s"%(value_name,variable_name,group_name))

 pl.savefig('plots/'+label+'.png')
 pl.show()
 
 
 
def sklearn_roc_plot(cv,scores,y,filename,weights=None):
 '''   Example  of ROC curve for the Classifier with help of the sklearn '''

 # assumed fraction of frauds in all dataset
 fraud_frac=0.01

 # plot ROC with help of sklearn
 mean_tpr = 0.0
 mean_fpr = np.linspace(0, 1, 100)
 for i, (train, test) in enumerate(cv):    
    
    # apply weights to the sample
    def applyweigt(arr,y): 
        return [ y[1] if x >0 else y[0] for x in arr ]
        
    weights = np.apply_along_axis(applyweigt, 0, y[test],[fraud_frac,1.-fraud_frac]) if (weights is None) else \
        np.apply_along_axis(applyweigt, 0, y[test],weights)

    if (i==0):   label="\n N(OK)= %d, \n N(NOT_OK) = %d "% ((1.-fraud_frac)*len(y[test][y[test]>0]),(fraud_frac)*len(y[test][y[test]==0]))
    else: label=""

    # Compute ROC curve and area the curve
    roc=skroc.sklearn_roc_curves(scores[test],y[test]).calculateROC(weights=weights).createPlotROC("PLE > cut",
        #label="fold %d, TP+FP = %d,FN+TN = %d "% (i,len(y[test][y[test]>0]),len(y[test][y[test]==0]))
         label=label
        )
    
    
    #calculate the mean ROC
    mean_tpr += scipy.interp(mean_fpr, roc.fpr, roc.tpr)
    mean_tpr[0] = 0.0

    
   
    # plot discriminant values
    if (i>0): continue
    for k,x in enumerate(roc.thresholds.tolist()):
        if k%(len(roc.thresholds.tolist())/10 if len(roc.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([roc.fpr[k]],[roc.tpr[k]],'o',color='r')
            pl.text(roc.fpr[k]+0.01,roc.tpr[k]-0.01,'%3.2f'%x,color='r')
            #print "threshold = %f, tpr= %f, fpr = %f"%(x,roc.tpr[k],roc.fpr[k])

 

 # plot mean ROC
 mean_tpr /= len(cv)
 mean_tpr[-1] = 1.0
 mean_auc = auc(mean_fpr, mean_tpr)
 pl.plot(mean_fpr, mean_tpr, 'k--',
         label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

 # plot random ROC
 pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')

 pl.legend(loc="lower right")
 pl.savefig('plots/'+filename+'.png')
 pl.show()
 return 


def sklearn_precision_plot(cv,scores,y,filename,weights=None):
 ''' plot recall vs precision plot '''

# assumed fraction of frauds in all dataset
 fraud_frac=0.01 
 for i, (train, test) in enumerate(cv):    

    # Compute Precisions curve
    def applyweigt(arr,y):        
        return [ y[1] if x >0 else y[0] for x in arr ]
    weights = np.apply_along_axis(applyweigt, 0, y[test],[fraud_frac,1.-fraud_frac]) if (weights is None) else \
        np.apply_along_axis(applyweigt, 0, y[test],weights)


    prec=skroc.sklearn_roc_curves(scores[test],y[test]).calculatePrecision(weights=weights).createPlotPrecision("PLE > cut",
        label="fold %d"% (i)         
        )
    # plot discriminant values
    if (not (i==1)): continue
    for k,x in enumerate(prec.thresholds.tolist()):
        if k%(len(prec.thresholds.tolist())/10 if len(prec.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([prec.recall[k]],[prec.prec[k]],'o',color='r')
            pl.text(prec.recall[k]+0.01,prec.prec[k]-0.01,'%3.2f'%x,color='r')
 
 pl.savefig('plots/'+filename+'.png') 
 pl.show()
 
 return 

if __name__ == '__main__':


 # read command line options and arguments
 import argparse
 parser = argparse.ArgumentParser(description='Two-Class Logistic Regressor')
 
 parser.add_argument('-cl1','--cl1', help='plot a distribution of probabilities for the model of class 1',default=None, required=False, action='store_true')
 parser.add_argument('-cl2','--cl2', help='plot a distribution of probabilities for the model of class 2',default=None, required=False, action='store_true')
 parser.add_argument('-clall','--clall', help='plot  distributions of probabilities for the model of both classes',default=None, required=False, action='store_true')
 parser.add_argument('-dc','--dynamiccut', help='use a dynamic cut in the classifier',default=None, required=False, action='store_true')
 parser.add_argument('-w','--weights', help='apply weights to equlize anssembles of different classes',default=None, required=False, action='store_true')
 parser.add_argument('-coef','--coef', help='plot a coefficients of the logistic model',default=None, required=False, action='store_true')
 parser.add_argument('-pd','--perfomance_diagram', help='plot a perfomance diagram',default=None, required=False, action='store_true')
 parser.add_argument('-roc','--perfomance_ROC', help='plot a ROC curve',default=None, required=False, action='store_true')
 parser.add_argument('-prec','--perfomance_precision', help='plot a recall vs precision curve',default=None, required=False, action='store_true')
 parser.add_argument('-linv','--label_inverse', help='use inverse transformation for xticks of plotted variable distributions',default=None, required=False, action='store_true')
 parser.add_argument('-vars','--vars', help='plot distributions of variables',default=None, required=False, action='store_true')
 
 parser.add_argument('-all','--all', help='make all plots',default=None, required=False, action='store_true')
 args = parser.parse_args()


 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    # ('airport_dist',None),
 #   ('airport_dist',preprocessing.MinMaxScaler()),    
    ('LD_booker_payer',None),
   # ('booker_depairport_dist',preprocessing.MinMaxScaler()),        
    #('booker_depairport_dist',Normalizer.NormalizerWrapper(type='normal')),        

    #('traveller_avg_age',preprocessing.MinMaxScaler()), 
    #('traveller_first_age',Normalizer.NormalizerWrapper(type='normal')), # not bad test
    #('booker_age',Normalizer.NormalizerWrapper(type='normal')), # not bad test
    #('booker_age',preprocessing.StandardScaler()), # bad 

    
    
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    #('traveller_avg_age',Normalizer.NormalizerWrapper(type='median')), 
    
    #('traveller_avg_age',None), # uncomment to plot it
    #('traveller_avg_age',preprocessing.MinMaxScaler()), 
    
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    #('booker_valid_addr',Normalizer.NormalizerWrapper(type='decrease')),
    #('booker_valid_addr',preprocessing.MinMaxScaler()), 
    
    #('booker_valid_addr',preprocessing.LabelEncoder()),  # uncomment it to plot
    
   # ('booker_arrairport_dist',preprocessing.MinMaxScaler()), 
    #('booker_arrairport_dist',Normalizer.NormalizerWrapper(type='normal')), 

    ('booking_period',preprocessing.MinMaxScaler()), # !!!

    #('booking_period',Normalizer.NormalizerWrapper(type='normal')), 

    ('LD_booker_all_travellers',None), # test !!!
    
   # ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('LD_booker_first_traveller',None), # bad
    #
    
    #('product_typeOfFlight',preprocessing.LabelEncoder()), # perhaps bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!


    #('product_airline',Normalizer.NormalizerWrapper(type='normal')),
    
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    #('product_arrivalAirport',Normalizer.NormalizerWrapper(type='decrease')),
    #('product_arrivalAirport',preprocessing.LabelEncoder()),
    
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    #('product_departureAirport',Normalizer.NormalizerWrapper(type='decrease')),
    
    # ('booker_email_provider',Normalizer.NormalizerWrapper(type='normal')),
    #('booker_email_country',Normalizer.NormalizerWrapper(type='normal')),
    
    #('is_birthday',Normalizer.NormalizerWrapper(type='normal')), # bad
    
    #('booker_country',Normalizer.NormalizerWrapper(type='normal')), # bad
    #('booker_country',preprocessing.LabelEncoder()), # bad
    
    #('payer_binCountry',Normalizer.NormalizerWrapper(type='normal')), # bad

    
    
    ]
    
    
 # Class doing logistic regression: 
 lg=logistic_regression_twoclass(dataframe[ [feature[0] for feature in features] ],features)
 # plot numerical data
 #lg.plot_dataframe()

 # transform to numerical data and plot again
 lg.transform_dataframe()
 #lg.plot_dataframe()
 
 #linear fit of the logit function
 lg.fit('status')
 
 # print the summary
 lg.summary()
 
 # create pdfs of the twoclass model
 lg.create_probas_pdf('status')
 
 def proba_maker(x,proba_provider):
     return (proba_provider.getProbability(x['probas'])[0],proba_provider.getProbability(x['probas'])[1])
     
 
 lg.df['probas_OK'] = lg.df.apply(lambda x: proba_maker(x,lg.pdfs_provider)[0],axis=1)
 lg.df['probas_NOTOK'] = lg.df.apply(lambda x: proba_maker(x,lg.pdfs_provider)[1],axis=1)
 
 print lg.df.loc[lg.df['status'] == 1].head() 
 print lg.df.loc[lg.df['status'] == 0].head() 
 

 
 
 # plot probabilites
 sns.set_context("poster")
 prefix = 'logistic_regression_twoclass_'
 label = 'probas_all_distr'
 b, g, r, p = sns.color_palette("muted", 4)
 if ('clall' in vars(args) and vars(args)['clall']) or ('all' in vars(args) and vars(args)['all']):  
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK for OK',hist=False, axlabel='probability')
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK for OK',hist=False, axlabel='probability')
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK for NOT_OK',hist=False, axlabel='probability')
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK for NOT_OK',hist=False, axlabel='probability') 
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 
  
 # plot probabilites
 if ('cl1' in vars(args) and vars(args)['cl1']) or ('all' in vars(args) and vars(args)['all']):  
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_ok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 # plot probabilites
 if ('cl2' in vars(args) and vars(args)['cl2']) or ('all' in vars(args) and vars(args)['all']):  
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_notok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
 
 
 
 # make a coefff plot and create coefficients
 lg.create_coefficients(target='probas_OK')
 print lg.linear_mode
 if ('coef' in vars(args) and vars(args)['coef']) or ('all' in vars(args) and vars(args)['all']):  
    sns.coefplot(lg.linear_mode, lg.df,'status')
    print lg.coefs
    label = 'probas_ok_coeffs'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
 
 
 # create logistic two-class classifier
 if ('dynamiccut' in vars(args) and vars(args)['dynamiccut']):   dynamic_cut=True
 else:   dynamic_cut=False
 
 if (dynamic_cut): prefix +='dynamic_'
 if (dynamic_cut): basic_cut = 0.2
 else: basic_cut= 0.5

 def logit_classifier(x,basic_cut,coeffs,dynamic_cut):
     if dynamic_cut:
        return (int(x['probas_OK'] >= basic_cut + sum([coef[1]*x[coef[0]] for coef in coeffs])),basic_cut + sum([coef[1]*x[coef[0]] for coef in coeffs]))
    
     return (int(x['probas_OK'] >= basic_cut),basic_cut)
 
 # perform predictions
 lg.df['predict'] = lg.df.apply(lambda x: logit_classifier(x,basic_cut,lg.coefs,dynamic_cut)[0],axis=1)
 lg.df['logit_cut'] = lg.df.apply(lambda x: logit_classifier(x,basic_cut,lg.coefs,dynamic_cut)[1],axis=1)

 # calculate weights (needed for correct type-1/type-2 error results)
 weights = {

        0:float(len(lg.df.loc[lg.df[lg.df['status'] == 1].index,['status']]))/float(len(lg.df.loc[lg.df[lg.df['status'] == 0].index,['status']])),
        1:1
    }

 #print "weights", weights
 if ('all' in vars(args) and vars(args)['all']):  
    sns.distplot(lg.df['logit_cut'].as_matrix(),color=b,label='logit_cut',hist=False, axlabel='logit_cut')
    label = 'logit_cut_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()


 # estimate perfomance: type1/ type2 errors etc
 pos = lg.df.loc[lg.df[lg.df['predict'] == 1].index,['status']]
 true_pos = pos.loc[pos[pos['status'] == 1].index]
 false_neg = pos.loc[pos[pos['status'] == 0].index]

 neg = lg.df.loc[lg.df[lg.df['predict'] == 0].index,['status']]
 true_neg = neg.loc[neg[neg['status'] == 0].index]
 false_pos = neg.loc[neg[neg['status'] == 1].index]

 
 if ('weights' in vars(args) and vars(args)['weights']):   apply_weights=True
 else:   apply_weights=False
 
 
 if (not apply_weights):
    print "found rates", {    'true_pos':len(true_pos),
            'false_pos':len(false_pos),
            'true_neg':len(true_neg),
            'false_neg':len(false_neg),
 
        }

    performance = {
 
        'prec':float( len(true_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'type1':float( len(false_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'tpr':float( len(true_pos) )/float(  len(true_pos) +   len(false_neg) ),
        'npv':float( len(true_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'type2':float( len(false_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'fpr':float( len(false_pos) )/float(  len(true_neg) +   len(false_pos) ),
 
    }
 else:
     print "found rates", {    'true_pos':len(true_pos)*weights[1],
            'false_pos':len(false_pos)*weights[1],
            'true_neg':len(true_neg)*weights[0],
            'false_neg':len(false_neg)*weights[0],
 
        }

     performance = {
 
        'prec':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'type1':float( len(false_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'tpr':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_neg)*weights[0] ),
        'npv':float( len(true_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'type2':float( len(false_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'fpr':float( len(false_pos)*weights[1] )/float(  len(true_neg)*weights[0] +   len(false_pos)*weights[1] ),
 
    }
 
 #print performance
 if ('perfomance_diagram' in vars(args) and vars(args)['perfomance_diagram']) or ('all' in vars(args) and vars(args)['all']):  
    sns.barplot(np.array(performance.keys()),np.array(performance.values()),ci=None, palette="Paired", hline=.1)
    label = 'perfomance'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
 
 # make some performance plot: ROC 
 vals_ok=lg.df.loc[lg.df[lg.df['status'] == 1].index,lg.train_features].as_matrix().tolist()
 vals_notok =lg.df.loc[lg.df[lg.df['status'] == 0].index,lg.train_features].as_matrix().tolist()
 
 # create a test sample
 vals =  np.array(vals_ok + vals_notok)
 y = np.array([ 1 if i<len(vals_ok) else 0   for i, val in enumerate(vals)]) 
 cv = StratifiedKFold(y, n_folds=5) # cross-validation  tool
 scores = np.array(
         lg.df.loc[lg.df[lg.df['status'] == 1].index,'probas_OK'].as_matrix().tolist() +
         lg.df.loc[lg.df[lg.df['status'] == 0].index,'probas_OK'].as_matrix().tolist() 
         )
 logit=lambda  x: proba_maker(x,lg.pdfs_provider)[0]
 
 # plot ROC
 if ('perfomance_ROC' in vars(args) and vars(args)['perfomance_ROC']) or ('all' in vars(args) and vars(args)['all']):  
    label = 'perfomance_ROC'
    sklearn_roc_plot(cv,scores,y,prefix+label,[weights[0],weights[1]] if apply_weights else None)
 
 # plot precision 
 if ('perfomance_precision' in vars(args) and vars(args)['perfomance_precision']) or ('all' in vars(args) and vars(args)['all']):  
    label = 'perfomance_precision'
    sklearn_precision_plot(cv,scores,y,prefix+label,[weights[0],weights[1]] if apply_weights else None)
 
 
 # plot dependence of the probas_OK on some parameter
 if ('vars' in vars(args) and vars(args)['vars']) or ('all' in vars(args) and vars(args)['all']):   do_plots_vs_variables=True
 else:   do_plots_vs_variables=False
 
 if ('label_inverse' in vars(args) and vars(args)['label_inverse']):   dolabelinverse=True
 else:   dolabelinverse=False
 
 
 if (not do_plots_vs_variables ): sys.exit(0)
 
 
 if (dolabelinverse): pl.figure(figsize=(15,8))
 transformers = dict((x[0],x[1]) for x in lg.features)
 
 for feature in lg.train_features:
  if (dolabelinverse):    transformer = transformers[feature]
  else:   transformer = None
  label = 'probas_ok_status_'+feature+'_distr'
  isolate_and_plot(lg.df,'probas_OK',feature,'status',prefix+label,transformer)       
  label = 'probas_ok_'+feature+'_distr'
  isolate_and_plot(lg.df,'probas_OK',feature,None,prefix+label,transformer)
 
 
 
