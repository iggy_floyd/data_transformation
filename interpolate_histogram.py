
'''
    How to get the probability density function from the histogram in python?
    
'''


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np

import scipy.interpolate as sci
import random
import matplotlib.pyplot as pl
import pandas as pd
import PDE
import projective_likelihood


class histogram_interpolate(object):
    
    def __init__(self,bins_content,bins):
        self.bins = bins
        self.bins_content = bins_content
        self.inverse = False

    def create_inverse_interpolation(self,kind='cubic'):
        self.x = np.cumsum(self.bins_content)*1./np.sum(self.bins_content) # normalization to unity
        
    
        self.bin_width = (self.bins[1:] - self.bins[:-1])
        
        
        if (len(self.bins)>len(self.bins_content)):
            self.y = np.array(self.bins[range(len(self.x)+1)][1:])
        else: 
            self.y = np.array(self.bins[range(len(self.x))])
        
        self.interpolate = sci.interp1d(self.x,self.y,kind=kind)
        #self.interpolate = sci.interp1d(self.x,self.y)
        self.inverse  = True
        return self

    def get_interploated(self,x):
        if (x>max(self.x)): x=max(self.x)
        if (x<min(self.x)): x=min(self.x)
        return self.interpolate(x)

    def create_interpolation(self,kind='cubic'):
        
        if (len(self.bins)>len(self.bins_content)):
            self.x = np.array(self.bins[0:-1])
        else:
            self.x = np.array(self.bins)
        
        self.y =  np.array(self.bins_content)*1./np.sum(self.bins_content)
        
        self.interpolate = sci.interp1d(self.x,self.y,kind=kind)
        #self.interpolate = sci.interp1d(self.x,self.y)
        
        self.inverse  = False
        return self


    def generate_random(self,count):
        if (not self.inverse): 
            print 'First make an inverse interpolation'
            return 
        b = np.zeros(count)
        for i in range(count):
            u = random.uniform( min(self.x),max(self.x) )            
            b[i] = self.interpolate( u )
            
        return b

    


    def plot(self):
        
        if (self.inverse):
            print 'First make an interpolation'
            return 
        
        xnew = np.linspace(min(self.x), max(self.x) , 100*len(self.bins))        
        print 'sanity check: integral of the interpolated histogram',np.sum(self.y)
        print 'sanity check: integral of the original histogram',np.sum(self.bins_content)
        pl.plot(self.x,self.y,label='an original histogram scaled to unity')
        pl.plot(xnew,self.interpolate(xnew),label='an interpolated histogram')
        pl.legend(loc='upper right')
        pl.show()
        return



class ple_creator(object):
    def __init__(self,dataframe=None):
        
         # Set-up some parameters and loading dataframe
         process_name='fps_fraud_classification'
         print "reading from ",process_name+'_transformed.csv',"...."
         if (dataframe is None): self.dataframe = pd.read_csv(process_name+'_transformed.csv')
         else: self.dataframe = dataframe

         self.pde_categ = [
                    ('airport_dist',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
                    ('LD_booker_payer',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
                    ('booking_period',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
                    ('product_price',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
                    ('booker_valid_addr',{'typepde':'categoricalnum'},lambda x: int(x)),
                    ('product_typeOfFlight',{'typepde':'categoricaltext'},lambda x: str(x)),
                    ('product_airline',{'typepde':'categoricaltext'},lambda x: str(x)),
                    ('product_arrivalAirport',{'typepde':'categoricaltext'},lambda x: str(x)),
                    ('product_departureAirport',{'typepde':'categoricaltext'},lambda x: str(x))
            ]
    def create_ple(self):
        # create PDE
            self.pdes_ok = []   
            self.pdes_notok = []
            self.vals_ok  = []
            self.vals_notok = []
            self.labels = []
            for categ in  self.pde_categ:
                self.labels +=[categ[0]]
                self.pdes_ok+=[PDE.PDE(categ[0],self.dataframe,**categ[1]).makePDE('status','CHECKED_OK')]
                self.pdes_notok+=[PDE.PDE(categ[0],self.dataframe,**categ[1]).makePDE('status','CHECKED_NOT_OK')]
                self.vals_ok +=[self.pdes_ok[-1].vals]
                self.vals_notok += [self.pdes_notok[-1].vals]
         
            # create PLE
            self.proj=projective_likelihood.projective_likelihood(self.pdes_ok,self.pdes_notok)
            return self
        
        
    def create_ple_pdf(self,nbins): 
         vals_ok = zip(*self.vals_ok)
         vals_notok = zip(*self.vals_notok)
         y_ok=map(lambda x:self.proj.getVal(x),vals_ok) 
         y_notok=map(lambda x:self.proj.getVal(x),vals_notok)
         self.ple_vals_ok = y_ok
         self.ple_vals_notok = y_notok

         # create a pdf for 'OK' status
         nx, xbins, ptchs = pl.hist(y_ok, bins=nbins,normed=True)
         pl.clf() 
         nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
         width = xbins[1] - xbins[0] # Width of each bin.
         xbins_ok =  xbins         
         self.x_ok=xbins[:-1]
         self.y_ok=nx_frac

        # create a pdf for 'NOTOK' status
         nx, xbins, ptchs = pl.hist(y_notok, bins=nbins,normed=True)
         pl.clf() 
         nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
         width = xbins[1] - xbins[0] # Width of each bin.
         xbins_notok =  xbins         
         self.x_notok=xbins[:-1]
         self.y_notok=nx_frac

         return self





class TwoClassPLEProbability(object):
    
    
    def __init__(self,interpolator1,interpolator2):
        self.interpolator1 = interpolator1.create_interpolation()
        self.interpolator2 = interpolator2.create_interpolation()
        return
    
    def getProbability(self,ple_value):
        val1 = self.interpolator1.get_interploated(ple_value)
        val2 = self.interpolator2.get_interploated(ple_value)

        val1 = min(1.0,max(0.,val1))
        val2 = min(1.0,max(0.,val2))
        return (val1/(val1+val2),val2/(val1+val2)) if (val1+val2) != 0. else (0.,0.)
    

if __name__ == '__main__':
    import seaborn as  sns
    sns.set_context("poster")
    # Test 1 : create normal distribution and test it
    normal =  np.random.normal(size=1000)
    #counts, bins = np.histogram(normal, bins=10, density=True)
    counts, bins = np.histogram(normal, bins=10)
    
    hi = histogram_interpolate(counts,bins).create_interpolation()
    hi.plot()
 
    # Test 2: generate a random value according to the histogram
    histo = hi.create_inverse_interpolation().generate_random(10000)
    pl.hist(histo,bins=bins)
    pl.show()
    
 
    # Test 3: create PLE and interpolate it
    ple = ple_creator().create_ple().create_ple_pdf(50)
    hi_ok=histogram_interpolate(ple.y_ok,ple.x_ok).create_interpolation()
    hi_ok.plot()
    print "hi_ok is %.3f at %.3f"%(hi_ok.get_interploated(0.99),0.99)
    
    
    hi_notok=histogram_interpolate(ple.y_notok,ple.x_notok).create_interpolation()
    hi_notok.plot()
    print "hi_notok is %.3f at %.3f"%(hi_notok.get_interploated(0.99),0.99)
    
    # Test 4: generate PLE for OK and NOTOK statuses
    histo_ok = hi_ok.create_inverse_interpolation().generate_random(10000)
    histo_notok = hi_notok.create_inverse_interpolation().generate_random(10000)
    pl.hist(histo_ok,bins=ple.x_ok,color='b')
    pl.hist(histo_notok,bins=ple.x_notok,color='r')
    pl.show()
    
    
    # Test 5: calculate the probabilities for 2-class ple
    twoclassple = TwoClassPLEProbability(hi_ok,hi_notok)
    for ple_val in ple.x_ok:
        print 'ple value = %.3f, Proba(OK)=%.3f,Proba(NOTOK)=%.3f'%(ple_val,twoclassple.getProbability(ple_val)[0],twoclassple.getProbability(ple_val)[1])
    
    
    # Test 6: plot the ratio of probabilites in the range of ple output:[0.1,0.90]
    ples = np.linspace(0.1,0.9,100)
    ratios = np.array([twoclassple.getProbability(ple)[0] for ple in ples])/ np.array([twoclassple.getProbability(ple)[1] for ple in ples])
    pl.plot(ples,ratios)
    pl.show()
