'''
	Logistic Regressions: 
        taken from http://blog.yhathq.com/posts/logistic-regression-and-python.html
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
from sklearn import preprocessing
import matplotlib.pyplot as pl
import prettytable
import statsmodels.api as sm
from sklearn import linear_model
from sklearn.cross_validation import cross_val_score
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
import statsmodels.api as sm
import Normalizer
import seaborn as  sns


class logistic_regression_v2(object):
 '''Does logistic analysis'''

 def __init__(self,df,features):

     self.df = df
     self.features = features
     # manually add the intercept
     self.df['intercept'] = 1.0 
     
     pass
    
 def  plot_dataframe(self): 
     self.df.hist()
     pl.show()
     return
 
 def transform_dataframe(self):
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]
         
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 #self.df[name_feature].apply(lambda x: transformator(x))

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 #print self.df[name_feature]
                 #transformator.fit(self.df[name_feature].as_matrix()) 
                 #print data
                 def f(x):
                     print x                  
                     print transformator.transform(x)
                     return transformator.transform(x)

                 #for x in self.df[name_feature]: f(x)
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
                 #self.df[name_feature] = self.df[name_feature] .apply(lambda x: f([x]))
    return self           

 def fit(self,target,doCoeff=False):
     self.train_cols=[feature[0] for feature in features]
     self.train_cols.remove(target)
     if 'intercept' in self.df: self.train_cols+=['intercept']
     print self.train_cols
     self.logit = sm.Logit(self.df[target],self.df[self.train_cols])
     self.fit_result = self.logit.fit()
     
     # find coefficents
     # get predictions from logit
     train_features = [ x for x in self.train_cols]
     if 'intercept' in self.df: train_features.remove('intercept')
     if (not doCoeff): 
        self.coefs = [ (feature,0.) for feature in train_features]
        return self
     
     self.df['probas_OK'] = self.fit_result.predict(self.df[train_cols[:-2]])
  
     
     linear_mode = 'probas_OK ~ '
     for feature in train_features:
        linear_mode+=feature+' +'
     linear_mode=linear_mode[:-2]
     
     grouped = self.df.groupby('status')
     import statsmodels.formula.api as sf
     self.coefs = grouped.apply(lambda d: sf.ols(linear_mode, d).fit().params).T
     self.coefs = self.coefs.ix[1:] # remove intercept
     self.coefs=zip(train_features,coefs.loc[:,0].as_matrix())
     return self

 def summary(self):
     print self.fit_result.summary()
     print '---'*20
     print self.fit_result.conf_int()
     print '---'*20
     params = self.fit_result.params
     conf = self.fit_result.conf_int()
     conf['OR'] = params
     conf.columns = ['2.5%', '97.5%', 'OR']
     print np.exp(conf) 
     
 '''
 def logistic_classifier(self,basic_cut,do_dynamic_cut,df):
     if do_dynamic_cut:
        return (int(x['probas_OK'] >= basic_cut + sum([coef[1]*x[coef[0]] for coef in self.coeffs])),basic_cut + sum([coef[1]*x[coef[0]] for coef in self.coeffs]))
    
     return (int(x['probas_OK'] >= basic_cut),basic_cut)

 def predict(self,df):
    return self.clf.predict(df)

 def predict_proba(self,df):     
    return  self.clf.predict_proba(df)
 '''






def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out







def isolate_and_plot(combos,value_name,variable_name,group_name,label):

 if group_name is None:
    grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name], aggfunc=np.mean)
 else:   
    grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name, group_name], aggfunc=np.mean)


 colors = 'rbgyrbgy'
 if (group_name):
  for col in sorted(combos[group_name].unique()):
   plt_data = grouped.ix[grouped.index.get_level_values(1)==col]
   pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],
   color=colors[int(col)])
 else:
   plt_data = grouped.ix[grouped.index.get_level_values(0)]
   pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],
   color=colors[int(0)])
 pl.xlabel(variable_name)
 pl.ylabel("P(%s)"%value_name)
 if (group_name): pl.legend([str(i) for i in range(int(combos[group_name].min()),int(combos[group_name].max()+1))], loc='upper left', title=group_name)
 pl.title("Prob(%s) isolating %s variable and group %s"%(value_name,variable_name,group_name))

 pl.savefig('plots/'+label+'.png')
 pl.show()
 
 
 
if __name__ == '__main__':

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    # ('airport_dist',None),
 #   ('airport_dist',preprocessing.MinMaxScaler()),    
    ('LD_booker_payer',None),
   # ('booker_depairport_dist',preprocessing.MinMaxScaler()),        
    #('booker_depairport_dist',Normalizer.NormalizerWrapper(type='normal')),        

    #('traveller_avg_age',preprocessing.MinMaxScaler()), 
    #('traveller_first_age',Normalizer.NormalizerWrapper(type='normal')), # not bad test
    #('booker_age',Normalizer.NormalizerWrapper(type='normal')), # not bad test
    #('booker_age',preprocessing.StandardScaler()), # bad 

    
    
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    #('traveller_avg_age',Normalizer.NormalizerWrapper(type='median')), 
    #('traveller_avg_age',None), 
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    #('booker_valid_addr',Normalizer.NormalizerWrapper(type='decrease')),
    #('booker_valid_addr',preprocessing.MinMaxScaler()), 
    #('booker_valid_addr',preprocessing.LabelEncoder()), 
    
   # ('booker_arrairport_dist',preprocessing.MinMaxScaler()), 
    #('booker_arrairport_dist',Normalizer.NormalizerWrapper(type='normal')), 

    ('booking_period',preprocessing.MinMaxScaler()), # !!!

    #('booking_period',Normalizer.NormalizerWrapper(type='normal')), 

    ('LD_booker_all_travellers',None), # test !!!
    
   # ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('LD_booker_first_traveller',None), # bad
    #
    
    #('product_typeOfFlight',preprocessing.LabelEncoder()), # perhaps bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!


    #('product_airline',Normalizer.NormalizerWrapper(type='normal')),
    
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    #('product_arrivalAirport',Normalizer.NormalizerWrapper(type='decrease')),
    #('product_arrivalAirport',preprocessing.LabelEncoder()),
    
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    #('product_departureAirport',Normalizer.NormalizerWrapper(type='decrease')),
    
    # ('booker_email_provider',Normalizer.NormalizerWrapper(type='normal')),
    #('booker_email_country',Normalizer.NormalizerWrapper(type='normal')),
    
    #('is_birthday',Normalizer.NormalizerWrapper(type='normal')), # bad
    
    #('booker_country',Normalizer.NormalizerWrapper(type='normal')), # bad
    #('booker_country',preprocessing.LabelEncoder()), # bad
    
    #('payer_binCountry',Normalizer.NormalizerWrapper(type='normal')), # bad

    
    
    ]
    
    
 # Class doing logistic regression: 
 lg=logistic_regression_v2(dataframe[ [feature[0] for feature in features] ],features)
 # plot numerical data
 #lg.plot_dataframe()

 # transform to numerical data and plot again
 lg.transform_dataframe()
 #lg.plot_dataframe()
 
 #linear fit of the logit function
 lg.fit('status')
 
 # print the summary
 lg.summary()
 
 # get predictions from logit
 print [feature[0] for feature in features[1:]]+['intercept']
 print [feature[0] for feature in features]
 if 'intercept' in lg.df:
    lg.df['probas_OK'] = lg.fit_result.predict(lg.df[[feature[0] for feature in features[1:]]+['intercept'] ])
 else:
    lg.df['probas_OK'] = lg.fit_result.predict(lg.df[[feature[0] for feature in features[1:]] ])
 #lg.df['probas_OK'] = lg.fit_result.predict(lg.df[[feature[0] for feature in features] ])
 
 # print a few of them
 print lg.df.loc[lg.df['status'] == 1].head() 
 print lg.df.loc[lg.df['status'] == 0].head() 
 
 # find features used to fit the model
 train_features = [ x for x in lg.train_cols]
 if 'intercept' in lg.df: train_features.remove('intercept')
 
 
 # plot probabilites
 prefix = 'logistic_regression_'
 sns.set_context("poster")
 b, g, r, p = sns.color_palette("muted", 4)
 sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel='probability')
 sns.distplot(lg.df.loc[lg.df[lg.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, axlabel='probability')

 label = 'probas_ok_distr'
 pl.savefig('plots/'+prefix+label+'.png')
 pl.show()

 #sys.exit(1)
 # plot dependence of the probas_OK on some parameter

 for feature in train_features:
  label = 'probas_ok_status_'+feature+'_distr'
  isolate_and_plot(lg.df,'probas_OK',feature,'status',prefix+label) 
  label = 'probas_ok_'+feature+'_distr'
  isolate_and_plot(lg.df,'probas_OK',feature,None,prefix+label)
  



 # plot probabilities as a linear functions features
 for feature in train_features:
    sns.lmplot(feature,'probas_OK',lg.df,hue='status',size=8, aspect=2)    
    label = 'probas_ok_scatter_'+feature+'_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    

 # make a coefff plot
 linear_mode = 'probas_OK ~ '
 for feature in train_features:
     linear_mode+=feature+' +'
 linear_mode=linear_mode[:-2]
 print linear_mode
 sns.coefplot(linear_mode, lg.df,'status')
 
 # this is a way how to get this coefficients as arrays
 # taken from  /usr/local/lib/python2.7/dist-packages/seaborn/linearmodels.py
 
 grouped = lg.df.groupby('status')
 import statsmodels.formula.api as sf
 coefs = grouped.apply(lambda d: sf.ols(linear_mode, d).fit().params).T
 coefs = coefs.ix[1:] # remove intercept
 print coefs
 print 'coefficient from CHECKED_NOT_OK for %s is %f'%(train_features[0],coefs.loc[train_features[0],0])
 print 'coefficient from CHECKED_OK for %s is %f'%(train_features[0],coefs.loc[train_features[0],1])
 #print zip(train_features,coefs.loc[:,0].as_matrix()) 
 label = 'probas_ok_coeffs'
 pl.savefig('plots/'+prefix+label+'.png')
 pl.show()
 
 
 dynamic_cut=True
 if (dynamic_cut): basic_cut = 0.90 
 else: basic_cut= 0.61

 def logit_classifier(x,basic_cut,coeffs,dynamic_cut):
     if dynamic_cut:
        return (int(x['probas_OK'] >= basic_cut + sum([coef[1]*x[coef[0]] for coef in coeffs])),basic_cut + sum([coef[1]*x[coef[0]] for coef in coeffs]))
    
     return (int(x['probas_OK'] >= basic_cut),basic_cut)
    
 # check interval of basic_cut [0.4,0.6]
 #basic_cut = 0.90 # 0.61
 
 lg.df['predict'] = lg.df.apply(lambda x: logit_classifier(x,basic_cut,zip(train_features,coefs.loc[:,0].as_matrix()),dynamic_cut)[0],axis=1)
 lg.df['logit_cut'] = lg.df.apply(lambda x: logit_classifier(x,basic_cut,zip(train_features,coefs.loc[:,0].as_matrix()),dynamic_cut)[1],axis=1)

 print lg.df[ ['status','predict']].head()

 #calculate weights    
 
 weights = {

        0:float(len(lg.df.loc[lg.df[lg.df['status'] == 1].index,['status']]))/float(len(lg.df.loc[lg.df[lg.df['status'] == 0].index,['status']])),
        1:1
    }

 print "weights", weights
 sns.distplot(lg.df['logit_cut'].as_matrix(),color=b,label='logit_cut',hist=False, axlabel='logit_cut')
 label = 'logit_cut_distr'
 pl.savefig('plots/'+prefix+label+'.png')
 pl.show()

 
 # estimate perfomance: type1/ type2 errors etc
 pos = lg.df.loc[lg.df[lg.df['predict'] == 1].index,['status']]
 true_pos = pos.loc[pos[pos['status'] == 1].index]
 false_neg = pos.loc[pos[pos['status'] == 0].index]

 neg = lg.df.loc[lg.df[lg.df['predict'] == 0].index,['status']]
 true_neg = neg.loc[neg[neg['status'] == 0].index]
 false_pos = neg.loc[neg[neg['status'] == 1].index]

 
 apply_weights = True
 if (not apply_weights):
    print "found rates", {    'true_pos':len(true_pos),
            'false_pos':len(false_pos),
            'true_neg':len(true_neg),
            'false_neg':len(false_neg),
 
        }

    performance = {
 
        'prec':float( len(true_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'type1':float( len(false_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'tpr':float( len(true_pos) )/float(  len(true_pos) +   len(false_neg) ),
        'npv':float( len(true_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'type2':float( len(false_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'fpr':float( len(false_pos) )/float(  len(true_neg) +   len(false_pos) ),
 
    }
 else:
     print "found rates", {    'true_pos':len(true_pos)*weights[1],
            'false_pos':len(false_pos)*weights[1],
            'true_neg':len(true_neg)*weights[0],
            'false_neg':len(false_neg)*weights[0],
 
        }

     performance = {
 
        'prec':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'type1':float( len(false_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'tpr':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_neg)*weights[0] ),
        'npv':float( len(true_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'type2':float( len(false_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'fpr':float( len(false_pos)*weights[1] )/float(  len(true_neg)*weights[0] +   len(false_pos)*weights[1] ),
 
    }
 
 print performance
 #print len(lg.df.loc[lg.df[lg.df['status'] == 0].index,['status']])
 #print len(lg.df.loc[lg.df[lg.df['status'] == 1].index,['status']])
 
 # plot perfomance
 
 
 sns.barplot(np.array(performance.keys()),np.array(performance.values()),ci=None, palette="Paired", hline=.1)
 label = 'perfomance'
 pl.savefig('plots/'+prefix+label+'.png')
 pl.show()
 sys.exit()
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
