#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 17, 2015 1:06:35 PM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn import cross_validation    
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
import Normalizer
import sklearn_roc_curves as skroc
import sklearn_optimizer
import time
import os.path
from sklearn.externals import joblib
import interpolate_histogram as ih
import binary_transformer as bt

import scipy
from sklearn.metrics import roc_curve, auc,precision_recall_curve

from sklearn.ensemble import BaggingClassifier

import dill



# my own classifiers placed here
import nb_gaussian_twoclass_sklearn as NBgauss



class BaggingUnitWrapper(object):
    def __init__(self,df,clf,features,target):
      self.df=df
      self.features = features   
      self.clf = clf
      self.transformators={}
      self.train_cols = []
      self.types = {}
      
      #prepare transformation list
      for feature in self.features:
         name_feature = feature[0]
         if name_feature == target: continue
         
         self.train_cols += [name_feature]
         transformator = feature[1]    
         self.types[name_feature] = type(transformator)
         if transformator:
             if ('function' in str(type(transformator))):                      
                 self.transformators[name_feature] =('func',transformator)

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix().tolist()) 
                 self.transformators[name_feature] =('class',transformator)
         else:        
                self.transformators[name_feature] =('None',lambda x: x)
      return

    def fit(self, X, y):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(self.train_cols):            

            transformation = self.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                #transformation[1](X[i].tolist())                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]

        X=np.array(transformed).T # transpose it back
        self.clf.fit(X,y)
        self.data_type_ = type(X)
        return self

    def predict(self, X):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(self.train_cols):            

            transformation = self.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            

        X=np.array(transformed).T # transpose it back
        return self.clf.predict(X)
        
        
    def get_params(self,deep=True):
        return self.clf.get_params(deep)

    def set_params(self,**params):
        return self.clf.set_params(params)


class WrapperGaussianNB(GaussianNB):
    
    
    @staticmethod
    def myinit(df,features,target):
        WrapperGaussianNB.df = df
        WrapperGaussianNB.features = features
        WrapperGaussianNB.target = target
        WrapperGaussianNB.transformators={}
        WrapperGaussianNB.train_cols = []
        
        #prepare transformation list
        for feature in WrapperGaussianNB.features:
         name_feature = feature[0]
         if name_feature == target: continue
         
         WrapperGaussianNB.train_cols += [name_feature]
         transformator = feature[1]    
        
         if transformator:
             if ('function' in str(type(transformator))):                      
                 WrapperGaussianNB.transformators[name_feature] =('func',transformator)

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(WrapperGaussianNB.df[name_feature].as_matrix().tolist()) 
                 WrapperGaussianNB.transformators[name_feature] =('class',transformator)
         else:        
                WrapperGaussianNB.transformators[name_feature] =('None',lambda x: x)
        return
        
        
    def fit(self, X, y):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(WrapperGaussianNB.train_cols):            

            transformation = WrapperGaussianNB.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                #transformation[1](X[i].tolist())                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]

        X=np.array(transformed).T # transpose it back
        super(WrapperGaussianNB, self).fit(X,y)
        
        return self
    
    def predict(self, X):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(WrapperGaussianNB.train_cols):            

            transformation = WrapperGaussianNB.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            

        X=np.array(transformed).T # transpose it back
        return  super(WrapperGaussianNB, self).predict(X)
    
    def predict_proba(self, X):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(WrapperGaussianNB.train_cols):            

            transformation = WrapperGaussianNB.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            

        X=np.array(transformed).T # transpose it back
        return  super(WrapperGaussianNB, self).predict_proba(X)  
        
    def __init__(self,**kwargs):
        super(WrapperGaussianNB, self).__init__(**kwargs)
        

from sklearn.ensemble import BaggingClassifier

if __name__ == "__main__":
  
# Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    

#    ('airport_dist',preprocessing.MinMaxScaler()),    
    ]
 dataframe['status'] =    dataframe['status'].apply(lambda x: features[0][1](x))
 
 # Test 1: create one unit for bagging
 bagging_unit=BaggingUnitWrapper(dataframe[ [feature[0] for feature in features] ],GaussianNB(),features,'status')      
 
 # Test 2: try to fit it
 X_train,X_test,y_train, y_test = cross_validation.train_test_split(bagging_unit.df[bagging_unit.train_cols].as_matrix(), bagging_unit.df['status'].as_matrix(), test_size=0.1, random_state=0)    
 bagging_unit.fit(X_train,y_train)
 
 # Test 3: try to predict several entries from test data samples
 print bagging_unit.predict(X_test[:20]),'*'*5, y_test[:20], '*'*5,  bagging_unit.predict(X_test[:20]) == y_test[:20]
 
 
 
 
 # Test 4: try to use BaggingClassifier
 from sklearn.utils import check_random_state
 rng = check_random_state(0)
 params={"max_samples": 0.5,"bootstrap":False,"n_estimators":200}
 
 WrapperGaussianNB.myinit(dataframe[ [feature[0] for feature in features] ],features,'status')
 wnb=WrapperGaussianNB()
 
 BC=BaggingClassifier(base_estimator=wnb,random_state=rng,**params).fit(X_train, y_train)
 print 'score of the BC=',BC.score(X_test,y_test)
 print BC.predict(X_test[:20]),'*'*5, y_test[:20], '*'*5,  BC.predict(X_test[:20]) == y_test[:20]
 
 
 
 wnb.fit(X_train,y_train)
 print wnb.predict(X_test[:20]),'*'*5, y_test[:20], '*'*5,  wnb.predict(X_test[:20]) == y_test[:20]
 print wnb.score(X_test,y_test)
 