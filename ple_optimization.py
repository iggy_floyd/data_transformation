'''
	Class to optimize PLE
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
from sklearn.metrics import roc_curve, auc,precision_recall_curve
from sklearn.cross_validation import StratifiedKFold,KFold
import PDE
import projective_likelihood
import sys
import scipy
import multiprocessing as mp
import threading
import Queue
import time

class ple_optimization(object):
 '''
    

 '''

 def __init__(self,dataframe,pde_categ):

        self.scores=None
        self.trues=None
        self.fpr = None
        self.tpr = None
        self.thresholds = None
        self.roc_auc = None
        
    
        # create PDE
        self.pdes_ok = []   
        self.pdes_notok = []
        self.vals_ok  = []
        self.vals_notok = []
        for categ in  pde_categ:
            self.pdes_ok+=[PDE.PDE(categ[0],dataframe,**categ[1]).makePDE('status','CHECKED_OK')]
            self.pdes_notok+=[PDE.PDE(categ[0],dataframe,**categ[1]).makePDE('status','CHECKED_NOT_OK')]
            self.vals_ok +=[self.pdes_ok[-1].vals]
            self.vals_notok += [self.pdes_notok[-1].vals]
           
        return

 def  train_and_test(self):

        # create a test sample
        self.vals_ok = zip(*self.vals_ok)
        self.vals_notok = zip(*self.vals_notok)
        self.vals =  np.array(self.vals_ok + self.vals_notok)
        self.y = np.array([ 1 if i<len(vals_ok) else 0   for i, val in enumerate(self.vals)]) 
        self.cv = StratifiedKFold(self.y, n_folds=2) # cross-validation  tool
         
        # create PLE
        self.proj=projective_likelihood.projective_likelihood(self.pdes_ok,self.pdes_notok)

        # get results
        for i, (train, test) in enumerate(self.cv):    
            # compute pde output for this trial
            self.scores  = np.array(map(lambda x:  self.proj.getVal(x), [[ pde_categ[j][2](x) for j,x in enumerate(self.vals[t].tolist())] for t in test]  ))        
            self.trues = self.y[test]
            self.calculateAreaROC()
            if i==0: break            
        return self.roc_auc


 def calculateAreaROC(self):
        self.fpr,self.tpr,self.thresholds =  roc_curve(self.trues, self.scores)
        self.roc_auc= auc(self.fpr, self.tpr)
        return self.roc_auc
 
 

class O(object):

        @staticmethod
        def calc(a):
            import os            
            return (a,os.getpid())



#lock=mp.Lock()
#res=[]
class ple_optimization2(object):

        
        @staticmethod
        ##def calc(dataframe,pde_categ):
        def calc(dataframe,pde_categ):
            # create PDE
            pdes_ok = []   
            pdes_notok = []
            vals_ok  = []
            vals_notok = []
            labels = []
            for categ in  pde_categ:
                labels +=[categ[0]]
                pdes_ok+=[PDE.PDE(categ[0],dataframe,**categ[1]).makePDE('status','CHECKED_OK')]
                pdes_notok+=[PDE.PDE(categ[0],dataframe,**categ[1]).makePDE('status','CHECKED_NOT_OK')]
                vals_ok +=[pdes_ok[-1].vals]
                vals_notok += [pdes_notok[-1].vals]

            # create a test sample
            
            vals_ok = zip(*vals_ok)
            vals_notok = zip(*vals_notok)
            vals =  np.array(vals_ok + vals_notok)
            y = np.array([ 1 if i<len(vals_ok) else 0   for i, val in enumerate(vals)]) 
            cv = StratifiedKFold(y, n_folds=2) # cross-validation  tool
         
            # create PLE
            proj=projective_likelihood.projective_likelihood(pdes_ok,pdes_notok)
                            
            # get results
            for i, (train, test) in enumerate(cv):    
                # compute pde output for this trial
                scores  = np.array(map(lambda x:  proj.getVal(x), [[ pde_categ[j][2](x) for j,x in enumerate(vals[t].tolist())] for t in test]  ))        
                fpr,tpr,_ =  roc_curve(y[test], scores)
                roc_auc= auc(fpr, tpr)
                if i==0: break  
            
            #lock.acquire()            
            #res+=[(roc_auc,labels)]
            #print "add result",res
            #lock.release()
            print roc_auc,labels
            return (roc_auc,labels)


class myThread (threading.Thread):
    #def __init__(self, threadID, dataframe,pde_categ):
    def __init__(self, threadID,q,lock):

        threading.Thread.__init__(self)
        self.threadID = threadID
        #self.dataframe = dataframe
        #self.pde_categ = pde_categ
        self.q=q
        self.lock=lock
    
    def run(self):
        print "Starting " + str(self.threadID)
        # Get lock to synchronize threads
        #threadLock.acquire()
        #print_time(self.name, self.counter, 3)
        #return ple_optimization2.calc( self.dataframe,self.pde_categ)
        process_data(self.q,self.lock)
        # Free lock to release next thread
       #threadLock.release()

def process_data(q,lock):          
        lock.acquire()
        if not q.empty():
            dataframe,pde_categ = q.get()            
            
            #print dataframe
            print pde_categ
            ple_optimization2.calc(dataframe,pde_categ)            
            lock.release()
        else:
            lock.release()
        time.sleep(1)


def all_combinations(stuff):
    from itertools import chain, combinations
    def all_subsets(ss):
        return chain(*map(lambda x: combinations(ss, x), range(0, len(ss)+1)))
    return  all_subsets(stuff)

def test(x):
    return len(x)


def fun(f,q_in,q_out):
    while True:
        i,x = q_in.get()
        if i is None:
            break
        q_out.put((i,f(x)))

def parmap(f, X, nprocs = mp.cpu_count()):
    q_in   = mp.Queue(1)
    q_out  = mp.Queue()

    proc = [mp.Process(target=fun,args=(f,q_in,q_out)) for _ in range(nprocs)]
    for p in proc:
        p.daemon = True
        p.start()

    sent = [q_in.put((i,x)) for i,x in enumerate(X)]
    [q_in.put((None,None)) for _ in range(nprocs)]
    res = [q_out.get() for _ in range(len(sent))]

    [p.join() for p in proc]

    return [x for i,x in sorted(res)]


import dill

def run_dill_encoded(what):
    fun, args = dill.loads(what)
    return fun(*args)

def apply_async(pool, fun, args):
    return pool.apply_async(run_dill_encoded, (dill.dumps((fun, args)),))


if __name__ == '__main__':


 # read command line options and arguments
 
 '''
 import argparse
 parser = argparse.ArgumentParser(description='Plot performance curves')
 
 parser.add_argument('-sr','--sroc', help='make a ROC plot with help of the sklearn',default=None, required=False, action='store_true')
 parser.add_argument('-sp','--sprecision', help='make a Recall vs Precision plot with help of the sklearn',default=None, required=False, action='store_true')
 parser.add_argument('-pr','--pyroc', help='make a ROC plot with help of the PyROC',default=None, required=False, action='store_true')
 parser.add_argument('-pp','--precision', help='make a precision plot with help of the PyROC',default=None, required=False, action='store_true')
 args = parser.parse_args()
 '''
 


 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 pde_categ = [
    ('airport_dist',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('LD_booker_payer',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('booking_period',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('product_price',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('booker_valid_addr',{'typepde':'categoricalnum'},lambda x: int(x)),
    ('product_typeOfFlight',{'typepde':'categoricaltext'},lambda x: str(x)),
    ('product_airline',{'typepde':'categoricaltext'},lambda x: str(x)),
    ('product_arrivalAirport',{'typepde':'categoricaltext'},lambda x: str(x)),
    ('product_departureAirport',{'typepde':'categoricaltext'},lambda x: str(x))
 ]

 #all_combinations(pde_categ)
 #pde_categ = [0,1,2,3,4]
 #print [a for a in all_combinations(pde_categ)]

 


 b = [a for a in all_combinations(pde_categ)]

 '''
 import ThreadPool
 # 1) Init a Thread pool with the desired number of threads
 pool = ThreadPool.ThreadPool(4)

 for pde_categ in b[1:]:
        # 2) Add the task to the queue
        pool.add_task(ple_optimization2.calc,dataframe,pde_categ )
        

 # 3) Wait for completion
 pool.wait_completion()

 #queueLock = threading.Lock()
 #workQueue = Queue.Queue(10)
 #threads = [myThread(i,dataframe,x) for i,x in enumerate(b[1:10])]
 #threads = [myThread(i,workQueue,queueLock) for i,x in enumerate(b[1:10])]
 #for t in threads:
 #   t.start()

 # Fill the queue
 #queueLock.acquire()
 #for _pde_categ in b[1:10]:
 #   workQueue.put((dataframe,_pde_categ))
 #queueLock.release()


 # Wait for queue to empty
 #while not workQueue.empty():
 #   pass


 #for t in threads:
 #   t.join()

 sys.exit(0)
 '''


 pool = mp.Pool(processes=5)
 
 #results = [apply_async(pool, ple_optimization2.calc,(dataframe,x)) for x in all_combinations(pde_categ)]
 results = [apply_async(pool, ple_optimization2.calc,(dataframe,x)) for x in b[-6:-1]]
 

 for job in results:
        print job.get()
 #print res


 

 