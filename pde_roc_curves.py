'''
	Class to Plot different Performance plots of PDE
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl



class pde_roc_curves(object):
 '''
    See
    http://en.wikipedia.org/wiki/Precision_and_recall	
 '''

 def __init__(self,vals_ok,vals_not_ok):

        self.vals_ok = vals_ok
        self.vals_not_ok = vals_not_ok        
	pass

 def precision(self,nsteps=100,type=True):
    ''' PPV  positive predictive value or (1.0 - Type-1) error rate regarding to Status 'OK' (type=True)
        or to Status 'NOT_OK' (type=False)
    '''

    precis=[]
    
    if (type):
        xmin = min(map(lambda x: x[0], self.vals_ok ) )
        xmax = max(map(lambda x: x[0], self.vals_ok ) )
        vals = self.vals_ok

    else:
        xmin = min(map(lambda x: x[0], self.vals_not_ok ) )
        xmax = max(map(lambda x: x[0], self.vals_not_ok ) )
        vals = self.vals_not_ok

    grid = np.linspace(xmin,xmax,nsteps).tolist()

    for x in grid:        
        precis +=[sum(map(lambda y: y[1],filter(lambda y: y[0]<=x,vals)))]
    return(grid,[ min(1,max(0,x)) for x in precis ])


 def Type1(self,nsteps=100,type=True):
    (grid,precis) = self.precision(nsteps,type)
    type1=1.0-np.array(precis)
    return (grid,type1.tolist())



 def NPV(self,nsteps=100,type=True):
    ''' negative predictive value  or (1.0 - Type-2) error rate regarding to Status 'OK' (type=True)
        or to Status 'NOT_OK' (type=False)
    '''

    NPV=[]
    
    if (type):
        xmin = min(map(lambda x: x[0], self.vals_ok ) )
        xmax = max(map(lambda x: x[0], self.vals_ok ) )
        vals = self.vals_ok

    else:
        xmin = min(map(lambda x: x[0], self.vals_not_ok ) )
        xmax = max(map(lambda x: x[0], self.vals_not_ok ) )
        vals = self.vals_not_ok

    grid = np.linspace(xmin,xmax,nsteps).tolist()

    for x in grid:        
        NPV +=[sum(map(lambda y: y[1],filter(lambda y: y[0]>x,vals)))]
    return(grid,[ min(1,max(0,x)) for x in NPV ])


    for x in grid:
        precis +=[sum(filter(lambda y: y>x,vals))]
    
    return(grid,precis)
        

 def Type2(self,nsteps=100,type=True):
    (grid,precis) = self.NPV(nsteps,type)
    type2=1.0-np.array(precis)
    return (grid,type2.tolist())


 def ROC(self,nsteps=100,type=True):
    ''' TPR  (true positive rate) vs FPR (false positive rate) regarding to Status 'OK' (type=True)
        or to Status 'NOT_OK' (type=False)

        http://www.google.de/imgres?imgurl=http://www.medcalc.org/manual/_help/images/roc_intro3.png&imgrefurl=http://www.medcalc.org/manual/roc-curves.php&h=279&w=289&tbnid=MRKmpRMSEJSVMM:&zoom=1&tbnh=90&tbnw=93&usg=__ssEk91t2tu5qiJOdaFoqPljIXRY=&docid=KJg1hQI-pnK4UM&sa=X&ei=g1X0VLrtDMfePYvEgOgD&sqi=2&ved=0CDsQ9QEwAw
    '''

    ROC=[]
    TPR = []
    FPR = []

    if (type):  
        xmin_pos = min(map(lambda x: x[0], self.vals_ok ) )
        xmax_pos = max(map(lambda x: x[0], self.vals_ok ) )
        vals_pos = self.vals_ok
        xmin_neg = min(map(lambda x: x[0], self.vals_not_ok ) )
        xmax_neg = max(map(lambda x: x[0], self.vals_not_ok ) )
        vals_neg = self.vals_not_ok

    else:

        xmin_neg = min(map(lambda x: x[0], self.vals_ok ) )
        xmax_neg = max(map(lambda x: x[0], self.vals_ok ) )
        vals_neg = self.vals_ok
        xmin_pos = min(map(lambda x: x[0], self.vals_not_ok ) )
        xmax_pos = max(map(lambda x: x[0], self.vals_not_ok ) )
        vals_pos = self.vals_not_ok

    
    grid_pos = np.linspace(xmin_pos,xmax_pos,nsteps).tolist()
    grid_neg = np.linspace(xmin_neg,xmax_neg,nsteps).tolist()


    

    for x in grid_pos:
        _tp = sum(map(lambda y: y[1],filter(lambda y: y[0]<=x,vals_pos)))
        _fn = sum(map(lambda y: y[1],filter(lambda y: y[0]<=x,vals_neg)))
        _fp = sum(map(lambda y: y[1],filter(lambda y: y[0]>x,vals_pos)))
        _tn = sum(map(lambda y: y[1],filter(lambda y: y[0]>x,vals_neg)))

#        _tp = sum(filter(lambda y: y<=x,vals_pos))
#        _fn = sum(filter(lambda y: y<=x,vals_neg))
#        _fp = sum(filter(lambda y: y>x,vals_pos))
#        _tn = sum(filter(lambda y: y>x,vals_neg))

        _tpr = _tp/(_tp+_fn) if ( (_tp+_fn) != 0 ) else 1.
        _fpr = _fp/(_fp+_tn) if ( (_fp+_tn) != 0 ) else 1.

        ROC +=[(min(1,max(0,_tpr)),min(1,max(0,_fpr)))]
        TPR +=[min(1,max(0,_tpr))]
        FPR +=[min(1,max(0,_fpr))]
    #return ROC
    return TPR,FPR

    
 

    


 