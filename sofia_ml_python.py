#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 20, 2015 3:09:55 PM$"

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np

import pandas as pd
from sklearn import preprocessing
from sklearn import cross_validation 
import Normalizer

from subprocess import Popen,PIPE

import os

import binary_transformer as bt

class sofia_ml_python(object):
    
    def __init__(self,df,features,target,path_to_sofia=None, doBinary=True,
    train_options='--learner_type pegasos --loop_type balanced-stochastic --lambda 0.1 \
    --iterations 100000 --dimensionality 1600    --eta_type basic'):
        
        self.path_to_sofia=path_to_sofia
        if (self.path_to_sofia is None or len(self.path_to_sofia ) == 0):
            raise ValueError('please, provide the correct path to the Sofia-ml package')
        self.exe = 'sofia-ml'
        self.training_file = '_tmp_train.txt'
        self.test_file = '_tmp_test.txt'
        self.model_file = '_tmp_model.txt'
        self.result_file = '_tmp_result.txt'
        self.prefix='_tmp'
        self.df=df
        self.target=target
        self.features=features
        self.train_options = train_options
        self.doBinary = doBinary

        print self.path_to_sofia
        
        
    def prepare_train_test_dataset(self,bins=100,transformer=None): 
        if (transformer is None): 
            self.transformer = bt.binary_transformer(self.df,self.features,bins=bins).transform_dataframe()
        
            if (self.doBinary):  
                self.transformer =  self.transformer.digitization(self.target).transform_dataframe_binary(self.target)
            else: 
                self.transformer =  self.transformer.create_dataframe_ordinary(self.target)
        
        else:
            self.transformer =    transformer 
        
        
        if (self.doBinary):              
            self.transformer.save_test_train_to_file_binary(self.prefix,test_size=0.1)
        else:         
            self.transformer.save_test_train_to_file_ordinary(self.prefix,test_size=0.1)
        

    def fit(self,X,y):
        cmd = self.path_to_sofia + self.exe + ' '+self.train_options + ' --training_file ' + self.training_file + ' --model_out '+self.model_file 
        cmd=cmd.split()
        (out,err) = Popen(cmd, stdout=PIPE).communicate()
        print out
        return self

    def predict(self,X):
        
        
        if (X is not None): 
            y=np.array([1 for i in range(X.shape[0]) ])
            
        
        
        if (self.doBinary):
            if (X is not None): 
                self.transformer.transform_and_save_binary(self.prefix,X,y)
                
        else:    
            if (X is not None): self.transformer.transform_and_save_ordinary(self.prefix,X,y)
        
        cmd = self.path_to_sofia + self.exe +  ' --model_in '+self.model_file + ' --test_file ' + self.test_file + ' --results_file ' + self.result_file
        print cmd
        
        cmd=cmd.split()
        (out,err) = Popen(cmd, stdout=PIPE).communicate()
        print out
        
        #os.system(cmd)
        
        #import subprocess
        #return_code = subprocess.call(cmd, shell=True) 
        
        result=[line.strip() for line in open(self.result_file)]
        return [[float(res.split('\t')[0]),float(res.split('\t')[1])] for res in result ]
        


 # Store current working directory
_pwd=os.path.dirname(os.path.abspath(__file__))
if __name__ == "__main__":


# Set-up some parameters and loading dataframe
 
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else -1),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('airport_dist',preprocessing.MinMaxScaler()),   
    ('booker_email_provider',Normalizer.NormalizerWrapper(type='normal')),   
    ('booker_email_country',Normalizer.NormalizerWrapper(type='normal')),   
    ('product_airline',Normalizer.NormalizerWrapper(type='normal')),   
    
    


    ]

 from ensemble import WrapperEnsemble
 dataframe3 = pd.read_csv(process_name+'_transformed.csv')
 df_binary3=dataframe3[ [feature[0] for feature in features] ]
 
 WrapperEnsemble.myinit(dataframe[ [feature[0] for feature in features] ],features,'status')
 
 
 
 
 dataframe2 = pd.read_csv(process_name+'_transformed.csv')
 df_binary2=dataframe2[ [feature[0] for feature in features] ]

 
 
 
 # Test1 the wrapper for sofia_ml
 #sml_python=sofia_ml_python(df_binary2,features,'status',path_to_sofia=_pwd+'/sofia-ml/',doBinary=False)
 #sml_python=sofia_ml_python(df_binary2,features,'status',path_to_sofia=_pwd+'/sofia-ml/',doBinary=True)
 
 train_options='--learner_type logreg-pegasos --loop_type  roc --lambda 0.1 --eta_type pegasos  --iterations 100000 '
 
 sml_python=sofia_ml_python(df_binary2,features,'status',path_to_sofia=_pwd+'/sofia-ml/',doBinary=True,train_options=train_options)
 binary_transformer=bt.binary_transformer(df_binary3,features,bins=20).transform_dataframe().digitization('status').transform_dataframe_binary('status')
 sml_python.prepare_train_test_dataset(bins=50,transformer=binary_transformer)
 #sml_python.prepare_train_test_dataset(bins=100,transformer=None)
 sml_python.fit(None,None)
 #print sml_python.predict(np.array([]))
 res= sml_python.predict(None)
 res = [ np.sign(r).tolist() for r in res ]
 X=None
 y=np.array([ r[1] for r in res])
 preds=np.array([ r[0] for r in res])
 
 def accuracy_v2(clf,X,y,preds=None):
    if (preds is None): preds = clf.predict(X)
    y=y.flatten()
    print 'prediction accuracy: %.4f' % (1 - (1. / len(y) * sum( preds != y )))
    true=(preds == y)
    poses=(y == 1)
    negs=(y == -1)
    print "True OK: %.4f"%(sum(true*poses)*1./sum(poses))
    print "True NOT_OK: %.4f"%(sum(true*negs)*1./sum(negs))
 
 accuracy_v2(sml_python,X,y,preds)
 sys.exit()
 
 # Test2 the binary model
 wrapper=WrapperEnsemble(sml_python,predict_function=None,predict_proba_function=None,fit_function=None,binary_transformer=binary_transformer)
 #wrapper.fit(dataframe[WrapperEnsemble.train_cols].as_matrix(),dataframe[['status']].as_matrix())
 print wrapper.predict(dataframe[WrapperEnsemble.train_cols].as_matrix()[:20])