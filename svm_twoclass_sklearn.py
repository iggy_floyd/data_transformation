#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 14, 2015 12:02:30 PM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd
from sklearn import svm
from sklearn import cross_validation    
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
import Normalizer
import sklearn_roc_curves as skroc
import sklearn_optimizer
import time
import os.path
from sklearn.externals import joblib
import cPickle

import dill
    

def run_dill_encoded(what):
        fun, args = dill.loads(what)
        return fun(*args)

def apply_async(pool, fun, args):        
        return pool.apply_async(run_dill_encoded, (dill.dumps((fun, args)),))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    



from multiprocessing import Pool, cpu_count

def applyParallel(dfGrouped, func):
        #p=Pool(cpu_count())
        p=Pool(process=2)
        ret_list = p.map(func, [(group,name) for name, group in dfGrouped])
        return pd.concat(ret_list)    


class svm_twoclass_sklearn(object):
    
 def __init__(self,df,features):

     self.df = df
     self.features = features
     self.binary_enc = None
     self.probability=False
     pass
    
 def plot_dataframe(self): 
     self.df.hist()
     pl.show()
     return
 
 def transform_dataframe(self):
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]
         
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 #self.df[name_feature].apply(lambda x: transformator(x))

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 #print self.df[name_feature]
                 #transformator.fit(self.df[name_feature].as_matrix()) 
                 #print data
                 def f(x):
                     print x                  
                     print transformator.transform(x)
                     return transformator.transform(x)

                 #for x in self.df[name_feature]: f(x)
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
                 #self.df[name_feature] = self.df[name_feature] .apply(lambda x: f([x]))
    return self  
 
 def label_encoders(self,input_variable_names):
    input_variable_types = map(lambda x: 'string' if 'object' in self.df[x].ftypes else self.df[x].ftypes,input_variable_names )
    self.labelencoders = {}
    for i,x in enumerate(input_variable_types):
    ## here transform string label to numeric labels
        if 'string' in x:
            transformer=preprocessing.LabelEncoder()
        else:    
            transformer=preprocessing.MinMaxScaler()
        values =  map(lambda x: x if x!=np.nan else 'NaN' ,self.df[input_variable_names[i]].get_values())
        self.labelencoders[input_variable_names[i]] =transformer
        self.labelencoders[input_variable_names[i]].fit(values)
        self.df[input_variable_names[i]] =  self.df[input_variable_names[i]].apply(lambda x: self.labelencoders[input_variable_names[i]].transform([x])[0])
        

    return self
 
 def transform_dataframe_binary(self,target):
    binary_train_col=[]   
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]
         if (name_feature in target):
            if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
          
             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
          
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
         else: 
             binary_train_col +=[name_feature]
    self.label_encoders(binary_train_col)
    
    self.binary_enc = preprocessing.OneHotEncoder().fit(self.df[binary_train_col].as_matrix().tolist())

    count=[0]
    def binary_transform(x,a):
        
        _data=[]
        
        for name_feature in binary_train_col:
             _data += [x[name_feature]]
        
        print a[0]
        a[0]+=1
        #print 'transform=',self.binary_enc.transform([_data]).toarray().tolist()[0]
        return   self.binary_enc.transform([_data]).toarray().tolist()[0]        
        
   
    #self.df['binary_data'] = self.df.apply(lambda x:binary_transform(x,count),axis=1) # takes a lot of cpu time!
    self.binary_data = self.df[binary_train_col].as_matrix()
    self.binary_data=map(lambda x: list(x),self.binary_data)
    self.binary_data=self.binary_enc.transform(self.binary_data).toarray().tolist()

    self.binary_target = self.df[target].as_matrix()
    print 'binary data is ready'
    return self  

    
    def binary_transform_df_parallel(df,name):
        print "job with  name ", name, 'started...'
        df['binary_data'] = df.apply(lambda x:binary_transform(x),axis=1)
        print "job with  name ", name, 'finished...'
        return df
    
    
    pool=Pool(processes=5)
    results = [apply_async(pool, binary_transform_df_parallel,(group,name)) for name, group in self.df.groupby(self.df.index) ]
    res=[]
    for job in results:
        print "job is waiting to be finished..."
        a= job.get()
        print a
        print type(a)
        res+=[a]
    self.df=pd.concat(res)
    
    
    
    return self  
 
     
 def fit(self,target,clf=None,probability=True):
     self.train_cols=[feature[0] for feature in self.features]
     self.train_cols.remove(target)
     

     self.probability=probability

     start = time.time()     
     print'features to train:', self.train_cols
     self.clf = svm.SVC(probability=probability,class_weight='auto') if clf is None else clf
     if (self.binary_enc is not None):
         print 'binary fitting'                 
         self.clf.fit(self.binary_data,self.binary_target)
     else:    
        self.clf.fit(self.df[self.train_cols].as_matrix().tolist(),self.df[target].as_matrix().tolist())

     print 'fit is done using %.3f sec'%(time.time()-start)
      
     return self

 def normalizate_dataframe(self):                                                                                                                                           
     self.df_norm = (self.df[self.train_cols] - self.df[self.train_cols].mean()) / (self.df[self.train_cols].max() - self.df[self.train_cols].min())
     pass
 

 def validation(self,target,doSklearnAcc=True):
     

     print 'validation is started...'     
     def pred_accuracy(clf,X_test,y_test):
         print 'doing prediction...'
         pred=clf.predict(X_test)             
         print 'Prediction accuracy on the sample (of %d) : %.4f' %(len(y_test), (1 - (1. / len(y_test) * sum( pred != y_test ))))
         return  (1 - (1. / len(y_test) * sum( pred != y_test )))
     
     
     # an user defined validation tool
     if (self.binary_enc is not None):
	 start = time.time()
         acc=pred_accuracy(self.clf,self.binary_data,self.binary_target)
	 print 'validation is done using %.3f sec'%(time.time()-start)

     else:    
        acc=pred_accuracy(self.clf,self.df[self.train_cols],self.df[target])
     if (not doSklearnAcc): return (acc,-1.)
     # a sklearn validation tool
     scores = cross_validation.cross_val_score(self.clf, self.df[self.train_cols].values, self.df[target].values, cv=5, n_jobs=2,verbose=2)
     print scores
     print("Accuracy of SVM with the sklearn validation tool: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
     return (acc,scores.mean())

 def validation_v2(self,X_test,y_test):        
	start = time.time()
        y_true, y_pred = y_test, self.clf.predict(X_test)
        print (classification_report(y_true, y_pred))
        print 'validation is done using %.3f sec'%(time.time()-start)


if __name__ == "__main__":
    
    
  # read command line options and arguments
 import argparse
 parser = argparse.ArgumentParser(description='Two-Class SVM Regressor')
 

 parser.add_argument('-all','--all', help='make all plots',default=None, required=False, action='store_true')
 args = parser.parse_args()

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('airport_dist',preprocessing.MinMaxScaler()),    
    
    ]
    
 renew = False
 doValidation=False
 if not(os.path.exists('svm_twoclass_sklearn_saved.pkl')) or renew:
 # Class doing C-Support Vector Classification
    svc=svm_twoclass_sklearn(dataframe[ [feature[0] for feature in features] ],features)
 # plot numerical data
 #svc.plot_dataframe()

 # transform to numerical data and plot again
    svc.transform_dataframe()
    #svc.transform_dataframe_binary('status') # cpu consuming!!!
 #svc.plot_dataframe()
 
 # fit the model
 #svc.fit('status',probability=True)
    svc.fit('status',probability=False)
 
 
 
 
 # print the validation information
 #acc1,acc2=svc.validation('status',doSklearnAcc=True)
    acc1,acc2=svc.validation('status',doSklearnAcc=False) if doValidation else  (None,None)
    start=time.time()
    print 'dumping SVM...'
    #joblib.dump(svc,'svm_twoclass_sklearn_saved.pkl')
    with open('svm_twoclass_sklearn_saved.pkl', 'wb') as fid:
        dill.dump(svc, fid)   
    print 'SVM has been dumped... with time %.3f'%( time.time()-start)
 else:
    start=time.time()
    print 'loading is  started..'
    #svc = joblib.load('svm_twoclass_sklearn_saved.pkl')
    with open('svm_twoclass_sklearn_saved.pkl', 'rb') as fid:
       svc=dill.load(fid)   
    print 'SVM has been loaded... with time %.3f'%( time.time()-start)

    print 'start valiadtion...'
    if svc.binary_enc is not None:
        _,X_test,_, y_test = cross_validation.train_test_split(svc.binary_data, svc.binary_target, test_size=0.1, random_state=0)
        if (doValidation):  svc.validation_v2(X_test,y_test)
    else:
        acc1,acc2=svc.validation('status',doSklearnAcc=False) if doValidation else (None,None)
    

 
 doOptimization=False
 # perhaps, it is worth to make an optimization of the SVM
 tuned_params=[
        {'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]}, # set 1
        {'kernel': ['linear'], 'C': [1, 10, 100]}    # set 2
        ]
 
 if doOptimization and doValidation:
    # do a normalization of the dataframe: otherwise it might be divergence in the fit
    acc3=-1e10
    svc.normalizate_dataframe()
    # split into train and test samples 
    
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(svc.df_norm[svc.train_cols].values, svc.df['status'].values, test_size=0.4, random_state=0)
    optimizer=sklearn_optimizer.sklearn_optimizer(svc.clf,X_train=X_train,y_train=y_train,scoring='precision',tuned_params=tuned_params,n_jobs=2) # will take a lot of CPU
 
    # print summary
    acc3=optimizer.summary(X_test, y_test)
 
    # decide on the optimization: take it or not
    if (not( acc3 < acc1 and acc3 < acc2) ): 
        print 'optimization is taken:'
        svc.clf = optimizer.best_clf
        print 'validation 1'
        svc.validation('status')
        svc.clf.set_params(**optimizer.best_params)
        svc.fit('status',clf=svc.clf)
        # refit model on best parameters
        print 'validation 2'
        svc.validation('status')




 if (svc.probability):
 # create probabilites: works only when probas are switched on, i.e. svc.fit('status',probability=True)
    def proba_maker(x,proba_provider,train_cols):     
         return (proba_provider.predict_proba(x[train_cols].values)[0][1],proba_provider.predict_proba(x[train_cols].values)[0][0])
     
 
 
    svc.df['probas_OK'] = svc.df.apply(lambda x: proba_maker(x,svc.clf,svc.train_cols)[0],axis=1)
    svc.df['probas_NOTOK'] = svc.df.apply(lambda x: proba_maker(x,svc.clf,svc.train_cols)[1],axis=1)
 

      # plot probabilites
    sns.set_context("poster")
    prefix = 'SVM_sklearn_twoclass_'
    label = 'probas_all_distr'
    b, g, r, p = sns.color_palette("muted", 4)
 
 
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK for OK',hist=False, axlabel='probability')
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK for OK',hist=False, axlabel='probability')
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK for NOT_OK',hist=False, axlabel='probability')
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK for NOT_OK',hist=False, axlabel='probability') 
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 
  
 # plot probabilites
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_ok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 # plot probabilites
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(svc.df.loc[svc.df[svc.df['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_notok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

  
 # make a classifier
 def svc_classifier(clf,x,train_cols):
      return int(clf.predict(x[train_cols])[0] > 0)
  
  
 # perform predictions
 svc.df['predict'] = svc.df.apply(lambda x: svc_classifier(svc.clf,x,svc.train_cols),axis=1)
 


 # calculate weights (needed for correct type-1/type-2 error results)
 weights = {
        0:float(len(svc.df.loc[svc.df[svc.df['status'] == 1].index,['status']]))/float(len(svc.df.loc[svc.df[svc.df['status'] == 0].index,['status']])),
        1:1
    }

  # estimate perfomance: type1/ type2 errors etc
 pos = svc.df.loc[svc.df[svc.df['predict'] == 1].index,['status']]
 true_pos = pos.loc[pos[pos['status'] == 1].index]
 false_neg = pos.loc[pos[pos['status'] == 0].index]

 neg = svc.df.loc[svc.df[svc.df['predict'] == 0].index,['status']]
 true_neg = neg.loc[neg[neg['status'] == 0].index]
 false_pos = neg.loc[neg[neg['status'] == 1].index]

 
 if (True): apply_weights=True
 #if ('weights' in vars(args) and vars(args)['weights']):   apply_weights=True
 else:   apply_weights=False
 
 
 if (not apply_weights):
    print "found rates", {    'true_pos':len(true_pos),
            'false_pos':len(false_pos),
            'true_neg':len(true_neg),
            'false_neg':len(false_neg),
 
        }

    performance = {
 
        'prec':float( len(true_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'type1':float( len(false_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'tpr':float( len(true_pos) )/float(  len(true_pos) +   len(false_neg) ),
        'npv':float( len(true_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'type2':float( len(false_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'fpr':float( len(false_pos) )/float(  len(true_neg) +   len(false_pos) ),
 
    }
 else:
     print "found rates", {    'true_pos':len(true_pos)*weights[1],
            'false_pos':len(false_pos)*weights[1],
            'true_neg':len(true_neg)*weights[0],
            'false_neg':len(false_neg)*weights[0],
 
        }

     performance = {
 
        'prec':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'type1':float( len(false_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'tpr':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_neg)*weights[0] ),
        'npv':float( len(true_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'type2':float( len(false_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'fpr':float( len(false_pos)*weights[1] )/float(  len(true_neg)*weights[0] +   len(false_pos)*weights[1] ),
 
    }
 
 #print performance
 if ('perfomance_diagram' in vars(args) and vars(args)['perfomance_diagram']) or ('all' in vars(args) and vars(args)['all']):  
    sns.barplot(np.array(performance.keys()),np.array(performance.values()),ci=None, palette="Paired", hline=.1)
    label = 'perfomance'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
