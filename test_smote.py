#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 21, 2015 11:08:50 AM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn import cross_validation    
import Normalizer
import time
import os.path
import mysmote as smote
import nb_gaussian_twoclass_sklearn as nbGauss



if __name__ == "__main__":
  # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    
    
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('airport_dist',preprocessing.MinMaxScaler()),   
    ('booker_email_provider',Normalizer.NormalizerWrapper(type='normal')),   
    ('booker_email_country',Normalizer.NormalizerWrapper(type='normal')),   
    ('product_airline',Normalizer.NormalizerWrapper(type='normal')),   
    
    ]

 nb=nbGauss.nb_gaussian_twoclass_sklearn(dataframe[ [feature[0] for feature in features] ],features)   
 
 # Test 1: before SMOTE generation
 print "before SMOTE"
 nb.transform_dataframe()
 nb.fit('status',probability=False)
 acc1,acc2=nb.validation('status',doSklearnAcc=False)
 _,X_test,_, y_test = cross_validation.train_test_split(nb.df[nb.train_cols].as_matrix(), nb.df['status'].as_matrix(), test_size=0.1, random_state=0)    
 nb.validation_v2(X_test,y_test)
    
 
 # Test 2 after SMOTE
 print "after SMOTE"
 smote = smote.mysmote(nb.df,features,'status')
 smote.train_cols=nb.train_cols
 
 print 'before SMOTE generate: Minority of len ',len(smote.df.loc[smote.df[smote.df[smote.target] == 0].index,[smote.target]])
 #smote.generate_smote_data(int(smote.getRatio(labels=[1,0]))*100,3,label=0,add2df=True)
 #smote.generate_smote_data_v2(500,3,label=0,add2df=True)
 
 from UnbalancedDataset import OverSampler, SMOTE, bSMOTE1, bSMOTE2, SVM_SMOTE 
 #smoteSampler = SMOTE(ratio=5,random_state=1) # reproduce smote.py
 #smoteSampler = bSMOTE1(ratio=5,random_state=1) # the best one!
 #smoteSampler = bSMOTE2(ratio=5,random_state=1) # the best one!
 #smoteSampler = SVM_SMOTE(ratio=5,random_state=1,class_weight='auto') # too slow, but also good enough
 #smoteSampler = SVM_SMOTE(ratio=2,random_state=1,class_weight='auto') # too slow, but also good enough
 smoteSampler = bSMOTE2(ratio=5,k=30,m=10,random_state=None) # the best one!
 smote.generate_smote_data_v3(smoteSampler,add2df=True)
 print 'after SMOTE generate: Minority of len ',len(smote.df.loc[smote.df[smote.df[smote.target] == 0].index,[smote.target]])
 
 
 nb.df=smote.df
 nb.fit('status',probability=False)
 acc1,acc2=nb.validation('status',doSklearnAcc=False)
 nb.validation_v2(X_test,y_test)
 
 