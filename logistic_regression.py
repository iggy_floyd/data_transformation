'''
	Logistic Regressions
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
import prettytable
import statsmodels.api as sm
from sklearn import linear_model
from sklearn.cross_validation import cross_val_score
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report



class logistic_regression(object):
 '''
	
 '''

 def __init__(self,tuned_parameters = {'C': [1, 10, 100, 10000,1e5,1e6]},scores = ['precision', 'recall']):

        self.tuned_parameters = tuned_parameters
        self.scores = scores
 	pass

 def train (self,X_train,y_train):
  for score in self.scores:
    print("# Tuning hyper-parameters for %s" % score)
    print()

    clf = GridSearchCV(linear_model.LogisticRegression(C=1e5,class_weight='auto'), self.tuned_parameters, n_jobs = -1,verbose=1 ,cv=5, scoring=score)
    clf.fit(X_train, y_train)

    print("Best parameters set found on development set:")
    print()
    print(clf.best_estimator_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() / 2, params))
    print()
    print "best score: %0.3f" % (clf.best_score_)
    print()
    print("Best parameters:")
    bestParams = clf.best_estimator_.get_params()
    for p in sorted(self.tuned_parameters.keys()):
     print "\t %s: %f" % (p, bestParams[p])

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_train, clf.predict(X_train)
    print(classification_report(y_true, y_pred))
    print()
  self.clf =  clf.best_estimator_
  return 

 def predict(self,df):
    return self.clf.predict(df)

 def predict_proba(self,df):
    return  self.clf.predict_proba(df)



if __name__ == '__main__':

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 labels = [
    'status',
    'airport_dist',
    'LD_booker_payer',
    'product_price',
    'LD_booker_first_traveller',
    'booker_depairport_dist'
    #'product_typeOfFlight',
    #'product_airline',
    #'product_arrivalAirport',
    #'product_departureAirport'   
    ]

 # needed transformations
 dataframe2 = dataframe[labels]
 dataframe2['status'] = dataframe2['status'].apply(lambda x: 0 if x=='CHECKED_OK' else 1)

 import Normalizer
 transformations = [
   # ('product_airline',Normalizer.NormalizerWrapper(type='normal'),None),
   # ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal'),None),
   # ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal'),None),
   # ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal'),None),
    ]
 for trans in  transformations:
    normalizer = trans[1]
    X=trans[0]
    normalizer.fit_transform(dataframe2[X].as_matrix())
    dataframe2[X] = dataframe2[X].apply(lambda x: normalizer.transform(x))

 
 # logistic regression
 lg=logistic_regression()
 lg.train(dataframe2[labels[1:]],dataframe2['status'])

 # Test of Logistic Regression
 y_ok_proba=lg.predict_proba(dataframe2.loc[dataframe2[dataframe2['status'] == 0].index,labels[1:]]) 
 y_notok_proba=lg.predict_proba(dataframe2.loc[dataframe2[dataframe2['status'] == 1].index,labels[1:]]) 

 nbins=50
 nx, xbins, ptchs = pl.hist(y_ok_proba[0], bins=nbins,normed=True)
 
 print y_ok_proba
 print y_notok_proba
 
 pl.clf() 
 nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.

 xbins_ok = xbins
 nx_frac_ok = nx_frac

 width = xbins[1] - xbins[0] # Width of each bin.
 x_ok = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
 y_ok = np.ravel(zip(nx_frac,nx_frac))
 
 
 nx, xbins, ptchs = pl.hist(y_notok_proba[0], bins=nbins,normed=True)
 pl.clf() 
 nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
 nx_frac_notok = nx_frac
 xbins_not_ok = xbins
 width = xbins[1] - xbins[0] # Width of each bin.
 x_notok = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
 y_notok = np.ravel(zip(nx_frac,nx_frac))
 

 pl.plot(x_ok,y_ok,c='b',linestyle="dashed",label="OK")
 pl.plot(x_notok,y_notok,c='r',label="NOT_OK")
 pl.xlim(0,1)
 pl.title('Projective likelihood estimator')          
 pl.legend(loc='upper right')
 pl.show()
 
 sys.exit(1)
 
