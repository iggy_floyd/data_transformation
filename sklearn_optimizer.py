#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 14, 2015 11:46:44 AM$"

from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report


class sklearn_optimizer(object):
    def __init__(self,clf,X_train=[],y_train=[],scoring='recall',tuned_params={},n_jobs=-1):
        self.clf=clf        
        print 'Starting optimization...'
        self.grid_clf=GridSearchCV(clf,tuned_params, n_jobs = n_jobs,verbose=2 , scoring=scoring).fit(X_train, y_train)
        self.best_clf=self.grid_clf.best_estimator_        
        self.best_params=self.grid_clf.best_params_
    
    
    def summary(self,X_test,y_test):
        print "best score: %0.3f" % (self.grid_clf.best_score_)
        print "best params: %r" % ( self.best_params)
        y_true, y_pred = y_test, self.best_clf.predict(X_test)
        print (classification_report(y_true, y_pred))
        
        def pred_accuracy(clf,X_test,y_test):
         pred=clf.predict(X_test)             
         print 'Prediction accuracy on the sample (of %d) : %.4f' %(len(y_test), (1 - (1. / len(y_test) * sum( pred != y_test ))))
         return  (1 - (1. / len(y_test) * sum( pred != y_test )))
     
        # an user defined validation tool
        acc=pred_accuracy(self.best_clf,X_test,y_test)
        
        #return self.grid_clf.best_score_
        return acc

