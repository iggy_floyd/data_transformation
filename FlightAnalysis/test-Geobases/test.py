'''	This an example of use Geobases.
	See 
	https://github.com/opentraveldata/geobases/wiki/Using-your-own-data-with-GeoBases
	https://gist.github.com/alexprengere/4691086

	about pdfkit 0.4.1
	see 
	https://pypi.python.org/pypi/pdfkit/0.4.1
'''



import pdfkit
import os

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


from GeoBases import GeoBase

g = GeoBase(data='feed',     source=open('csv/file.csv'),     delimiter=' ',     headers=['name', 'lat', 'lng'],    key_fields = ['name'])

lines = [ 
		['d1','d2']
	]

icons = ['d1','d2']

with cd("web/"):
	 g.visualize(output='test',  icon_label='name', from_keys=icons,   add_lines=lines)
	 options = {
			'javascript-delay':5000,
			'no-stop-slow-scripts':True
		   }
	 pdfkit.from_file('test_map.html', 'test.pdf',options=options)


