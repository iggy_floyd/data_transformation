# -*- coding: utf-8 -*-
'''
	    This is a GeoFlights Class designed 
            to plot Flights and Airports on Earth maps.


            There are some external packages needed for GeoFlights

            1) The code requires that aggdraw <http://www.effbot.org/zone/aggdraw-index.htm>
            is installed. This is a short how-to on the installation:

            # get source
            wget http://effbot.org/media/downloads/aggdraw-1.1-20051010.zip
            unzip aggdraw-1.1-20051010.zip
            cd aggdraw-1.1-20051010/
            
            # compile source
            python setup.py build

            # make installation folder and copy all needed file there
            mkdir aggdraw
            cp build/lib.linux-i686-2.7/aggdraw.so aggdraw
            
            # create a bootstrap file and __init__.py following suggestion of http://stackoverflow.com/questions/10968309/how-to-import-python-module-from-so-file
            # touch aggdraw/aggdraw.py and add def __bootstrap__(): ....
              def __bootstrap__():
                global __bootstrap__, __loader__, __file__
                import sys, pkg_resources, imp
                __file__ = pkg_resources.resource_filename(__name__,'aggdraw.so')
                __loader__ = None; del __bootstrap__, __loader__
                imp.load_dynamic(__name__,__file__)
             __bootstrap__()


             # __init__.py:



             
                # Aggdraw Library. http://www.effbot.org/zone/aggdraw-index.htm

                import os,sys
                # Store current working directory
                _pwd = os.path.dirname(__file__)
                # Append current directory to the python path
                sys.path.append(_pwd)
                
                
               __doc__ = aggdraw.__doc__


             # test
                import Image
                from aggdraw import *
                draw = Draw("RGB", (800, 600))
                draw.mode, draw.size
                pen = Pen("black")
                draw.ellipse((120, 150, 170, 200), pen)

             # install and clean
             mv aggdraw ..  
             cd ..
             rm -r aggdraw-1.1-20051010/


           2) Another package what is needed is GCMAP
              It requires  pyproj package, please, install it
                 
               sudo pip install -U pyproj

               
            # download it 
            git clone https://github.com/paulgb/gcmap.git

            # install it 
            mv gcmap/ _gcmap/
            mv _gcmap/gcmap/ .


            # because gcmap depends on aggdraw, add a soft link
            cd gcmap/
            ln -s ../aggdraw 
            cd -

            # clean
            rm -rf _gcmap/


            # test it
            
            from gcmap import GCMapper
            gcm = GCMapper()
            lons1 = [-79.4, -73.9, -122.4, -123.1, -0.1  ]
            lats1 = [ 43.7,  40.7,   37.8,   49.2,  51.5 ]
            lons2 = lons1[1:] + lons1[:1]    # this creates a cycle through the points
            lats2 = lats1[1:] + lats1[:1]
            gcm.set_data(lons1, lats1, lons2, lats2)
            img = gcm.draw()
            img.save('output.png')



        To generate HTML documentation for this module issue the  command:

        pydoc -w GeoFlights

        
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import numpy as np
import pandas as pd
import json
import sys
import traceback
import csv
from gcmap import GCMapper 
from gcmap import Gradient 
import matplotlib.pyplot as pl
import matplotlib.image as mpimg

import os

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)




class GeoFlights(object):
 '''
   
 '''

 # http://datahub.io/dataset/open-flights OpenFlight Database. 
 # Names of Columns for routes
 ROUTE_COLS = ('airline_name', 'airline_id', 'source_code', 'source_id', 'dest_code', 'dest_id', 'codeshare', 'stops', 'equiptment')
 # Name of Columns for airpots
 AIRPORT_COLS = ('airport_id', 'airport_name', 'city', 'country', 'iata', 'icao', 'latitude', 'longitude', 'altitude', 'timezone', 'dst')
 # Name of Columns for airlines
 AIRLINE_COLS = ('airline_id', 'airline_name', 'alias', 'iata','icao', 'callsign', 'country','active')
 # Name of Columns for domains
 DOMAIN_COLS = ('id','domain','tested url','additional extensions','last check')
 
 table = {
                0xe4: u'ae', # a diaeresis
                ord(u'ö'): u'oe',
                ord(u'ü'): u'ue',
                ord(u'ß'): u'ss',
                0xe1: u'a',  	# a acute 'á' => 'a'
                0xe0: u'a',     # a grave 'à' => 'a'
                0xe2: u'a',     # a circumflex  'â' => 'a'
                0xe3: u'a',     # a tilde 'ã' => 'a'
                0xe7: u'c',     # c cedilla 'ç' => 'c'
                0xe9: u'e',     # e acute 'é' => 'e'
                0xe8: u'e',     # e grave 'è' => 'e'
                0xea: u'e',     # e circumflex 'ê' => 'e'
                0xed: u'i',     # i acute 'í' => 'i'
                0xec: u'i',     # i grave 'ì' => 'i'
                0xee: u'i',     # i circumflex 'î' => 'i'
                0xef: u'i',     # i diaeresis 'ï' => 'i'
                0xf1: u'n',     # n tilde 'ñ' => 'n'
                0xf2: u'o',     # o grave 'ò' => 'o'
                0xf3: u'o',     # o acute 'ó' => 'o'
                0xf5: u'o',     # o tilde   'õ' => 'o'
                0xf4: u'o',     # o circumflex 'ô' => 'o'
                0xf6: u'o',     # o diaeresis 'ï' => 'o'
                0xf9: u'u',     # u grave 'ù' => 'u'
                0xfa: u'u',     # u acute 'ú' => 'u'
                0xfb: u'u',     # u circumflex 'û' => 'u'
                0xfd: u'y',      # y acute 'ý' => 'y',
                0xdf: u'ss'      
                
            }
  



 import os,sys
 from GeoBases import GeoBase

 pwd = os.path.dirname(__file__)
 if pwd == '': pwd='.'
 routes = pd.read_csv(pwd+'/routes.dat', header=None,index_col=False, names=ROUTE_COLS, na_values=['\\N'])
 airports = pd.read_csv(pwd+'/airports.dat', header=None,index_col=False, names=AIRPORT_COLS)
 airlines = pd.read_csv(pwd+'/airlines.dat', header=None,index_col=False, names=AIRLINE_COLS, na_values=['\\N'])
 airport_geobase = GeoBase(data='ori_por', verbose=False)
 domains=pd.read_csv(pwd+'/bulk-domains-26-oct-2014.csv',index_col=0,names=DOMAIN_COLS,na_values=[' '],keep_default_na = False)
    


 @staticmethod
 def findUrlforDomain(name):
    ''' return url of the domain name  '''
    
    domains = GeoFlights.domains[GeoFlights.domains['domain'].str.contains('^ '+name+'\.')]
    if len(domains) == 0: domains = GeoFlights.domains[GeoFlights.domains['domain'].str.contains('^ '+name)]
    if len(domains) == 0: return (None,None)
    return (domains['domain'].as_matrix().tolist(),domains['tested url'].as_matrix().tolist())

    

 @staticmethod 
 def plotGeoDataDomains(names,output_prefix,output_folder):
    ''' plots  geolocations of found Domains '''

    res = []
    for name in names:
        
        domains,urls = GeoFlights.findUrlforDomain(name)                
        if (urls is None): continue
        res += [(domains,urls)]
        

    import mmutils.ipinfo

    mmutils.ipinfo.init_geo(GeoFlights.pwd+'/mmutils/GeoLiteCity.dat') 
    import socket 
    icons   = []    
    geodata = []
    for u in res:                
        _geodata=mmutils.ipinfo.get_geo(socket.gethostbyname(str(u[0][0][1:]))) # remove leading ' '
        
        geodata +=[( _geodata.country_name,_geodata.latitude,_geodata.longitude)]
        icons +=[ _geodata.country_name]

    df = pd.DataFrame(geodata, columns=['name', 'lat', 'lng']) 
    
    from StringIO import StringIO
    from GeoBases import GeoBase
    output = StringIO()
    df.to_csv(output, sep=',', encoding='utf-8',index=False,header=False)    
    output.seek(0) 
    
    g = GeoBase(data='feed',     source=output,     delimiter=',',     headers=['name', 'lat', 'lng'],    key_fields = ['name'])
    with cd(output_folder):
	 g.visualize(output=output_prefix, icon_label='name',from_keys=icons)
  
    return 



 @staticmethod
 def get_country_city_from_airport(iata_code):
    ''' return all needed information '''
    return (
            GeoFlights.airport_geobase.get(iata_code)['country_name'],
            GeoFlights.airport_geobase.get(iata_code)['country_code'],
            GeoFlights.airport_geobase.get(iata_code)['city_name_ascii']
            )


 @staticmethod
 def plot_airport_geobase(dataframe,categ_name_in,categ_name_out,val_in,val_out,status,output_prefix,output_folder,remove_duplicates=True):
    ''' plot distribution of airports with geobases '''
    
    # extract entries
    entries =dataframe.loc[dataframe[dataframe['status'] == status].index,[categ_name_in,categ_name_out]]
    pairs = entries.groupby((categ_name_in, categ_name_out)).size()    
    pairs = pairs.reset_index()
    pairs.columns = (categ_name_in, categ_name_out, 'cnt')

    type = None # type is True if we plot all connection to arrival airport
    if (val_in is None) and (val_out is not None) : type = True
    if (val_in is not None) and (val_out is  None) : type = False
    if (type is None): return
    
    # filter dataframe
    if (remove_duplicates):
        if (type):  dataframe = pairs[pairs[categ_name_out].isin([val_out])]
        else :      dataframe = pairs[pairs[categ_name_in].isin([val_in])]
    else:
        if (type):  dataframe = dataframe[dataframe[categ_name_out].isin([val_out])]
        else :      dataframe = dataframe[dataframe[categ_name_in].isin([val_in])]
    

    # find all data in geobases
    
    if (type):
        
        icons = [val_out]        
        vals = dataframe[categ_name_in].as_matrix().tolist()
        icons += vals
        lines = [ [val,val_out] for val in vals ]
    else:
        
        icons = [val_in]        
        vals = dataframe[categ_name_out].as_matrix().tolist()
        icons += vals
        lines = [ [val_in,val] for val in vals ]

    # here plot
    
    with cd(output_folder):
        GeoFlights.airport_geobase.visualize(output=output_prefix,  icon_label='iata_code', from_keys=icons,add_lines=lines)
    return


 @staticmethod
 def test_plot():
    ''' make a test plot using OpenFlights Database '''

    # taken from http://nbviewer.ipython.org/gist/paulgb/5851489
    airport_pairs = GeoFlights.routes.groupby(('source_id', 'dest_id')).size()
    airport_pairs = airport_pairs.reset_index()
    airport_pairs.columns = ('source_id', 'dest_id', 'cnt')

    airport_pairs = airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='airport_id').merge(GeoFlights.airports, left_on='dest_id', right_on='airport_id', suffixes=('_source', '_dest'))

    gcm = GCMapper()
    gcm.set_data(airport_pairs.longitude_source, 
                airport_pairs.latitude_source, 
                airport_pairs.longitude_dest, airport_pairs.latitude_dest, 
                airport_pairs.cnt)

    img = gcm.draw()
    img.save('test.png')
    pl.imshow(img)
    pl.show()
    return






def plot_frauds_gcmap():
    ''' does some fraud flight plots '''
 
    # Set-up some parameters and loading dataframe
    process_name='fps_fraud_classification'
    print "reading from ",process_name+'_transformed.csv',"...."
    dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
    
    # extract fraud entries with only information on airports
    frauds =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,['product_departureAirport','product_arrivalAirport']]
    
    
    fraud_airport_pairs = frauds.groupby(('product_departureAirport', 'product_arrivalAirport')).size()
    
    fraud_airport_pairs = fraud_airport_pairs.reset_index()
    fraud_airport_pairs.columns = ('source_id', 'dest_id', 'cnt')
    
    
    # get geodata and merge it to 'fraud_airport_pairs'
    fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='iata')    
    fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='dest_id', right_on='iata', suffixes=('_source', '_dest'))

    size=(4678, 3308)
    width,height=size
    
    gcm_fraud = GCMapper(line_width=2,width=width,height=height,gc_resolution=400,cols=Gradient(((0, 0, 0, 0), (0.5, 255, 0, 0), (1, 255, 255, 255)))) # use red gradient
    gcm_fraud.set_data(fraud_airport_pairs.longitude_source, 
                fraud_airport_pairs.latitude_source, 
                fraud_airport_pairs.longitude_dest, 
                fraud_airport_pairs.latitude_dest, 
                fraud_airport_pairs.cnt)

    img_fraud = gcm_fraud.draw()


 # get plot of all routes
 # taken from http://nbviewer.ipython.org/gist/paulgb/5851489
    airport_pairs = GeoFlights.routes.groupby(('source_id', 'dest_id')).size()
    airport_pairs = airport_pairs.reset_index()
    airport_pairs.columns = ('source_id', 'dest_id', 'cnt')

    airport_pairs = airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='airport_id').merge(GeoFlights.airports, left_on='dest_id', right_on='airport_id', suffixes=('_source', '_dest'))

    gcm = GCMapper(line_width=3,width=width,height=height,gc_resolution=400)
    gcm.set_data(airport_pairs.longitude_source, 
                airport_pairs.latitude_source, 
                airport_pairs.longitude_dest, airport_pairs.latitude_dest, 
                airport_pairs.cnt)

    img = gcm.draw()

    

    # Overlay of two images
    # taken from http://stackoverflow.com/questions/10640114/overlay-two-same-sized-images-in-python
    #import Image
    from PIL import Image
    from PIL import ImageDraw
    
    ## http://stackoverflow.com/questions/9174338/programmatically-change-image-resolution
    
    
    img_fraud = img_fraud.resize(size, Image.ANTIALIAS)
    img = img.resize(size, Image.ANTIALIAS)    
    new_image = Image.blend(img, img_fraud, 0.65)

    new_image .save('fraud_A2_200dpi.png',dpi=(200,200))
    pl.imshow(new_image)
    
    
    pl.show()
    return 

    
def plot_nonfrauds_gcmap():
    ''' does some non-fraud flight plots '''
 
    # Set-up some parameters and loading dataframe
    process_name='fps_fraud_classification'
    print "reading from ",process_name+'_transformed.csv',"...."
    dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
    
    # extract fraud entries with only information on airports
    frauds =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,['product_departureAirport','product_arrivalAirport']]
    
    
    fraud_airport_pairs = frauds.groupby(('product_departureAirport', 'product_arrivalAirport')).size()
    
    fraud_airport_pairs = fraud_airport_pairs.reset_index()
    fraud_airport_pairs.columns = ('source_id', 'dest_id', 'cnt')
    
    
    # get geodata and merge it to 'fraud_airport_pairs'
    fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='iata')    
    fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='dest_id', right_on='iata', suffixes=('_source', '_dest'))
    

    gcm_fraud = GCMapper(cols=Gradient(((0, 0, 0, 0), (0.5, 0, 255, 0), (1, 255, 255, 255)))) # use green gradient
    gcm_fraud.set_data(fraud_airport_pairs.longitude_source, 
                fraud_airport_pairs.latitude_source, 
                fraud_airport_pairs.longitude_dest, 
                fraud_airport_pairs.latitude_dest, 
                fraud_airport_pairs.cnt)

    img_fraud = gcm_fraud.draw()


 # get plot of all routes
 # taken from http://nbviewer.ipython.org/gist/paulgb/5851489
    airport_pairs = GeoFlights.routes.groupby(('source_id', 'dest_id')).size()
    airport_pairs = airport_pairs.reset_index()
    airport_pairs.columns = ('source_id', 'dest_id', 'cnt')

    airport_pairs = airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='airport_id').merge(GeoFlights.airports, left_on='dest_id', right_on='airport_id', suffixes=('_source', '_dest'))

    gcm = GCMapper()
    gcm.set_data(airport_pairs.longitude_source, 
                airport_pairs.latitude_source, 
                airport_pairs.longitude_dest, airport_pairs.latitude_dest, 
                airport_pairs.cnt)

    img = gcm.draw()


    # Overlay of two images
    # taken from http://stackoverflow.com/questions/10640114/overlay-two-same-sized-images-in-python
    import Image
    new_image = Image.blend(img, img_fraud, 0.5)

    new_image .save('nonfraud.png')
    pl.imshow(new_image)
    pl.show()   
    return 


def plot_frauds_basemap():    
 ''' try to use basemap api '''


 try:
    from mpl_toolkits.basemap import Basemap
 except: 
    print "Basemap is not installed"
    return 

 
 # taken from http://nbviewer.ipython.org/github/mqlaql/geospatial-data/blob/master/Geospatial-Data-with-Python.ipynb
 fig, ax = pl.subplots(figsize=(12,12))
 #map = Basemap(projection='merc', llcrnrlat=-80, urcrnrlat=80,
 map = Basemap(projection='mill',
            llcrnrlat=-80, urcrnrlat=80,
            llcrnrlon=-180, urcrnrlon=180, 
            resolution='l')


 land_color = 'lightgray'
 water_color = 'lightblue'
 map.fillcontinents(color=land_color, lake_color=water_color)
 map.drawcoastlines()
 map.drawmapboundary(fill_color=water_color)

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
    
 # extract fraud entries with only information on airports
 frauds =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,['product_departureAirport','product_arrivalAirport']]
    
    
 fraud_airport_pairs = frauds.groupby(('product_departureAirport', 'product_arrivalAirport')).size()
    
 fraud_airport_pairs = fraud_airport_pairs.reset_index()
 fraud_airport_pairs.columns = ('source_id', 'dest_id', 'cnt')
 # get geodata and merge it to 'fraud_airport_pairs'
 fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='iata')    
 fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='dest_id', right_on='iata', suffixes=('_source', '_dest'))


 for i in range(len(fraud_airport_pairs.longitude_source)):
    map.drawgreatcircle(
                fraud_airport_pairs.longitude_source[i], 
                fraud_airport_pairs.latitude_source[i], 
                fraud_airport_pairs.longitude_dest[i], 
                fraud_airport_pairs.latitude_dest[i], 
                color='r'
                    )

 ax.set_title('Fraud Flights')
 map.ax = ax 

 #map = Basemap(projection='mill',lon_0=180)
 #map = Basemap(projection='cyl',
 #           #projection='mill',
 #           llcrnrlon=-180. ,llcrnrlat=-60,
 #           urcrnrlon=180. ,urcrnrlat=80. )

 #map.drawcoastlines()
## map.fillcontinents(color='coral')   
# map.fillcontinents()   
 #map.bluemarble()
# map.drawcoastlines(linewidth=0.5)
# map.drawcountries(linewidth=0.5)
# map.drawstates(linewidth=0.5)
#
# #im=pl.imshow(pl.imread('fraud.png'))
#
 pl.savefig('fraud_basemap.png')
 pl.show() 



 


def plot_frauds_geobases():    
 ''' try to use basemap api '''
 
 try:
    from GeoBases import GeoBase
 except: 
    print "GeoBases is not installed"
    return 


 
# Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)

 # extract fraud entries with only information on airports
 frauds =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,['product_departureAirport','product_arrivalAirport']]
    
    
 fraud_airport_pairs = frauds.groupby(('product_departureAirport', 'product_arrivalAirport')).size()
    
 fraud_airport_pairs = fraud_airport_pairs.reset_index()
 fraud_airport_pairs.columns = ('source_id', 'dest_id', 'cnt')
 # get geodata and merge it to 'fraud_airport_pairs'
 fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='iata')    
 fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='dest_id', right_on='iata', suffixes=('_source', '_dest'))


 from StringIO import StringIO
 output = StringIO()
 fields_source = ['iata_source','latitude_source','longitude_source']
 fields_dest = ['iata_dest','latitude_dest','longitude_dest']

 fraud_airport_pairs[fields_source].to_csv(output, sep=',', encoding='utf-8',index=False,header=False)
 fraud_airport_pairs[fields_dest].to_csv(output, sep=',', encoding='utf-8',index=False,header=False)
 
 lines = [  [i,fraud_airport_pairs['iata_dest'].as_matrix().tolist()[a]] for a,i in enumerate(fraud_airport_pairs['iata_source'].as_matrix().tolist())

         ]
 

 output.seek(0) 
 g = GeoBase(data='feed',     source=output,     delimiter=',',     headers=['name', 'lat', 'lng'],    key_fields = ['name'])
 with cd("web/"):
	 g.visualize( icon_label='name', add_lines=lines)

 

 return


def plot_frauds_geotiler():    
 ''' try to use basemap api 
     https://github.com/wrobell/geotiler/blob/master/examples/ex-basemap.py
     https://github.com/wrobell/geotiler/blob/master/examples/ex-matplotlib.py
 '''
 
 #fig, ax = pl.subplots(figsize=(12,12)) 
 fig = pl.figure(figsize=(10, 10))
 ax = pl.subplot(111)
 try:
    from mpl_toolkits.basemap import Basemap
 except: 
    print "Basemap is not installed"
    return 

 try:     
    import geotiler
 except Exception,e: 
    print "geotiler is not installed"
    print e
    return 
 
 #bbox = -180, -80, 180, 80
 #bbox = -180, -80, 180, 80 
 bbox = 11.78560, 46.48083, 11.79067, 46.48283
 #
 # download background map using OpenStreetMap
 #
 mm = geotiler.Map(extent=bbox, zoom=18)
 img = geotiler.render_map(mm)

 #
 # create basemap
 #
 map = Basemap(
 llcrnrlon=bbox[0], llcrnrlat=bbox[1],
 urcrnrlon=bbox[2], urcrnrlat=bbox[3],
 projection='merc', ax=ax
 )

 
 map.imshow(img, interpolation='lanczos', origin='upper')
 #ax.imshow(img)
 '''
 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
    
 # extract fraud entries with only information on airports
 frauds =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,['product_departureAirport','product_arrivalAirport']]
    
    
 fraud_airport_pairs = frauds.groupby(('product_departureAirport', 'product_arrivalAirport')).size()
    
 fraud_airport_pairs = fraud_airport_pairs.reset_index()
 fraud_airport_pairs.columns = ('source_id', 'dest_id', 'cnt')
 # get geodata and merge it to 'fraud_airport_pairs'
 fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='source_id', right_on='iata')    
 fraud_airport_pairs = fraud_airport_pairs.merge(GeoFlights.airports, left_on='dest_id', right_on='iata', suffixes=('_source', '_dest'))


 for i in range(len(fraud_airport_pairs.longitude_source)):
    map.drawgreatcircle(
                fraud_airport_pairs.longitude_source[i], 
                fraud_airport_pairs.latitude_source[i], 
                fraud_airport_pairs.longitude_dest[i], 
                fraud_airport_pairs.latitude_dest[i], 
                color='r'
                    )

 ax.set_title('Fraud Flights')
 map.ax = ax 
 '''
 pl.savefig('fraud_geotiler.png') 
 pl.show() 
 pl.close()
 return 

if __name__ == '__main__':


 import argparse
 parser = argparse.ArgumentParser(description='Plot Flights on GeoMap of the Earth')
 
 parser.add_argument('-tp','--testplot', help='make a test plot',default=None, required=False, action='store_true')
 parser.add_argument('-fp','--fraudplot', help='make a fraud plot',default=None, required=False, action='store_true')
 parser.add_argument('-nfp','--nonfraudplot', help='make a  non-fraud plot',default=None, required=False, action='store_true')
 parser.add_argument('-bf','--basemapfraud', help='make a fraud plot with Basemap',default=None, required=False, action='store_true')
 parser.add_argument('-geobf','--geobasesfraud', help='make a fraud plot with Geobases',default=None, required=False, action='store_true')
 parser.add_argument('-geotf','--geotilerfraud', help='make a fraud plot with Geotiler',default=None, required=False, action='store_true')

 args = parser.parse_args()

 if ('testplot' in vars(args) and vars(args)['testplot']):  GeoFlights.test_plot()
 if ('fraudplot' in vars(args) and vars(args)['fraudplot']):  plot_frauds_gcmap()
 if ('nonfraudplot' in vars(args) and vars(args)['nonfraudplot']):  plot_nonfrauds_gcmap()
 if ('basemapfraud' in vars(args) and vars(args)['basemapfraud']):  plot_frauds_basemap()
 if ('geobasesfraud' in vars(args) and vars(args)['geobasesfraud']):  plot_frauds_geobases()
 if ('geotilerfraud' in vars(args) and vars(args)['geotilerfraud']):  plot_frauds_geotiler() 


 #GeoFlights.plotGeoDataDomains(['mail'],'test','web/')
 
