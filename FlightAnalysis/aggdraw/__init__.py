'''
	Aggdraw	Library. http://www.effbot.org/zone/aggdraw-index.htm
	
'''
import os,sys
# Store current working directory
_pwd = os.path.dirname(__file__)
# Append current directory to the python path
sys.path.append(_pwd)

import aggdraw
from   aggdraw import *

__doc__ = aggdraw.__doc__  
