'''
 Flight Analysis
	
'''

from FlightAnalysis.aggdraw import *
from FlightAnalysis.gcmap import *

import os,sys
# Store current working directory
_pwd = os.path.dirname(__file__)
# Append current directory to the python path
sys.path.append(_pwd)


_GeoFlights= __import__('GeoFlights')
__doc__ = _GeoFlights.__doc__  
