'''
	This is a CategoricalAnalysis Class designed to analyze some categorical distributions
'''

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
import VariableAnalysis 
import FlightAnalysis.GeoFlights as FG

class CategoricalAnalysis(object):
 '''
	CategoricalAnalysis 
 '''

 @staticmethod
 def processOutliers(x_grid,pdf_ok,pdf_not_ok,threshold=2.,fileoutput='some_table'):
  '''find and process categories from x_grid such thar pdf_not_ok/pdf_ok[category] >threshold '''
 
  from scipy.stats import chisqprob


  not_ok_outlier=[]
  ok_outlier=[]
  for i,x in enumerate(x_grid): 
    chi2test = chisqprob((pdf_ok[i]-pdf_not_ok[i])*(pdf_ok[i]-pdf_not_ok[i])*1000/pdf_ok[i],1) if pdf_ok[i] >0 else 0

    if pdf_ok[i] <=0.: 
      if pdf_not_ok[i]>0.: _not_ok_outlier = threshold
      else: _not_ok_outlier =  0.
    else:  _not_ok_outlier=pdf_not_ok[i]/pdf_ok[i] 

    if pdf_not_ok[i] <=0.: 
      if pdf_ok[i]>0.: _ok_outlier = threshold
      else: __ok_outlier =  0.
    else:  _ok_outlier=pdf_ok[i]/pdf_not_ok[i]

    if ( _not_ok_outlier  >= threshold ): not_ok_outlier+=[(x,_not_ok_outlier,chi2test,pdf_not_ok[i],pdf_ok[i])]
    if ( _ok_outlier  >= threshold ): ok_outlier+=[(x,_ok_outlier,chi2test,pdf_not_ok[i],pdf_ok[i])]
    
    #print x, pdf_not_ok[i],pdf_ok[i],chi2test,pdf_not_ok[i]/pdf_ok[i] if pdf_ok[i] >0 else -1
  
  import operator
  not_ok_outlier = sorted(not_ok_outlier,key=operator.itemgetter(1),reverse=True)
  ok_outlier = sorted(ok_outlier,key=operator.itemgetter(1),reverse=True)
  #not_ok_outlier=np.array(not_ok_outlier, dtype='object, float32, float32')
  #ok_outlier=np.array(ok_outlier, dtype='object, float32, float32')
  
  #df_not_ok = pd.DataFrame.from_records(not_ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  df_not_ok = pd.DataFrame(not_ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK']) 
  #df_ok = pd.DataFrame.from_records(ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  df_ok = pd.DataFrame(ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  
  #df_not_ok = pd.DataFrame(not_ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  #df_ok = pd.DataFrame(ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])

  from StringIO import StringIO
  import prettytable    
  output = StringIO()  
  df_not_ok.to_csv(output, sep='\t', encoding='utf-8')
  output.seek(0)
  pt = prettytable.from_csv(output)
  print >> open(fileoutput+'_not_ok_outlier.log',"w"), pt   ## for debugging
  output = StringIO()
  df_ok.to_csv(output, sep='\t', encoding='utf-8')
  output.seek(0)
  pt = prettytable.from_csv(output)
  print >> open(fileoutput+'_ok_outlier.log',"w"), pt   ## for debugging

  return (df_not_ok,df_ok)


 @staticmethod
 def processOutliersLargerThan(x_grid,pdf_ok,pdf_not_ok,threshold=2.,threshold_prob=0.005,fileoutput='some_table'):
  '''find and process categories from x_grid such thar pdf_not_ok/pdf_ok[category] >threshold '''
 
  from scipy.stats import chisqprob


  not_ok_outlier=[]
  ok_outlier=[]
  for i,x in enumerate(x_grid): 
    chi2test = chisqprob((pdf_ok[i]-pdf_not_ok[i])*(pdf_ok[i]-pdf_not_ok[i])*1000/pdf_ok[i],1) if pdf_ok[i] >0 else 0

    if pdf_ok[i] <=0.: 
      if pdf_not_ok[i]>0.: _not_ok_outlier = threshold
      else: _not_ok_outlier =  0.
    else:  _not_ok_outlier=pdf_not_ok[i]/pdf_ok[i] 
    if (pdf_not_ok[i]<threshold_prob):  _not_ok_outlier =  0.

    if pdf_not_ok[i] <=0.: 
      if pdf_ok[i]>0.: _ok_outlier = threshold
      else: __ok_outlier =  0.
    else:  _ok_outlier=pdf_ok[i]/pdf_not_ok[i]
    if (pdf_ok[i]<threshold_prob):  _ok_outlier =  0.

    if ( _not_ok_outlier  >= threshold ): not_ok_outlier+=[(x,_not_ok_outlier,chi2test,pdf_not_ok[i],pdf_ok[i])]
    if ( _ok_outlier  >= threshold ): ok_outlier+=[(x,_ok_outlier,chi2test,pdf_not_ok[i],pdf_ok[i])]
    
    #print x, pdf_not_ok[i],pdf_ok[i],chi2test,pdf_not_ok[i]/pdf_ok[i] if pdf_ok[i] >0 else -1
  
  import operator
  not_ok_outlier = sorted(not_ok_outlier,key=operator.itemgetter(1),reverse=True)
  ok_outlier = sorted(ok_outlier,key=operator.itemgetter(1),reverse=True)
  #not_ok_outlier=np.array(not_ok_outlier, dtype='object, float32, float32')
  #ok_outlier=np.array(ok_outlier, dtype='object, float32, float32')
  
  #df_not_ok = pd.DataFrame.from_records(not_ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  df_not_ok = pd.DataFrame(not_ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK']) 
  #df_ok = pd.DataFrame.from_records(ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  df_ok = pd.DataFrame(ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  
  #df_not_ok = pd.DataFrame(not_ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])
  #df_ok = pd.DataFrame(ok_outlier, columns=['name', 'outlier_factor', 'Chi2Test','Prob_of_NOT_OK','Prob_of_OK'])

  from StringIO import StringIO
  import prettytable    
  output = StringIO()  
  df_not_ok.to_csv(output, sep='\t', encoding='utf-8')
  output.seek(0)
  pt = prettytable.from_csv(output)
  print >> open(fileoutput+'_not_ok_outlier.log',"w"), pt   ## for debugging
  output = StringIO()
  df_ok.to_csv(output, sep='\t', encoding='utf-8')
  output.seek(0)
  pt = prettytable.from_csv(output)
  print >> open(fileoutput+'_ok_outlier.log',"w"), pt   ## for debugging

  return (df_not_ok,df_ok)


 @staticmethod
 def check_airportoutlier_country(dataframe,fileoutput='some_table'):
    ''' analyisis of country contributions '''
    from StringIO import StringIO
    import prettytable    
    output = StringIO()
    D={}
    for i in dataframe['name'].as_matrix().tolist(): 
            (country,country_code,city) = FG.GeoFlights().get_country_city_from_airport(i)
            if country not in D: D[country] = 1
            else: D[country] += 1
    import operator
    D=sorted([(key,val) for key,val in D.items() ],key=operator.itemgetter(1),reverse=True)
    df = pd.DataFrame(D, columns=['name', 'count']) 
    df.to_csv(output, sep='\t', encoding='utf-8')
    output.seek(0)
    pt = prettytable.from_csv(output)
    print >> open(fileoutput+'_country_outlier.log',"w"), pt   ## for debugging
    return
 
if __name__ == '__main__':

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
 import argparse
 import sys 

 # get outlier for all Probas 
 '''
 X='product_airline' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliers(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)


 X='product_departureAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliers(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 X='product_arrivalAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliers(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 X='booker_email_country' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliers(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 X='booker_email_provider'
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliers(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)



 # get outlier for Probas >= 0.05
 X='product_airline' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)


 X='product_departureAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 X='product_arrivalAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 X='booker_email_country' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 X='booker_email_provider'
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)
 '''

 # make some plots on outliers
 # plot departure airports
 
 
 X='product_departureAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 for i,val_in in  enumerate(a['name'].as_matrix().tolist()):   
 
    FG.GeoFlights().plot_airport_geobase(
        dataframe,
        'product_departureAirport',
        'product_arrivalAirport',
        val_in,None,
        'CHECKED_NOT_OK',
        X+'_'+str(i)+'_outlier','web/',remove_duplicates=False)

 # plot departure airports 
 X='product_arrivalAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)

 for i,val_out in  enumerate(a['name'].as_matrix().tolist()):   
 
    FG.GeoFlights().plot_airport_geobase(
        dataframe,
        'product_departureAirport',
        'product_arrivalAirport',
        None,val_out,
        'CHECKED_NOT_OK',
        X+'_'+str(i)+'_outlier','web/',remove_duplicates=False)

 '''
 # check country from the airport
 X='product_departureAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)
 CategoricalAnalysis().check_airportoutlier_country(a,fileoutput=X+'_not_ok')

 X='product_arrivalAirport' 
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)
 CategoricalAnalysis().check_airportoutlier_country(a,fileoutput=X+'_not_ok')
 '''

 '''
 X='booker_email_provider'
 va=VariableAnalysis.VariableAnalysis(X,dataframe)
 (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
 (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)
 (a,b)=CategoricalAnalysis().processOutliersLargerThan(x_grid,pdf,pdf2,threshold=2.,fileoutput=X)
  
 FG.GeoFlights().plotGeoDataDomains(
        a['name'].as_matrix().tolist(),
        X+'_'+'_outlier','web/')
 '''
 sys.exit(1)
 
 
