'''
	This is a VariableAnalysis Class designed to store some columns from DataFrame
'''
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
import seaborn as  sns
import Normalizer


class VariableAnalysis(object):
 '''
	VariableAnalysis 
 '''

 def __init__(self,name,dataframe,type=None):

        self.name = name
        self.dataframe = dataframe        
        if (type is not None and not self.dataframe[name].isnull().any()):  #.astype(type)
            self.dataframe[name] = pd.DataFrame(self.dataframe[name],dtype=type)
        self.type = type
        self.bins=None
        self.hist=None
	pass


 # taken from  https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
 def kde(self,x, x_grid, bandwidth=0.5, **kwargs):

    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)


 # taken from http://toyoizumilab.brain.riken.jp/hideaki/res/code/sskernel.py
 def kde_adaptive(self, x, x_grid):
    import sskernel
    ret = sskernel.sskernel(x,tin=x_grid, bootstrap=True)
    return ret['y']


 def likelihood(self,target,key,type='adaptive',nsteps=100,X_grid=None):
    ''' possible types: 'adaptive', 'small', 'big', 'standard', 'boolean', 'categoricalnum', 'categoricaltext' '''    

    # extract values for kde creation
    vals =self.dataframe.loc[self.dataframe[self.dataframe[target] == key].index,self.name].as_matrix()  

    fullvals = self.dataframe.loc[:,self.name].as_matrix()  if type in 'categoricaltext' else None

    kdes={
             'adaptive': None,
             'small': None,
             'big': None,
             'standard': None
         }


    if type in kdes:
        if X_grid is None:
            minx=min(vals)
            maxx=max(vals)
            if (minx == maxx): minx = float(minx)/nsteps
            X_grid= np.linspace(float(minx),float(maxx), nsteps)        

        try:
            kdes = {
                'adaptive': self.kde_adaptive(vals,X_grid),
                'small':    self.kde(vals,X_grid, bandwidth=0.05),  # a small bandwith
                'big':      self.kde(vals,X_grid, bandwidth=1.5),   #  a big bandwith,to decrease fluctuations
                'standard': self.kde(vals,X_grid, bandwidth=0.5)  # a standard  bandwith
            }               
            pdf = kdes[type] 
            import detect_peaks as dp
            import operator
            peakind=dp.detect_peaks(pdf, mph=0, edge='both' ,show=True)
            #peakind=dp.detect_peaks(pdf, mph=0, edge='both')
            return(X_grid.tolist(),pdf.tolist(),vals.tolist(),peakind.tolist())
        except: 
            print "something wrong with kde"           
            return(None,None,None,None)
    elif type in 'boolean':
        
        f1 = lambda x: x>0
        f2 = lambda x: x==0
            
        return([0,1],{0:float(len(filter(f2,vals.tolist())))/len(vals.tolist()),1:float(len(filter(f1,vals.tolist())))/len(vals.tolist())},vals.tolist(),[0,1])

    elif type in 'categoricalnum':
        
        D = {}
        for i in vals.tolist(): 
            if i not in D: D[i] = 1
            else: D[i] += 1
       
        import operator
        D=sorted([(key,float(val)/len(vals.tolist())) for key,val in D.items() ],key=operator.itemgetter(0),reverse=False)
            
        return (map(lambda x: x[0],D),map(lambda x: x[1],D),vals.tolist(),map(lambda x: x[0],D)) 

    elif type in 'categoricaltext':
        
        D = {} 
        for i in vals.tolist(): 
            if i not in D: D[i] = 1
            else: D[i] += 1

        Dfullvals ={}
        for i in fullvals.tolist(): 
            if i not in Dfullvals: Dfullvals[i] = 1
            else: Dfullvals[i] += 1



        if X_grid is None:
            import operator
            Dfullvals=sorted([(key,float(val)/len(vals.tolist())) for key,val in Dfullvals.items() ],key=operator.itemgetter(1),reverse=False)
        else:
            Dfullvals = [(key,key) for key in  X_grid]
        
  
        
        return (map(lambda x: x[0],Dfullvals),map(lambda x: float(D[x[0]])/len(vals.tolist()) if x[0] in D.keys() else 0,Dfullvals),vals.tolist(),map(lambda x: x[0],Dfullvals)) 
     

 def takeClosest(self,myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    from bisect import bisect_left
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before  

 def likelihood_at_point(self,x,x_grid,pdf):
    x=self.takeClosest(x_grid,x)
    return pdf[x_grid.index(x)]
    
 #### Numerical integration (http://rosettacode.org/wiki/Numerical_integration)
 @staticmethod
 def simpson(f,x,h):
  return (f(x) + 4*f(x + h/2) + f(x+h))/6.0
 
 @staticmethod
 def left_rect(f,x,h):
  return f(x)
 @staticmethod
 def integrate( f, a, b, steps, meth):
   import __builtin__
   h = float(b-a)/steps
   ival = h * __builtin__.sum([meth(f, a+i*h, h) for i in range(steps)])
   return ival  

 @staticmethod
 def integrate_scipy(f, a, b):
   from scipy import integrate
   return integrate.romberg(f,a,b)


 def normalize(self,minx,maxx,x_grid,pdf):
   f = lambda x: self.likelihood_at_point(x,x_grid,pdf)
   intgrl= VariableAnalysis.integrate(f,minx,maxx,len(x_grid),VariableAnalysis.simpson)
   return map(lambda x: float(x)/intgrl,pdf) if intgrl !=0 else pdf 


 def scatter_plot(self,X,Y,bins=100):
    ''' www.bi.wisc.edu/~fox/2013/06/05/visualizing-the-correlation-of-two-volumes/'''

    from scipy.stats import spearmanr
    from matplotlib.ticker import NullFormatter

    MapX = X[0]
    MapY = Y[0]
    x = np.array(X[1])
    y = np.array(Y[1])

     # start with a rectangular Figure
    mainFig = pl.figure(1, figsize=(8,8), facecolor='white')

    # define some gridding.
    axHist2d = pl.subplot2grid( (9,9), (1,0), colspan=8, rowspan=8 )
    axHistx  = pl.subplot2grid( (9,9), (0,0), colspan=8 )
    axHisty  = pl.subplot2grid( (9,9), (1,8), rowspan=8 )

    # the 2D Histogram, which represents the 'scatter' plot:
    H, xedges, yedges = np.histogram2d( x, y, bins=(bins,bins) )
    cax=axHist2d.imshow(H.T, interpolation='nearest', aspect='auto' )
    cbar = pl.colorbar(cax, pad = 0.5)
    

    # make histograms for x and y seperately.
    axHistx.hist(x, bins=xedges, facecolor='blue', alpha=0.5, edgecolor='None' )
    axHisty.hist(y, bins=yedges, facecolor='blue', alpha=0.5, orientation='horizontal', edgecolor='None')

    # print some correlation coefficients at the top of the image.
    mainFig.text(0.05,.95,'r='+str(round(np.corrcoef( x, y )[1][0],2))+'; rho='+str(round(spearmanr( x, y )[0],2)), style='italic', fontsize=10 )

    # set axes
    axHistx.set_xlim( [xedges.min(), xedges.max()] )
    axHisty.set_ylim( [yedges.min(), yedges.max()] )
    axHist2d.set_ylim( [ axHist2d.get_ylim()[1], axHist2d.get_ylim()[0] ] )

    # remove some labels
    nullfmt   = NullFormatter()
    axHistx.xaxis.set_major_formatter(nullfmt)
    axHistx.yaxis.set_major_formatter(nullfmt)
    axHisty.xaxis.set_major_formatter(nullfmt)
    axHisty.yaxis.set_major_formatter(nullfmt)

    # remove some axes lines
    axHistx.spines['top'].set_visible(False)
    axHistx.spines['right'].set_visible(False)
    axHistx.spines['left'].set_visible(False)
    axHisty.spines['top'].set_visible(False)
    axHisty.spines['bottom'].set_visible(False)
    axHisty.spines['right'].set_visible(False)

    # remove some ticks
    axHistx.set_xticks([])
    axHistx.set_yticks([])
    axHisty.set_xticks([])
    axHisty.set_yticks([])

    # label 2d hist axes
    myTicks = np.arange(0,bins,10);
    axHist2d.set_xticks(myTicks)
    axHist2d.set_yticks(myTicks)
    axHist2d.set_xticklabels(np.round(xedges[myTicks],2))
    axHist2d.set_yticklabels(np.round(yedges[myTicks],2))

    # set titles
    axHist2d.set_xlabel(MapX, fontsize=16)
    axHist2d.set_ylabel(MapY, fontsize=16)
    axHistx.set_title(MapX, fontsize=10)
    axHisty.yaxis.set_label_position("right")
    axHisty.set_ylabel(MapY, fontsize=10, rotation=-90, verticalalignment='top', horizontalalignment='center' )

    # set the window title
    mainFig.canvas.set_window_title( (MapX + ' vs. ' + MapY) )

    
    return
    
 

def continues_plots(dataframe):
  
 what_to_plot=[
                #'airport_dist',
                #'booker_depairport_dist',
                #'booker_arrairport_dist',
                #'traveller_avg_age',
                #'booker_valid_addr',
                'Number_of_travellers',
                #'LD_booker_payer',
                #'booker_valid_ip',                
                #'product_price',
                #'traveller_total_age',
                #'booker_age',
                #'LD_booker_first_traveller',
                #'LD_booker_all_travellers',
                #'traveller_avg_age',
                #'booking_period',
                #'score'

            ]

 def _plot_data(dataframe,name):
    X=name
    va=VariableAnalysis(X,dataframe,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='adaptive')
    (x_grid2,pdf2,vals2,peaksind2)=va.likelihood('status','CHECKED_NOT_OK',type='adaptive')
    # normalize to unity
    pdf=va.normalize(min(vals),max(vals),x_grid,pdf)
    pdf2=va.normalize(min(vals2),max(vals2),x_grid2,pdf2)

    ## Test 2: check normalization of the last kde
    #f = lambda x: va.likelihood_at_point(x,x_grid,pdf)
    #f2 = lambda x: va.likelihood_at_point(x,x_grid2,pdf2)
    #print "Integral1:", VariableAnalysis.integrate(f,min(vals),max(vals),len(x_grid),VariableAnalysis.simpson)  
    #print "Integral2:", VariableAnalysis.integrate(f2,min(vals2),max(vals2),len(x_grid2),VariableAnalysis.simpson)  

    output = StringIO()
    try:
        _dataframe=pd.DataFrame(map(lambda x: (x_grid[x],pdf[x]),peaksind),columns=['X','Y']) if len(peaksind) else None
        _dataframe2=pd.DataFrame(map(lambda x: (x_grid2[x],pdf2[x]),peaksind2),columns=['X','Y']) if len(peaksind2) else None
        _dataframe.to_csv(output, sep='\t', encoding='utf-8')
        _dataframe2.to_csv(output, sep='\t', encoding='utf-8')
        output.seek(0)
        pt = prettytable.from_csv(output)
        print >> open('plots/'+X+'_table_peak.txt' ,"w"), pt   ## for debugging  
    except: pass

 
    pl.plot(x_grid,pdf,label='CHECKED_OK',color='b')
    pl.plot(x_grid,pdf2,label='CHECKED_NOT_OK',color='r')
    pl.title(X)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+X+'.png')
    pl.show()

 
 for name in what_to_plot:    _plot_data(dataframe,name)
 return 
 


def binary_plots(dataframe,prefix='binary_'):

 def _plot_data(dataframe,X,prefix):    
    
    va=VariableAnalysis(X,dataframe,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='bool')
    (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='bool')
 
    counter  = 0
    pl.bar(-.05+counter*0.1,pdf[0],color='b',width=0.1,label='CHECKED_OK')
    pl.bar(0.95+counter*0.1,pdf[1],color='b',width=0.1)
    counter +=1
    pl.bar(-.05+counter*0.1,pdf2[0],color='r',width=0.1,label='CHECKED_NOT_OK')
    pl.bar(0.95+counter*0.1,pdf2[1],color='r',width=0.1)

    pl.xlim(-.5,3) 
    pl.title(X)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+X+'.png')
    pl.show()
    return 

 what_to_plot = [
        'booker_country_payer_binCountry',
        'booker_country_payer_binCountry',
        'booker_ipCountry_payer_binCountry',        
        'is_zipcode','is_birthday','is_phone','is_email'
    ]
 
 
 
 for name in what_to_plot:    _plot_data(dataframe,name,prefix)

 return


def categ_num_plots(dataframe,prefix='categ_num_'):

 def _plot_data(dataframe,X,prefix,width=0.1):    
    
   
    va=VariableAnalysis(X,dataframe,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricalnum')
    (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricalnum')
 
 
    counter  = 0
    if(max(pdf) > max(pdf2)):
        pl.bar(np.array(x_grid)+counter*width,pdf,color='b',width=width,label='CHECKED_OK')
        counter +=1
        pl.bar(np.array(x_grid2)+counter*width,pdf2,color='r',width=width,label='CHECKED_NOT_OK')
    else:
        pl.bar(np.array(x_grid2)+counter*width,pdf2,color='r',width=width,label='CHECKED_NOT_OK')
        counter +=1
        pl.bar(np.array(x_grid)+counter*width,pdf,color='b',width=width,label='CHECKED_OK')

    pl.legend(loc='upper right')
    pl.title(X)    
    pl.savefig('plots/'+prefix+X+'.png')      
    pl.show()
    return 

 what_to_plot = [
        #'booker_valid_ip',
        #'booker_valid_addr',
        'Number_of_travellers',
        #['score',2]        
    ]
 
 
 
 for name in what_to_plot:    
    if (type(name) == type([])):
        _plot_data(dataframe,name[0],prefix=prefix,width=name[1])
    else:
        _plot_data(dataframe,name,prefix)

 return


def categ_text_plots(dataframe,prefix='categ_text_'):

 def _plot_data(dataframe,X,prefix):    
    va=VariableAnalysis(X,dataframe)
    (x_grid,pdf,vals,peaksind)=va.likelihood('status','CHECKED_OK',type='categoricaltext')
    (x_grid2,pdf2,vals2,peaksind)=va.likelihood('status','CHECKED_NOT_OK',type='categoricaltext',X_grid=x_grid)

    counter  = 0
 
    pl.bar([ 2*i  for i,j in enumerate(x_grid)], pdf, align='center',color='b',label='CHECKED_OK')
    pl.bar([ 2*i+0.5  for i,j in enumerate(x_grid2)], pdf2, color='r',label='CHECKED_NOT_OK')
    pl.xticks([ 2*i  for i,j in enumerate(x_grid)],x_grid,rotation=60)
    pl.legend(loc='upper left')
    pl.title(X)          
  
    pl.savefig('plots/'+prefix+X+'.png')
    pl.show()
    return 

 what_to_plot = [
    #    'booker_ipCountry',
    #     'product_departureAirport',
    #     'product_arrivalAirport',
    #     'booker_email_country',
    #     'booker_email_provider',
    #      'product_airline',        
         'product_typeOfFlight',
    ]
 
 
 
 for name in what_to_plot:    _plot_data(dataframe,name,prefix)

 return


def seaborn_plots_lm(dataframe,prefix='seaborn_lm_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
       # start with a rectangular Figure
        #mainFig = pl.figure(1, figsize=(12,12), facecolor='white')

        X=args['X']
        Y=args['Y']
        prefix=args['prefix']
        dataframe=args['dataframe']
        del args['X'],args['Y'],args['dataframe'],args['prefix']
        # linear analysis 
        sns.lmplot(X,Y,dataframe,**args)
        
        #sns.lmplot('airport_dist', 'product_price',dataframe,palette="Set1", fit_reg=True,hue='status')

        pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
        pl.show()
        return 

        #sns.jointplot('airport_dist', 'product_price',dataframe, kind="reg", color="seagreen")

    args={
        'dataframe':dataframe,
        'X':'airport_dist', 'Y':'product_price','prefix':'seaborn_lm_', 
        'palette':'Set1', 'fit_reg':True,'hue':'status'
        }
    what_to_plot = [    
                'airport_dist',
                'booker_depairport_dist',
                'booker_arrairport_dist',
                'traveller_avg_age',
                'Number_of_travellers',
                'LD_booker_payer',
                #'booker_valid_ip',                
                'product_price',
                'traveller_total_age',
                'booker_age',
                'LD_booker_first_traveller',
                'LD_booker_all_travellers',
                'traveller_avg_age',
                'booking_period',
                'score'
       ]
 

    for i,name in enumerate(what_to_plot):
        for j in range(i,len(what_to_plot)):
            args['X']=name
            args['Y']=what_to_plot[j]
            _plot_data(**args)

    return


def seaborn_plots_joint(dataframe,prefix='seaborn_joint_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
       # start with a rectangular Figure
        #mainFig = pl.figure(1, figsize=(12,12), facecolor='white')

        X=args['X']
        Y=args['Y']
        prefix=args['prefix']
        dataframe=args['dataframe']
        del args['X'],args['Y'],args['dataframe'],args['prefix']
        # linear analysis 

        # if Nan values, do some treatment 
        if any(pd.isnull(dataframe[X])): 
            print "X=",X
            import DataFrameImputer
            dataframe2 = DataFrameImputer.DataFrameImputer().fit(dataframe[X]).transform(dataframe[X])
            for x in dataframe2[X]: dataframe[x] = dataframe2[x]
        if any(pd.isnull(dataframe[Y])):  
            print "Y=",Y
            import DataFrameImputer
            dataframe2 = DataFrameImputer.DataFrameImputer().fit(dataframe[Y]).transform(dataframe[Y])
            for x in dataframe2[Y]: dataframe[x] = dataframe2[x]

        sns.jointplot(X,Y,dataframe,**args)          
        pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
        pl.show()
        return 


    args={
        'dataframe':dataframe,
        'X':'airport_dist', 'Y':'product_price','prefix':'seaborn_joint_', 
        'kind':'reg','color':'seagreen'
        }
    what_to_plot = [    
                'airport_dist',
                'booker_depairport_dist',
                'booker_arrairport_dist',
                'traveller_avg_age',
                'Number_of_travellers',
                'LD_booker_payer',
                #'booker_valid_ip',                
                'product_price',
                'traveller_total_age',
                'booker_age',
                'LD_booker_first_traveller',
                'LD_booker_all_travellers',
                'traveller_avg_age',
                'booking_period',
                'score'
       ]
 

    for i,name in enumerate(what_to_plot):
        for j in range(i+1,len(what_to_plot)):
            args['X']=name
            args['Y']=what_to_plot[j]
            _plot_data(**args)

    return

def seaborn_plots_dist(dataframe,prefix='seaborn_dist_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
       # start with a rectangular Figure
        #mainFig = pl.figure(1, figsize=(12,12), facecolor='white')

        sns.set(style="white", palette="muted")
        f, axes = pl.subplots(2, 2, figsize=(12, 12), sharex=True)
        b, g, r, p = sns.color_palette("muted", 4)
        sns.despine(left=True)

        X=args['X']
        prefix=args['prefix']
        dataframe=args['dataframe']
        del args['X'],args['dataframe'],args['prefix']
        OK =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,
                         [X]]
        NOTOK =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,
                         [X]]

        print X
        # if Nan values, do some treatment 
        if any(pd.isnull(OK)):  
            import DataFrameImputer
            dataframe = DataFrameImputer.DataFrameImputer().fit(OK).transform(OK)
            for x in OK: OK[x] = dataframe[x]
        if any(pd.isnull(NOTOK)): 
            import DataFrameImputer
            dataframe = DataFrameImputer.DataFrameImputer().fit(NOTOK).transform(NOTOK)
            for x in NOTOK: NOTOK[x] = dataframe[x]


        # distributution plot 
        sns.distplot(OK.as_matrix(),color=b,label='CHECKED_OK', ax=axes[0, 0],**args)
        sns.distplot(NOTOK.as_matrix(),color=r,label='CHECKED_NOT_OK', ax=axes[0, 1],**args)
        sns.distplot(NOTOK.as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, ax=axes[1, 0])
        sns.distplot(OK.as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel=X,ax=axes[1, 0])
        
       
        pl.savefig('plots/'+prefix+str(X)+'.png')
        pl.show()
        return 

      

    args={
        'dataframe':dataframe,
        'X':'airport_dist','prefix':'seaborn_dist_',
        'axlabel':'airport_dist'
        }
    what_to_plot = [ 'Number_of_travellers'   ]
    '''
    what_to_plot = [    
                'airport_dist',
                'booker_depairport_dist',
                'booker_arrairport_dist',
                'traveller_avg_age',
                'Number_of_travellers',
                'LD_booker_payer',
                'booker_valid_ip',                
                'product_price',
                'traveller_total_age',
                'booker_age',
                'LD_booker_first_traveller',
                'LD_booker_all_travellers',
                'traveller_avg_age',
                'booking_period',
                'score'
       ]
    '''

    for i,name in enumerate(what_to_plot):
        
            args['X']=name
            args['axlabel']=name
           
            
            _plot_data(**args)

    return


def seaborn_plots_correlation(dataframe,prefix='seaborn_corr_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
        sns.set_context("poster")

       # sns.set(style="white", palette="muted")
       # f, axes = pl.subplots(2, 1, figsize=(12, 12), sharex=True)
       # b, g, r, p = sns.color_palette("muted", 4)
       # sns.despine(left=True)

        X=args['X']
        prefix=args['prefix']
        dataframe=args['dataframe']

        del args['X'],args['dataframe'],args['prefix']
        OK =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,
                         X]
        NOTOK =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,
                         X]


        # correlation plot 

        sns.corrplot(OK)
        pl.savefig('plots/'+prefix+'CHECKED_OK.png')
        pl.show()
        sns.corrplot(NOTOK)        
        pl.savefig('plots/'+prefix+'CHECKED_NOT_OK.png')
        pl.show()
        return 

      

    args={
        'dataframe':dataframe,
        'X':'airport_dist','prefix':'seaborn_corr_'
        }
    what_to_plot = [    
            'airport_dist',
            'product_price',
            'traveller_first_age',
            'booking_period',
            'LD_booker_payer',
            'booker_ipCountry_payer_binCountry',
            'booker_valid_addr',
            'traveller_total_age'
            
            
       ]
 


    args['X']=what_to_plot
          

            
    _plot_data(**args)

    return


def seaborn_plots_factor_num(dataframe,prefix='seaborn_factor_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
   
        sns.set_context("talk", font_scale=1.25)
        X=args['X']
        Y=args['Y']
        prefix=args['prefix']
        dataframe=args['dataframe']
        xlabel = args['X']
        del args['X'],args['Y'],args['dataframe'],args['prefix']
        # linear analysis 
        dataframe['probability_fraud'] = dataframe['status'].apply(lambda x: 0 if x=='CHECKED_OK' else 1)
        g=sns.factorplot(X,Y,data=dataframe,**args)
        g.set_xticklabels(rotation=60)
        #pl.xticks(range(0,200,10)) 
        # xticks
        locs,labels = pl.xticks()
        pl.xticks(locs, map(lambda x: "%2.3f" % x, locs))
        pl.xlabel(xlabel)
        pl.title(xlabel)
        
        pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
        pl.show()
        return 


    args={
        'dataframe':dataframe,
        'X':'airport_dist', 'Y':'probability_fraud','prefix':'seaborn_factor_', 
        'hue':'product_typeOfFlight','size':10, 'aspect':2
        }
    what_to_plot = [
        'traveller_first_age', 
        #'product_price',
        'LD_booker_payer',
        #'booking_period',
        'LD_booker_all_travellers',
        'booker_valid_addr'
       ]
 

    for i,name in enumerate(what_to_plot):
            args['X']=name
            _plot_data(**args)

    return

def seaborn_plots_factor_text(dataframe,prefix='seaborn_factor_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
        sns.set_context("talk", font_scale=1.25)
        X=args['X']
        Y=args['Y']
        prefix=args['prefix']
        dataframe=args['dataframe']
        xlabel = args['X']
        del args['X'],args['Y'],args['dataframe'],args['prefix']
        # linear analysis 



        sns.set_context("poster")
        #fig=pl.figure(figsize=(8, 6))
        #fig = pl.figure(1, figsize=(10, 10))

        normalizer=Normalizer.NormalizerWrapper(type='normal')
        transformed_data=normalizer.fit_transform(dataframe[X].as_matrix())
        import operator
        #labels = sorted([(key,value) for key,value in normalizer.normalizer.transformed.items() ],key=operator.itemgetter(1))  
        dataframe[X] = dataframe[X].apply(lambda x: normalizer.transform(x))
        dataframe['probability_fraud'] = dataframe['status'].apply(lambda x: 0 if x=='CHECKED_OK' else 1)
        g=sns.factorplot(X,Y,data=dataframe,**args)
        g.set_xticklabels( [ normalizer.inverse_transform(i)for i in transformed_data ],rotation=90)

        locs,labels = pl.xticks()
        #pl.xticks(locs, map(lambda x: "%2.3f" % x, locs))
        pl.xticks(locs,fontsize = 12)
        pl.title(xlabel)
        pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
        pl.show()
        return 


    args={
        'dataframe':dataframe,
        'X':'airport_dist', 'Y':'probability_fraud','prefix':'seaborn_factor_text_', 
        'hue':'product_typeOfFlight','size':10, 'aspect':2
        }
    what_to_plot = [
        'product_arrivalAirport', 
        'product_departureAirport',
        'product_airline',
        
       ]
 

    for i,name in enumerate(what_to_plot):
            args['X']=name
            _plot_data(**args)

    return


def seaborn_plots_coeff(dataframe,prefix='seaborn_coeffplot_'):
    ''' to make correlation plots and linear model analysis '''
    
    def _plot_data(**args):  
 
   
        sns.set_context("poster")
        X=args['X']
        prefix=args['prefix']
        label=args['label']
        dataframe=args['dataframe']
        group = args['group']
        del args['X'],args['dataframe'],args['prefix'],args['label'],  args['group']

        dataframe['probability_fraud'] = dataframe['status'].apply(lambda x: 0 if x=='CHECKED_OK' else 1)

        # linear analysis 
        pl.rc("figure", figsize=(16, 10))
        if (group):        sns.coefplot(X, dataframe,group,**args)
        else:    sns.coefplot(X, dataframe,**args)
        pl.title(str(X).split('~')[0])
        pl.savefig('plots/'+prefix+str(X).split('~')[0]+label+'.png')
        pl.show()

        return 


    args={
            'dataframe':dataframe,
            'X':'airport_dist', 
            'prefix':'seaborn_coeffplot_', 
            'palette':'muted',
            'label':'label',
            #'group':'product_typeOfFlight',
            'group':None,            
            #'group':'booker_country',            
            'ci':68,       
            #'intercept':True
            'intercept':False
         }
    what_to_plot = [
         
         ('product_price ~   Number_of_travellers','num_of_trav'),       
         #('booker_age ~ product_price + traveller_avg_age + Number_of_travellers','full_mod'),
         #('probability_fraud ~ product_price + traveller_avg_age','_price_trav_avg_age'),
         ('probability_fraud ~ booker_valid_addr + LD_booker_all_travellers + booking_period ','full_mod_2'),
         ##('probability_fraud ~ product_airline + product_arrivalAirport','_airline_arrivalAirport'),
         #('booker_valid_addr ~ LD_booker_all_travellers + booking_period','full_mod_3'),
         #('booker_valid_addr ~ booker_country','full_mod_4'),
         ('product_price ~ booker_age','full_mod_5','booker_country'),
         #('product_price ~  booker_valid_addr','full_mod_6','booker_country'),
         ##('probability_fraud ~ booker_country','full_mod_7','product_typeOfFlight'),

         #('probability_fraud ~ booker_country * booker_age','full_mod_8'),
         ('probability_fraud ~ product_typeOfFlight*booker_country','full_mod_9'),
         #('booker_age ~ booker_valid_addr*booker_country','full_mod_10'),
         #('booker_age ~ booker_valid_addr','full_mod_11'),
         #('booker_age ~ booker_country','full_mod_12','booker_country',True),
         #('booker_age ~ booker_valid_addr','full_mod_13','booker_country',True),
         #('booker_age ~ booker_depairport_dist + booker_arrairport_dist','full_mod_14','booker_country',True),
         #('booker_age ~ booker_depairport_dist + booker_arrairport_dist','full_mod_15','',True),
         #('booker_depairport_dist ~ booker_country + booker_arrairport_dist','full_mod_16','',True),
         #('booker_depairport_dist ~ booker_country','full_mod_17','',True),
         #('booker_depairport_dist ~ booker_arrairport_dist','full_mod_18','',True),
         #('booker_depairport_dist ~ booker_arrairport_dist','full_mod_19','product_typeOfFlight',True),
         #('booker_depairport_dist ~ booker_age','full_mod_20','product_typeOfFlight',True),
         #('booker_depairport_dist ~  booker_age','full_mod_21','',True),
         #('booker_arrairport_dist ~  booker_age','full_mod_22','',True),
         #('traveller_avg_age ~  Number_of_travellers*booker_age','full_mod_23','',True),
         #('traveller_avg_age ~  airport_dist','full_mod_24','',True),
         #('traveller_avg_age ~  booker_country','full_mod_25',''),
         #('booker_age ~   product_price','full_mod_26','booker_country'),
         #('probability_fraud ~   product_price','full_mod_27','booker_country'),
         #('probability_fraud ~   booker_age*booking_period','full_mod_28','booker_country'),
         #('probability_fraud ~   booker_age*booking_period','full_mod_29',''),
         #('probability_fraud ~    booker_depairport_dist+ booker_depairport_dist / booker_arrairport_dist + booker_arrairport_dist','full_mod_30',''),
         #('probability_fraud ~    booker_valid_addr+ booker_valid_addr*booker_country  + booker_country','full_mod_31',''),
         #('probability_fraud ~   product_airline','full_mod_32',''),
         #('probability_fraud ~   booker_age','full_mod_33','product_airline'),
         #('booker_valid_addr ~   probability_fraud+probability_fraud*booker_age + booker_age','full_mod_34',''),
         #('booker_valid_addr ~   probability_fraud+probability_fraud*booker_age + booker_age','full_mod_35','product_airline'),
         #('product_price ~ booker_depairport_dist + booker_arrairport_dist','full_mod_7','booker_country'),
         #('booking_period ~ booker_depairport_dist + booker_arrairport_dist + booker_age + product_price','full_mod_36',''),
         #('booking_period ~ booker_depairport_dist + booker_arrairport_dist + booker_age + product_price + product_price ','full_mod_37','product_airline'),
         #('booking_period ~ booker_depairport_dist + booker_arrairport_dist + booker_age + product_price + product_airline','full_mod_38',''),

        
       ]
 


    for i,name in enumerate(what_to_plot):
            args['X']=name[0]
            args['label']=name[1]
            
            if (len(name)>2):  
                if (len(name[2])>0):
                    args['group']=name[2]
                    if 'ci' in args: del args['ci']
                else: 
                    args['group']=None
                    args['ci']=68
            if (len(name)>3):  args['intercept']=name[3]
            try:
                _plot_data(**args)
            except: pass
    return

def build_plots(dataframe,type=0):
 
 plots = {
            0: [
                  lambda x: continues_plots(x),
                  lambda x: binary_plots(x),
                  lambda x: categ_num_plots(x),
                  lambda x: categ_text_plots(x)
                ], # plot all data
            1: [lambda x: continues_plots(x)], # plot continues data
            2: [lambda x: binary_plots(x)], # plot binary data
            3: [lambda x: categ_num_plots(x)], # plot categorical numeric data
            4: [lambda x: categ_text_plots(x)], # plot categorical text data
            5: [lambda x: seaborn_plots_lm(x)], # plot using seaborn
            6: [lambda x: seaborn_plots_joint(x)], # plot using seaborn
            7: [lambda x: seaborn_plots_dist(x)], # plot using seaborn
            8: [lambda x: seaborn_plots_correlation(x)], # plot using seaborn
            9: [lambda x: seaborn_plots_factor_num(x)], # plot using seaborn
            10: [lambda x: seaborn_plots_factor_text(x)], # plot using seaborn
            11: [lambda x: seaborn_plots_coeff(x)], # plot using seaborn

         }

 if type in plots: 
    for item in plots[type]: item(dataframe)
  
 return  


if __name__ == '__main__':

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
 from StringIO import StringIO
 import prettytable 
 import argparse
 import sys 
 
 parser = argparse.ArgumentParser(description='Plot FPS data')
 parser.add_argument('-t','--type', help='type of the plottings',default=0, required=False, type=int)
 args = parser.parse_args()
 build_plots(dataframe, **vars(args) )
 
 sys.exit(1)
 
 
 
 
 ## Test 2: check normalization of the last kde
 #f = lambda x: va.likelihood_at_point(x,x_grid,pdf)
 #print "Integral1:", VariableAnalysis.integrate(f,min(vals),max(vals),len(x_grid),VariableAnalysis.simpson)
 #f = lambda x: va.likelihood_at_point(x,x_grid2,pdf2)
 #print "Integral2:", VariableAnalysis.integrate(f,min(vals2),max(vals2),len(x_grid2),VariableAnalysis.simpson)
 
 
 '''
  

 # Test 5:   plot some categorical-text distribution
 
 '''

 # Test 6:    plot 2D scatter plot of numerical data
 # plot 8:   'product_price' and 'airport_dist'
 X=dataframe['airport_dist'].as_matrix()
 Y=dataframe['product_price'].as_matrix()
 va=VariableAnalysis('airport_dist',dataframe,'float')
 va.scatter_plot(('airport_dist',X),('product_price',Y))
 pl.show()

 # Test 7:    plot 2D scatter plot of 'categoticaltext' data
 # plot 9:  
 import Normalizer
 X=dataframe['airport_dist'].as_matrix()
 Y= dataframe['product_typeOfFlight']
 Y=Normalizer.NormalizerWrapper(type='normal').fit_transform(Y.as_matrix())
 
 va=VariableAnalysis('airport_dist',dataframe,'float')
 va.scatter_plot(('airport_dist',X),('product_typeOfFlight',Y),bins=50)
 pl.show()

 # Test 8
 # plot 10:   plot all combinations
 '''
 labels = [ 
            'airport_dist',
            'product_price',
            'traveller_first_age',
            'booking_period',
            'LD_booker_payer',
            'booker_ipCountry_payer_binCountry',
            'booker_valid_addr',
            'product_typeOfFlight',
            'product_airline',
            'product_arrivalAirport',
            'product_departureAirport'
          ]
 

 for i in range(len(labels)):
  for j in range(i+1,len(labels)):
   #print labels[i], dataframe[labels[i]].dtype
   #print labels[j], dataframe[labels[j]].dtype
   #if ((dataframe[labels[i]].dtype == np.dtype('float')) or (dataframe[labels[i]].dtype == np.dtype('int'))):
   if (dataframe[labels[i]].dtype != np.dtype('object')):
    X=dataframe[labels[i]].as_matrix() 
    xbins = 100
   else:
    X= dataframe[labels[i]]
    X=Normalizer.NormalizerWrapper(type='median').fit_transform(X.as_matrix())
    xbins = 25
   #if ((dataframe[labels[j]].dtype == np.dtype('float')) or (dataframe[labels[j]].dtype == np.dtype('int'))):
   if (dataframe[labels[j]].dtype != np.dtype('object')):
    Y=dataframe[labels[j]].as_matrix() 
    ybins = 100
   else:
    Y=dataframe[labels[j]]
    Y=Normalizer.NormalizerWrapper(type='normal').fit_transform(Y.as_matrix())
    ybins = 25
   
   va=VariableAnalysis('airport_dist',dataframe,'float')
   va.scatter_plot((labels[i],X),(labels[j],Y),bins=min(xbins,ybins))
   pl.show()

 # Test 9
 # plot 11:   plot all combinations for 'OK'
 
 labels = [ 
            'airport_dist',
            'product_price',
            'traveller_first_age',
            'booking_period',
            'LD_booker_payer',
            'booker_ipCountry_payer_binCountry',
            'booker_valid_addr',
            'product_typeOfFlight',
            'product_airline',
            'product_arrivalAirport',
            'product_departureAirport'
          ]
 

 for i in range(len(labels)):
  for j in range(i+1,len(labels)):
   #print labels[i], dataframe[labels[i]].dtype
   #print labels[j], dataframe[labels[j]].dtype   
   X=dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,labels[i]].as_matrix()
   Y=dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,labels[j]].as_matrix()
   if (dataframe[labels[i]].dtype != np.dtype('object')):    
    xbins = 100
   else:    
    X=Normalizer.NormalizerWrapper(type='median').fit_transform(X)
    xbins = 25   
   if (dataframe[labels[j]].dtype != np.dtype('object')):    
    ybins = 100
   else:
    Y=Normalizer.NormalizerWrapper(type='normal').fit_transform(Y)
    ybins = 25
   
   va=VariableAnalysis('airport_dist',dataframe,'float')
   va.scatter_plot((labels[i],X),(labels[j],Y),bins=min(xbins,ybins))
   pl.show()
 
 # plot 12:   plot all combinations for 'CHECKED_NOT_OK'
 for i in range(len(labels)):
  for j in range(i+1,len(labels)):
   #print labels[i], dataframe[labels[i]].dtype
   #print labels[j], dataframe[labels[j]].dtype   
   X=dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,labels[i]].as_matrix()
   Y=dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,labels[j]].as_matrix()
   if (dataframe[labels[i]].dtype != np.dtype('object')):    
    xbins = 100
   else:    
    X=Normalizer.NormalizerWrapper(type='median').fit_transform(X)
    xbins = 25   
   if (dataframe[labels[j]].dtype != np.dtype('object')):    
    ybins = 100
   else:
    Y=Normalizer.NormalizerWrapper(type='normal').fit_transform(Y)
    ybins = 25
   
   va=VariableAnalysis('airport_dist',dataframe,'float')
   va.scatter_plot((labels[i],X),(labels[j],Y),bins=min(xbins,ybins))
   pl.show()
 
 '''

 # Test 10: using linear Model Analysis in Seaborn
 import seaborn as  sns
 '''
 
 # linear analysis of all data
 sns.lmplot('airport_dist', 'product_price',dataframe)
 pl.show()

 sns.lmplot('airport_dist', 'product_price',dataframe,col='booker_ipCountry_payer_binCountry')
 pl.show()

 
 #sns.lmplot('airport_dist', 'product_price',dataframe,hue='product_arrivalAirport')
 #pl.show() 

 sns.jointplot('airport_dist', 'product_price',dataframe, kind="reg", color="seagreen")
 pl.show()
 
 labels = [ 
            'airport_dist',
            'product_price',
            'traveller_first_age',
            'booking_period',
            'LD_booker_payer',
            'booker_ipCountry_payer_binCountry',
            'booker_valid_addr',
#            'product_typeOfFlight',
#            'product_airline',
#            'product_arrivalAirport',
#            'product_departureAirport'
          ]
 
# sns.corrplot(dataframe[labels])
# pl.show()

 for i in range(len(labels)):
    str ='%s ~ '%labels[i]        
    for j in range(i+1,len(labels)):
        str=str + ' %s'%labels[j] + "+"
    str=str[:-1]
    print str
    if i<len(labels)-1:
        sns.coefplot(str, dataframe[labels]) 
        pl.show()

 '''
 #sns.lmplot('traveller_first_age', 'booker_ipCountry_payer_binCountry',dataframe,palette="Set1", fit_reg=False,hue='status')
 #sns.lmplot('traveller_first_age', 'LD_booker_payer',dataframe,palette="Set1", fit_reg=False,hue='status')
 
 #sns.pairplot(dataframe[['status','airport_dist',
 #           'product_price',
 #           'traveller_first_age',
 #           'LD_booker_payer',
 #           'booker_ipCountry_payer_binCountry',
 #           'booker_valid_addr',
 #           'product_typeOfFlight',
 #           'product_airline',
 #           'product_arrivalAirport',
 #           'product_departureAirport'
 #           ]], "status", size=2.5);
 #sns.lmplot('airport_dist', 'product_price',dataframe,palette="Set1", fit_reg=True,hue='status')
 dataframe2 = dataframe[['status','airport_dist',
            'product_price',
            'traveller_first_age',
            'LD_booker_payer',
            'booker_ipCountry_payer_binCountry',
            'booker_valid_addr',
            'product_typeOfFlight',
            'product_airline',
            'product_arrivalAirport',
            'product_departureAirport',
            'booking_period'
            ]]
 dataframe2['status'] = dataframe2['status'].apply(lambda x: 0 if x=='CHECKED_OK' else 1)
 '''
 g=sns.factorplot("traveller_first_age",'status' , hue='product_typeOfFlight',           data=dataframe2)
 g.set_xticklabels(rotation=60)
 pl.show()
# sns.factorplot('product_price','status' , hue='product_typeOfFlight',           data=dataframe2)
# pl.show()
 g=sns.factorplot('LD_booker_payer','status' , hue='product_typeOfFlight',           data=dataframe2)
 g.set_xticklabels(rotation=60)
 pl.show()
 #g=sns.factorplot('booking_period','status' , hue='product_typeOfFlight',           data=dataframe2)
 #g.set_xticklabels(rotation=60)
 #pl.show()
 
 normalizer=Normalizer.NormalizerWrapper(type='normal')
 transformed_data=normalizer.fit_transform(dataframe2['product_airline'].as_matrix())
 print normalizer.inverse_transform(0.674) 
 dataframe2['product_airline'] = dataframe2['product_airline'].apply(lambda x: normalizer.transform(x))
 g=sns.factorplot('product_airline','status' , hue='product_typeOfFlight',           data=dataframe2)
 g.set_xticklabels( [ normalizer.inverse_transform(i)for i in transformed_data ],rotation=60)
 pl.show()
 '''

 
 '''
 from sklearn import preprocessing
 transformers = [preprocessing.LabelEncoder(),preprocessing.StandardScaler()]
 transformed_data=transformers[1].fit_transform(transformers[0].fit_transform(dataframe2['product_departureAirport'].as_matrix()) )
 normalizer = lambda x: transformers[1].transform([transformers[0].transform(x)])[0]
 #normalizer = lambda x: transformers[0].transform(x)
 normalizer_inverse = lambda x: transformers[0].inverse_transform(int(transformers[1].inverse_transform(x)))


 import operator
 def get_labels_codes(collection,normalizer):
    D = {}
    for item in collection:
      if item not in D: D[item] = normalizer(item)
    labels = sorted([(key,value) for key,value in D.items() ],key=operator.itemgetter(1))
    return labels
 
 labels =  get_labels_codes(dataframe2['product_departureAirport'].as_matrix(),normalizer)
 dataframe2['product_departureAirport'] = dataframe2['product_departureAirport'].apply(lambda x: normalizer(x))
 g=sns.factorplot('product_departureAirport','status' , hue='product_typeOfFlight',           data=dataframe2)
 g.set_xticklabels( [x[0] if x[0] in ['LGA','EBL','KAN'] else '' for x in labels],rotation=60)
 pl.show()
 

 normalizer=Normalizer.NormalizerWrapper(type='normal')
 transformed_data=normalizer.fit_transform(dataframe2['product_departureAirport'].as_matrix())
 import operator
 labels = sorted([(key,value) for key,value in normalizer.normalizer.transformed.items() ],key=operator.itemgetter(1))
 dataframe2['product_departureAirport'] = dataframe2['product_departureAirport'].apply(lambda x: normalizer.transform(x))
 g=sns.factorplot('product_departureAirport','status' , hue='product_typeOfFlight',         data=dataframe2)
 #g.set_xticklabels( [x[0] if x[0] == 'VIE' else '' for x in labels],rotation=60)
 g.set_xticklabels( [x[0] for x in labels],rotation=60)
 pl.show()

 
 #normalizer=Normalizer.NormalizerWrapper(type='normal')
 #transformed_data=normalizer.fit_transform(dataframe2['product_arrivalAirport'].as_matrix())
 #import operator
 #labels = sorted([(key,value) for key,value in normalizer.normalizer.transformed.items() ],key=operator.itemgetter(1))
 dataframe2['product_arrivalAirport'] = dataframe2['product_arrivalAirport'].apply(lambda x: normalizer.transform(x))
 g=sns.factorplot('product_arrivalAirport','status' , hue='product_typeOfFlight',         data=dataframe2)
 g.set_xticklabels( [x[0] for x in labels],rotation=60)
 pl.show()
 '''
 
 
