'''
	This is a Cluster Class designed to make data clusters
'''

from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import copy 
import TransformDataset
import Dataset
from sklearn import preprocessing
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA,TruncatedSVD


class Cluster(object):
 '''
	Cluster Class ...
 '''

 def __init__(self,dataframe,processing=None):

        self.dataframe = dataframe
        self.processing =processing
        self.cluster=None
        pass

 def preprocess (self):

    if (self.processing is not None):
        for transform in self.processing:            
            name = transform[0]
            #print name
            data=self.dataframe[name]
            pipeline = transform[1]
            for item in pipeline:
                data = item.fit_transform(data)                
                self.dataframe[name] = self.dataframe[name].apply(lambda x: item.transform([x])[0][0] if type(item.transform([x])[0]) == type([]) else  item.transform([x])[0] )                            
                print self.dataframe[name].head()
                print 'OK1'
           
    return self

 def inverse_preprocess (self,transforms=None):
        
    if (transforms is  None): transforms = self.processing
    if (transforms is not None):
        
        for transform in transforms:
            name = transform[0]
            pipeline = transform[1]            
            for item in reversed(pipeline): 
                print item
                #self.dataframe[name] = self.dataframe[name].apply(lambda x: item.inverse_transform(x)  )                
                self.dataframe[name] = self.dataframe[name].apply(lambda x: item(x)  )                
#                print self.dataframe[name].head()
#                print 'OK2'
           
            

    return self



 def createCluster (self,categories,numclust=2,dataframe=None):

    
    if (dataframe is None): dataframe = self.dataframe

    self.cluster = KMeans(n_clusters=numclust)
    self.cluster.fit(dataframe[categories])
    return self


 def pca(self,categories,dataframe=None):
    if (dataframe is None): dataframe = self.dataframe
    X_pca = PCA().fit_transform(dataframe[categories])

    for i,name in enumerate(categories):
        self.dataframe[name] = X_pca[:,i]

    return self



 def truncated_svn(self,categories,dataframe=None):
    if (dataframe is None): dataframe = self.dataframe
    X_pca = TruncatedSVD(n_components=len(categories)-1).fit_transform(dataframe[categories])

    for i,name in enumerate(categories):
        self.dataframe[name] = X_pca[:,i]

    return self


if __name__ == '__main__':

 '''
 # Test 1:  Test of the data cluster processing
 ds = Dataset.Dataset() # create dataset

 ds.loadJson("fps_data_1.json")  # load data
 df= ds.createDataFrame() # create dataframe

 #print ds.listStatuses()
 # gives [u'CHECKED_OK', u'DENIED_PAYMENTERROR', u'CHECKED_NOT_OK', u'DENIED_DOUBLE', u'DENIED_NOULA', u'OPEN', u'DENIED_OTHER']
 df = df.drop(df[df.status == 'DENIED_PAYMENTERROR'].index) # drop not needed rows
 df = df.drop(df[df.status == 'DENIED_DOUBLE'].index) # drop not needed rows
 df = df.drop(df[df.status == 'DENIED_NOULA'].index) # drop not needed rows
 df = df.drop(df[df.status == 'OPEN'].index) # drop not needed rows
 df = df.drop(df[df.status == 'DENIED_OTHER'].index) # drop not needed rows


 # transform dataframe
 td = TransformDataset.TransformDataset(df)
 td.imputtingColumns([
    'booker_ipCountry', 
    'booker_country',
    'payer_binCountry',
    'product_departureAirport',
    'product_arrivalAirport',
    ]).addBooking_period().addLevenshtein().addNumberTravellers().addCountryMatches().addRoute().addBookerEmailInformation()
 
print "Modification has been finished"
# list of interesting data columns
tosave=['status',
    'booker_ipCountry',
    'booker_country',
    'payer_binCountry',
    'product_departureAirport',
    'product_arrivalAirport',
    'booker_email_country',
    'booker_email_provider', 'booker_ipCountry_country', 'booker_country_payer_binCountry','booker_ipCountry_payer_binCountry',
    'LD_booker_payer','booking_period','Number_of_travellers','isHit','score']

from StringIO import StringIO
import prettytable    
output = StringIO()
print td.dataframe[tosave].head()
#td.dataframe[tosave].to_csv(output)
#output.seek(0)
#pt = prettytable.from_csv(output)
#print >> open("cluster_data_orig.log","w"), pt   ## for debugging

# create a processing pipeline
processing = [
    ('booker_ipCountry',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]),
    ('product_departureAirport',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()])
]


cluster = Cluster(td.dataframe[tosave],processing)
cluster.preprocess()
print cluster.dataframe.head()
#output = StringIO()
#cluster.dataframe.to_csv(output)
#output.seek(0)
#pt = prettytable.from_csv(output)
#print >> open("cluster_data_transformed.log","w"), pt   ## for debugging



# Test 2: create clusters and test them
cluster.createCluster(['booker_ipCountry','product_departureAirport'])
centroids = cluster.cluster.cluster_centers_
print centroids

(target, ipCountry, departureAirport) = (cluster.dataframe['status'].get_values(),cluster.dataframe['booker_ipCountry'].get_values(),cluster.dataframe['product_departureAirport'].get_values())
for i in range(len(target)):
 print "target=",target[i],'predict=',cluster.cluster.predict([ipCountry[i],departureAirport[i]])
 if i >10: break



# Test 3: Test of inverse transformation
## some hack for the inverse_transform
f1 = lambda x: processing[0][1][0].inverse_transform(int(processing[0][1][1].inverse_transform(x)))
f2 = lambda x: processing[1][1][0].inverse_transform(int(processing[1][1][1].inverse_transform(x)))

cluster.inverse_preprocess(
[
        ('booker_ipCountry',[f1]),
        ('product_departureAirport',[f2])
]
)
print cluster.dataframe.head()
''' 




