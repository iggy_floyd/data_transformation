#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 17, 2015 3:38:10 PM$"

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn import cross_validation    
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
import Normalizer
import sklearn_roc_curves as skroc
import sklearn_optimizer
import time
import os.path
from sklearn.externals import joblib
import interpolate_histogram as ih
import binary_transformer as bt

import scipy
from sklearn.metrics import roc_curve, auc,precision_recall_curve



import dill
from sklearn.cross_validation import StratifiedKFold


class ensemble(object):
    def __init__(self,df,features,target,n_clf=4):
        self.df=df
        self.target=target
        self.features = features
        self.n_clf = n_clf
        self.train_cols=[feature[0] for feature in self.features]        
        self.train_cols.remove(target)
        self.available_trains=n_clf
        self.clfs=[]
        
        y =  self.df[target].values
        
        #print self.df.loc[self.df[self.df[target] == 0].index,:].index.values
        self.cv = StratifiedKFold(y, n_folds=self.n_clf) # cross-validation  tool
        self.datasample_inds= []
        for i, (train, test) in enumerate(self.cv): 
        #    print "train=",train
        #    if i == 0: 
        #        self.available_trains -= 1
        #        continue
            self.datasample_inds +=[(train,test)]
            

    def get_dataframe_for_classifier(self):
        self.available_trains -= 1
        if self.available_trains< 0: return 
        #print "available_trains=",self.available_trains
        #print sum(self.datasample_inds[self.available_trains][0])
        
        #return self.df[self.train_cols]
        #return self.df.ix[self.datasample_inds[self.available_trains][0],[self.target]+self.train_cols]
        return self.df.ix[self.datasample_inds[self.available_trains][0],:]
    

    def add_classifier(self,clf):
        self.clfs+=[clf]
        return

    def predict(self,X): # assume that X=[[...],[...],...]
        
        def chage_target_sign(x):  
            return 1 if x>0 else -1
        
        
        chage_target_sign_vec = np.vectorize(chage_target_sign)
        
        
        def inverse_chage_target_sign(x):
            return 1 if x>0 else 0

        inverse_chage_target_sign_vec = np.vectorize(inverse_chage_target_sign)
        
        
        total_pred=np.zeros(len(X))
        
        
        for clf in self.clfs:
            pred=clf.predict(X) ## assume that preds is np.array()
            pred = chage_target_sign_vec(pred)
            
            total_pred += pred
        return inverse_chage_target_sign_vec(pred)
    

class WrapperEnsemble(object):
    
    
    @staticmethod
    def myinit(df,features,target):
        WrapperEnsemble.df = df
        WrapperEnsemble.features = features
        WrapperEnsemble.target = target
        WrapperEnsemble.transformators={}
        WrapperEnsemble.train_cols = []
        
        
        #prepare transformation list
        for feature in WrapperEnsemble.features:
         name_feature = feature[0]
         if name_feature == target: continue
         
         WrapperEnsemble.train_cols += [name_feature]
         transformator = feature[1]    
        
         if transformator:
             if ('function' in str(type(transformator))):                      
                 WrapperEnsemble.transformators[name_feature] =('func',transformator)

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(WrapperEnsemble.df[name_feature].as_matrix().tolist()) 
                 WrapperEnsemble.transformators[name_feature] =('class',transformator)
         else:        
                WrapperEnsemble.transformators[name_feature] =('None',lambda x: x)
        return
    
    def __init__(self,clf,predict_function=None,predict_proba_function=None,fit_function=None,binary_transformer=None):
        self.clf=clf
        self.predict_function=predict_function
        self.predict_proba_function=predict_proba_function
        self.fit_function=fit_function
        self.binary_transformer = binary_transformer
        
    def fit(self, X, y):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(WrapperEnsemble.train_cols):            

            transformation = WrapperEnsemble.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                #transformation[1](X[i].tolist())                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]

        X=np.array(transformed).T # transpose it back
        
        if (self.binary_transformer is not None): 
            def digitization(a):
                return [ self.binary_transformer.digitization_value(x,i) for i,x in enumerate(a)]
            X=np.apply_along_axis(digitization, 1, X)
        
            X=self.binary_transformer.binary_enc.transform(X).toarray()
        
        if len(y.shape)>1: y=y.flatten()
        
        if (self.fit_function is None): self.clf.fit(X,y)
        else: self.fit_function(self.clf,X,y)
        
        
        
        return self
    
    def predict(self, X):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(WrapperEnsemble.train_cols):            

            transformation = WrapperEnsemble.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            

        X=np.array(transformed).T # transpose it back
        if (self.binary_transformer is not None): 
            
            def digitization(a):
                return [ self.binary_transformer.digitization_value(x,i) for i,x in enumerate(a)]
            X=np.apply_along_axis(digitization, 1, X)        
            X=self.binary_transformer.binary_enc.transform(X).toarray()
        

        
        if (self.predict_function is None):  return self.clf.predict(X)
        else: return self.predict_function(self.clf,X)

        
    
    def predict_proba(self, X):
        
        # transform X values. Assume X is a list of type:  [[...],[...],[...]]
        X=np.array(X).T # transpose it
        transformed = []
        
        for i,name in enumerate(WrapperEnsemble.train_cols):            

            transformation = WrapperEnsemble.transformators[name]
            
            if ('func' in transformation[0]): transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            elif ('class' in transformation[0]): transformed += [np.array(
                map(lambda x: transformation[1].transform([x])[0],X[i].tolist())
                
                ).tolist()]
            else: transformed += [np.array(transformation[1](X[i].tolist())).tolist()]
            

        X=np.array(transformed).T # transpose it back
        if (self.binary_transformer is not None): 
            def digitization(a):
               return [ self.binary_transformer.digitization_value(x,i) for i,x in enumerate(a)]
            X=np.apply_along_axis(digitization, 1, X)        
            X=self.binary_transformer.binary_enc.transform(X).toarray()

        
        if (self.predict_proba_function is None):  return self.clf.predict_proba(X)
        else: return self.predict_proba_function(self.clf,X)
        
        



if __name__ == "__main__":
 

# Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    #('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    #('booker_age',preprocessing.StandardScaler()), # bad 
    #('product_price',preprocessing.MinMaxScaler()), # bad 
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!       
    ]

 # Test 1: ensemble create
 dataframe['status'] =    dataframe['status'].apply(lambda x: features[0][1](x))
 
 
 en = ensemble(dataframe[ [feature[0] for feature in features] ],features,'status',n_clf=4)
 
 # Test 2: test difference of the assigned three dataframes
 print "df1"
 df1=en.get_dataframe_for_classifier()
 print df1.describe()
 
 print "\n\ndf2\n\n"
 df2=en.get_dataframe_for_classifier()
 print df2.describe()
 print "\n\ndf3\n\n"
 df3=en.get_dataframe_for_classifier()
 print df3.describe()



# Test 3: create classifiers and train them on df1, df2, df3
 WrapperEnsemble.myinit(dataframe[ [feature[0] for feature in features] ],features,'status')

 from libnb import TwoClassNB

 nb = TwoClassNB.TwoClassNB(None, 0,1)
 def fit_function_nb(clf,X,y):
    #print "X=",X
    #print "y=",y
    #print np.c_[y,X]
    clf.dataset = np.c_[y,X]
    clf.splitDataset(0.81)
    return 

 def predict_function_nb(clf,X):
    #print "X=",X
    #print map(lambda x: int(clf.predict(x)[0]>0),X )    
    return np.array(map(lambda x: int(clf.predict(x)[0]>0),X ))
    
 def predict_proba_function_nb(clf,X):
    #return clf.predict(X.tolist()[0])[1]
    return np.array(map(lambda x: clf.predict(x)[1],X ))

 wnb=WrapperEnsemble(nb,predict_function=predict_function_nb,
    predict_proba_function=predict_proba_function_nb,fit_function=fit_function_nb)

 #X_train_nb,X_test_nb,y_train_nb, y_test_nb = cross_validation.train_test_split(
 #    df1[WrapperEnsemble.train_cols].as_matrix(), df1['status'].as_matrix(), test_size=0.1, random_state=0)    


 X_train_nb =  df1[WrapperEnsemble.train_cols].as_matrix()
 y_train_nb =  df1[['status']].as_matrix()
 train_nb =df1[[feature[0] for feature in features]].as_matrix()
 wnb.fit(X_train_nb,y_train_nb)

 #print wnb.predict(X_train_nb[:20]),'*'*5, y_train_nb[:20], '*'*5,  wnb.predict(X_train_nb[:20]) == y_train_nb[:20]


 nb2 = TwoClassNB.TwoClassNB(None, 0,1)
 wnb2=WrapperEnsemble(nb2,predict_function=predict_function_nb,
    predict_proba_function=predict_proba_function_nb,fit_function=fit_function_nb)
 wnb2.fit(df2[WrapperEnsemble.train_cols].as_matrix(),df2[['status']].as_matrix())


 nb3 = TwoClassNB.TwoClassNB(None, 0,1)
 wnb3=WrapperEnsemble(nb3,predict_function=predict_function_nb,
    predict_proba_function=predict_proba_function_nb,fit_function=fit_function_nb)
 wnb3.fit(df3[WrapperEnsemble.train_cols].as_matrix(),df3[['status']].as_matrix())


 def accuracy(clf,X,y):
    preds = clf.predict(X)
    y=y.flatten()
    print 'prediction accuracy: %.4f' % (1 - (1. / len(y) * sum( preds != y )))

 #from functools import partial
 #pred1=partial(predict_function_nb,nb)

 #accuracy(wnb,X_train_nb[1:100],y_train_nb[1:100])
 #accuracy(wnb2,df2[WrapperEnsemble.train_cols].as_matrix()[1:20],df2[['status']].as_matrix()[1:20])
 #accuracy(wnb3,df3[WrapperEnsemble.train_cols].as_matrix()[1:20],df3[['status']].as_matrix()[1:20])
 #accuracy(wnb3,X_train_nb[1:100],y_train_nb[1:100])



 en.add_classifier(wnb)
 en.add_classifier(wnb2)
 en.add_classifier(wnb3)

 #accuracy(en,X_train_nb[1:20],y_train_nb[1:20])


# Test4 with sklearn classifiers
 from sklearn.naive_bayes import GaussianNB

 en = ensemble(dataframe[ [feature[0] for feature in features] ],features,'status',n_clf=5)
 df1=en.get_dataframe_for_classifier()
 df2=en.get_dataframe_for_classifier()
 df3=en.get_dataframe_for_classifier()
 df4=en.get_dataframe_for_classifier()
 df5=en.get_dataframe_for_classifier()


 nb1 = GaussianNB()
 wnb1=WrapperEnsemble(nb1,predict_function=None,predict_proba_function=None,fit_function=None)
 wnb1.fit(df1[WrapperEnsemble.train_cols].as_matrix(),df1[['status']].as_matrix())

 accuracy(wnb1,X_train_nb,y_train_nb)


 nb2 = GaussianNB()
 wnb2=WrapperEnsemble(nb2,predict_function=None,predict_proba_function=None,fit_function=None)
 wnb2.fit(df2[WrapperEnsemble.train_cols].as_matrix(),df2[['status']].as_matrix()) 


 nb3 = GaussianNB() 
 wnb3=WrapperEnsemble(nb3,predict_function=None,predict_proba_function=None,fit_function=None)
 wnb3.fit(df3[WrapperEnsemble.train_cols].as_matrix(),df3[['status']].as_matrix())



 en.add_classifier(wnb1)
 en.add_classifier(wnb2)
 en.add_classifier(wnb3)

 accuracy(en,X_train_nb,y_train_nb)


 # Test5 add binary predictor

 from sklearn.naive_bayes import BernoulliNB
 import binary_transformer as bt

 dataframe2 = pd.read_csv(process_name+'_transformed.csv')
 df_binary=dataframe2[ [feature[0] for feature in features] ]
 binary_transformer=bt.binary_transformer(df_binary,features,bins=100).transform_dataframe().digitization('status').transform_dataframe_binary('status')

 nb4 = BernoulliNB()
 wnb4=WrapperEnsemble(nb4,predict_function=None,predict_proba_function=None,fit_function=None,binary_transformer=binary_transformer)
 wnb4.fit(df4[WrapperEnsemble.train_cols].as_matrix(),df4[['status']].as_matrix())

 nb5 = BernoulliNB()
 wnb5=WrapperEnsemble(nb5,predict_function=None,predict_proba_function=None,fit_function=None,binary_transformer=binary_transformer)
 wnb5.fit(dataframe[WrapperEnsemble.train_cols].as_matrix(),dataframe[['status']].as_matrix())


 en.add_classifier(wnb4)
 en.add_classifier(wnb5)


 accuracy(en,X_train_nb,y_train_nb)


 def accuracy_v2(clf,X,y):
    preds = clf.predict(X)
    y=y.flatten()
    print 'prediction accuracy: %.4f' % (1 - (1. / len(y) * sum( preds != y )))
    true=(preds == y)
    poses=(y == 1)
    negs=(y == 0)
    print "True OK: %.4f"%(sum(true*poses)*1./sum(poses))
    print "True NOT_OK: %.4f"%(sum(true*negs)*1./sum(negs))

 accuracy_v2(en,X_train_nb,y_train_nb)



 from libsvm.python import svm,svmutil

 class _svm(object):
         def __init__(self): self.model=None
        
         def fit(self,X,y):
             
             self.problem = svm.svm_problem(y.tolist(),X.tolist())
             
             self.param = svm.svm_parameter('-s 1 -t 2 -n 0.1') 
             self.model = svmutil.svm_train(self.problem, self.param)
         
         def predict(self,X):  
             
             y=np.zeros(np.array(X).shape[0]).tolist()
             X=X.tolist()
             #a,b,c= svmutil.svm_predict([y],[X],self.model,options='-b 1')
             a,_,_= svmutil.svm_predict(y,X,self.model,options='-q')             
             
             return np.array(a)
         
         def predict_proba(self,X):  
             y=np.zeros(np.array(X).shape[0]).tolist()
             X=X.tolist()
             return svmutil.svm_predict(y,X,self.model)[2]
         

 # Test 6: using SVM from libsvm
 svm1 = _svm()
 wsvm1=WrapperEnsemble(svm1,predict_function=None,predict_proba_function=None,fit_function=None)
 wsvm1.fit(df2[WrapperEnsemble.train_cols].as_matrix(),df2[['status']].as_matrix())

 svm2 = _svm()
 wsvm2=WrapperEnsemble(svm2,predict_function=None,predict_proba_function=None,fit_function=None,binary_transformer=binary_transformer)
 wsvm2.fit(df3[WrapperEnsemble.train_cols].as_matrix(),df3[['status']].as_matrix())

 en.add_classifier(wsvm1)
 en.add_classifier(wsvm2)


 accuracy_v2(en,X_train_nb,y_train_nb)

