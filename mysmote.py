#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 21, 2015 9:40:32 AM$"




import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
import Normalizer
import time
import os.path
from smote import SMOTE, borderlineSMOTE
class mysmote(object): 
 def __init__(self,df,features,target):
     self.df = df
     self.features = features
     self.generated=None
     self.target = target
     self.train_cols=[]
     
    
 def plot_dataframe(self): 
     self.df.hist()
     pl.show()
     return
 
 def transform_dataframe(self):
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]
         if not (name_feature in self.target): self.train_cols +=[name_feature]
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 #self.df[name_feature].apply(lambda x: transformator(x))

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 #print self.df[name_feature]
                 #transformator.fit(self.df[name_feature].as_matrix()) 
                 #print data
                 def f(x):
                     print x                  
                     print transformator.transform(x)
                     return transformator.transform(x)

                 #for x in self.df[name_feature]: f(x)
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
                 #self.df[name_feature] = self.df[name_feature] .apply(lambda x: f([x]))
    return self  

 def getRatio(self,labels=[0,1]):
        class_0 = float(len(self.df.loc[self.df[self.df[self.target] == labels[0]].index,[self.target]]))
        class_1 = float(len(self.df.loc[self.df[self.df[self.target] == labels[1]].index,[self.target]]))        
        return class_0/class_1 if class_1 > 0 else np.nan

 def generate_smote_data(self,N, k, h = 1.0,label=0,add2df=True):
     minority_data=self.df.loc[self.df[self.df[self.target] == label].index,self.train_cols].as_matrix()
     smote_data = SMOTE(minority_data,N,k,h)
     #index_target = self.df.columns.get_loc(self.target)
     y=np.array([ [label] for i in range(smote_data.shape[0])])
     
     
     if (add2df): 
        minority_data_new = np.c_[y,smote_data]
        
        new_df_array=np.concatenate((self.df[[self.target]+self.train_cols].as_matrix(),minority_data_new))

        
        #from pandas.util.testing import rands_array        
        #new_df=pd.DataFrame(minority_data_new, columns=[self.target]+self.train_cols,index=rands_array(smote_data.shape[1], smote_data.shape[0]))
        new_df=pd.DataFrame(new_df_array, columns=[self.target]+self.train_cols)
        
        #self.df.append(new_df) new_df.ix[1:,[self.target]+self.train_cols]
        #self.df=pd.concat(self.df.ix[2:],self.df.ix[1:2])
        self.df = new_df
        
     return   smote_data 
 
 def generate_smote_data_v2(self,N, k,label=0,add2df=True):
     all_data=self.df[self.train_cols].as_matrix()
     all_target=self.df[self.target].as_matrix()
     (safe_minority,smote_data,danger_minority) = borderlineSMOTE(all_data,all_target,label,N,k)     
     #index_target = self.df.columns.get_loc(self.target)
     y=np.array([ [label] for i in range(smote_data.shape[0])])
     
     
     if (add2df): 
        minority_data_new = np.c_[y,smote_data]
        
        new_df_array=np.concatenate((self.df[[self.target]+self.train_cols].as_matrix(),minority_data_new))

        
        #from pandas.util.testing import rands_array        
        #new_df=pd.DataFrame(minority_data_new, columns=[self.target]+self.train_cols,index=rands_array(smote_data.shape[1], smote_data.shape[0]))
        new_df=pd.DataFrame(new_df_array, columns=[self.target]+self.train_cols)
        
        #self.df.append(new_df) new_df.ix[1:,[self.target]+self.train_cols]
        #self.df=pd.concat(self.df.ix[2:],self.df.ix[1:2])
        self.df = new_df
        
     return   smote_data 
 
 def generate_smote_data_v3(self,Sampler,add2df=True):
     all_data=self.df[self.train_cols].as_matrix()
     all_targets=self.df[self.target].as_matrix()
     X,y = Sampler.fit_transform(all_data,all_targets)
     minority_data_new = np.c_[y,X]
     
     if (add2df): 
        
        new_df=pd.DataFrame(minority_data_new, columns=[self.target]+self.train_cols)
        
        self.df = new_df
        
     return   minority_data_new 


if __name__ == "__main__":
    
    
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else -1),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('airport_dist',preprocessing.MinMaxScaler()),   
    ('booker_email_provider',Normalizer.NormalizerWrapper(type='normal')),   
    ('booker_email_country',Normalizer.NormalizerWrapper(type='normal')),   
    ('product_airline',Normalizer.NormalizerWrapper(type='normal')),   

    ]
 smote = mysmote(dataframe[ [feature[0] for feature in features] ],features,'status').transform_dataframe()
 print smote.getRatio(labels=[1,-1])
 
 
 
 print 'before SMOTE generate: Minority of len ',len(smote.df.loc[smote.df[smote.df[smote.target] == -1].index,[smote.target]])
 smote.generate_smote_data(int(smote.getRatio(labels=[1,-1]))*100,5,label=-1,add2df=True)
 print 'after SMOTE generate: Minority of len ',len(smote.df.loc[smote.df[smote.df[smote.target] == -1].index,[smote.target]])