'''
	This is a ClusterVisualization Class designed to make data ClusterVisualizations
'''

from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import copy 
import TransformDataset
import Dataset
from sklearn import preprocessing
import matplotlib.pyplot as pl
import Cluster
from sklearn.neighbors import KernelDensity
import Normalizer
import sys



class ClusterVisualization(object):
 '''
	ClusterVisualization Class ...
 '''

 def __init__(self,dataframe,cluster):

        self.dataframe = dataframe
        self.cluster = cluster
        pass


 # taken from  https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
 def kde(self,x, x_grid, bandwidth=0.5, **kwargs):

    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)

 # taken from http://toyoizumilab.brain.riken.jp/hideaki/res/code/sskernel.py
 def kde_adaptive(self, x, x_grid):
    import sskernel
    ret = sskernel.sskernel(x,tin=x_grid, bootstrap=True)
    return ret['y']


 
 def plotData1D(self,X,target,labels,type='adaptive',preprocessing=[],nsteps=100,D=None):
    ''' possible types: 'adaptive', 'small', 'big', 'standard', 'boolean' '''

    
    fig=pl.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    counter=0
    #for key,value in labels.items():
    for key,value in labels:

        
        X_vals =self.dataframe.loc[self.dataframe[self.dataframe[target] == key].index,X].as_matrix()  
        #X_vals =map(lambda x: float(x),X_vals)
        #_min = 1e9
        #_max = -1e9
        #for x in X_vals:
        #    if (x <_min): _min=float(x)
        #    if (x >_max): _max=float(x)
            #print >> open("xvals.log","a"), x   ## for debugging
            #print  x   ## for debugging
    
        # make kde: the main plotting feature
        minx=min(X_vals)
        maxx=max(X_vals)

        if (minx == maxx): minx = float(minx)/nsteps
        X_grid= np.linspace(float(minx),float(maxx), nsteps)        


        kdes ={}
        try:
            kdes = {
                'adaptive': self.kde_adaptive(X_vals,X_grid),
                'small':    self.kde(X_vals,X_grid, bandwidth=0.05),  # a small bandwith
                'big':      self.kde(X_vals,X_grid, bandwidth=1.5),   #  a big bandwith,to decrease fluctuations
                'standard': self.kde(X_vals,X_grid, bandwidth=0.5)  # a standard  bandwith
            }               
            pdf = kdes[type] if type in kdes else ''
            pl.plot(X_grid,pdf,label=key,color=value)  if type in kdes else ''
        except: 
            print "something wrong with kde"
            pass


        if type in kdes:
         ''' try to find peaks '''
         # from http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks_cwt.html
         #from scipy import signal
         #peakind = signal.find_peaks_cwt(pdf, X_grid)
         #print "found peaks: ", peakind,[(X_grid[i],pdf[i]) for i in peakind]

         # from http://nbviewer.ipython.org/github/demotu/BMC/blob/master/notebooks/DetectPeaks.ipynb
         import detect_peaks as dp
         import operator
         #peakind=dp.detect_peaks(pdf, mph=0, edge='both' ,show=True)
         peakind=dp.detect_peaks(pdf, mph=0, edge='both')
         #print "found peaks: ", peakind,sorted([(X_grid[i],pdf[i]) for i in peakind.tolist()],key=operator.itemgetter(1))
         
         # try to get labels from inverse_transform
         processor = filter(lambda x: x[0] == X,preprocessing)
         print processor
         processor = processor[0][1][0] if len(processor)>0 and len(processor[0])>1 and len(processor[0][1])>0 else lambda x: x
         print "found peaks: ", peakind,[(processor(X_grid[i]),pdf[i]) for i in peakind.tolist()]
         #print "found peaks: "
         #for i in sorted([(processor(X_grid[i]),pdf[i]) for i in peakind.tolist()],key=operator.itemgetter(1),reverse=True):
            #label_score = D[i[0]]  if not (D is None) else -1
            #print >> open("peaks.log","a"),  i[0],i[1],label_score
            



        if type in 'boolean':
            # make histogram
            # Let X_vals be the array whose histogram needs to be plotted.
            #nx, xbins, ptchs = pl.hist(X_vals, bins=1000)
            #pl.clf() # Get rid of this histogram since not the one we want.

            #nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
            #width = xbins[1] - xbins[0] # Width of each bin.
            #xhist = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
            #yhist = np.ravel(zip(nx_frac,nx_frac))
            #print 'xhist=',xhist
            #print 'yhist=',yhist
            #pl.plot(xhist,yhist,label=key,color=value)        

            #make bars
            f1 = lambda x: x>0
            f2 = lambda x: x==0
            pl.bar(-.05+counter*0.1,float(len(filter(f2,X_vals)))/len(X_vals),color=value,width=0.1,label=key)
            pl.bar(0.95+counter*0.1,float(len(filter(f1,X_vals)))/len(X_vals),color=value,width=0.1)
            counter +=1
            pl.xlim(-.5,3)
            

    pl.title(X)          
    pl.legend(loc='upper right')
    pl.show()
    
    
 

    

 def plotData2D(self,X,Y,target,labels):
    
    if (self.dataframe is None): return
    X_vals = self.dataframe[X].as_matrix()
    Y_vals = self.dataframe[Y].as_matrix()
    target_vals = self.dataframe[target].as_matrix()          
    target_vals = [ labels[i] for i in  target_vals]

    pl.figure(figsize=(10, 10))
          
    pl.scatter(X_vals, Y_vals, c=target_vals)
    pl.show()



if __name__ == '__main__':

 # Test 1:  Test of the data visualization
 ds = Dataset.Dataset() # create dataset

 #ds.loadJson("fps_data_3.json")  # load data
 ds.loadJson("fps_data_1.json")  # load data
 df= ds.createDataFrame() # create dataframe

 #print ds.listStatuses()
 # gives [u'CHECKED_OK', u'DENIED_PAYMENTERROR', u'CHECKED_NOT_OK', u'DENIED_DOUBLE', u'DENIED_NOULA', u'OPEN', u'DENIED_OTHER']
 df = df.drop(df[df.status == 'DENIED_PAYMENTERROR'].index) # drop not needed rows
 df = df.drop(df[df.status == 'DENIED_DOUBLE'].index) # drop not needed rows
 df = df.drop(df[df.status == 'DENIED_NOULA'].index) # drop not needed rows
 df = df.drop(df[df.status == 'OPEN'].index) # drop not needed rows
 df = df.drop(df[df.status == 'DENIED_OTHER'].index) # drop not needed rows


 # transform dataframe
 
 td = TransformDataset.TransformDataset(df)
 '''
 #traveller_first_age
 td.addIsEmail().addIsPhone().addIsBithday().addIsZipCode().addBookerAge().addProductPrice()
 td.addFirstTravellerAge().addAverageTravellerAge().addTotalTravellerAge().addLevenshteinFirstTraveller().addLevenshteinAllTraveller() # some categories should be added before 'Imputer'
 #td.addValidation()  # might be time consuming
 print td.dataframe.head()
 td.imputtingColumns([
    'booker_ipCountry', 
    'booker_country',
    'payer_binCountry',
    'product_departureAirport',
    'product_arrivalAirport',
    'product_airline',
    'product_typeOfFlight',
    ]).addBooking_period().addLevenshtein().addNumberTravellers().addCountryMatches().addRoute().addBookerEmailInformation()
 '''

 

 #repeate for missing values in email information
 #td.imputtingColumns(['booker_email_country','booker_email_provider'])

 print "Modification has been finished"
 # list of interesting data columns
 tosave=['status',
    'booker_ipCountry',
    'booker_country',
    'payer_binCountry',
    'product_departureAirport',
    'product_arrivalAirport',
    'booker_email_country',
    'booker_email_provider', 'booker_ipCountry_country', 'booker_country_payer_binCountry','booker_ipCountry_payer_binCountry',
    'LD_booker_payer','booking_period','Number_of_travellers','isHit','score','route',
    'product_price','is_zipcode','is_birthday','is_phone','is_email','booker_age','traveller_first_age',
    'traveller_avg_age','traveller_total_age','LD_booker_first_traveller','LD_booker_all_travellers',  'product_airline',
    'product_typeOfFlight',
    'booker_valid_ip','booker_valid_addr','airport_dist','booker_depairport_dist','booker_arrairport_dist'

    ]


  
 # Test 4: plot labels of 'booker_email_provider'
 #labels=td.dataframe['product_departureAirport']
 labels=df['product_departureAirport']
 '''
 D = {}
 for item in labels: 
      if item not in D: D[item] = 1
      else: D[item] += 1
 # example taken from http://stackoverflow.com/questions/16010869/python-plot-a-bar-using-matplotlib-using-a-dictionary
 pl.bar(range(len(D)), D.values(), align='center')
 pl.xticks(range(len(D)), D.keys())
 pl.show()
 #import operator
 #print sorted([(key,value) for key,value in D.items()],key=operator.itemgetter(1),reverse=True)
 '''
 # create a processing pipeline
 processing = [
    #('booker_ipCountry',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    #('product_departureAirport',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    #('booker_email_country',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer  
    #('booker_email_provider',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer    
    #('booker_email_provider',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    #('route',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer
    #('booker_ipCountry',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer
    ('product_departureAirport',[Normalizer.NormalizerWrapper(type='decrease')]), # sklearn transformers
 ]


 # create a manager for clustering the data
 #cluster = Cluster.Cluster(td.dataframe[tosave],processing)
 cluster = Cluster.Cluster(df,processing)
 cluster.preprocess()
 
 # create a plotter for visualization
 vis = ClusterVisualization(cluster.dataframe,cluster)
 
 # plot 1D distributions of numerical data stored in the dataframe
 #vis.plotData1D('LD_booker_payer','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('booking_period','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('booker_ipCountry_country','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('booker_country_payer_binCountry','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('booker_ipCountry_payer_binCountry','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('Number_of_travellers','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('score','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('isHit','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('is_zipcode','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('is_birthday','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('is_phone','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('is_email','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),type='bool')
 #vis.plotData1D('product_price','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('booker_age','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('traveller_first_age','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('traveller_avg_age','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('traveller_total_age','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('LD_booker_first_traveller','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('LD_booker_all_travellers','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))

 vis.plotData1D('product_departureAirport','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))

 sys.exit()
 

 # plot 1D distributions of non-numerical data stored in the dataframe
 
 # to test the Normalizers and Transformers of the textual categories: plot the output distributions of the transformer pipelines
 # to test: StandardScaler() and LabelEncoder()
 cluster.preprocess()
 #vis.plotData1D('booker_ipCountry','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('product_departureAirport','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 # to test: my own Normalizer
 #vis.plotData1D('booker_email_country','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 # some hack with inverse_preprocessing
 #f1 = lambda x: processing[0][1][0].inverse_transform(x)
 f1 = lambda x: processing[0][1][0].inverse_transform(int(processing[0][1][1].inverse_transform(x)))
 inverse_preprocess = [
        ('booker_email_provider',[f1])
 ]


 #vis.plotData1D('booker_email_provider','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),preprocessing=inverse_preprocess,type='standard',nsteps=10000,D=D)
 #vis.plotData1D('booker_email_provider','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')),preprocessing=inverse_preprocess,D=D)
 #vis.plotData1D('route','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))
 #vis.plotData1D('booker_ipCountry','status',(('CHECKED_OK','b'),('CHECKED_NOT_OK','r')))

 
 nm=processing[0][1][0]
 x=0.006 
 print "inverse_transform {0} to {1}".format(x,nm.inverse_transform(x))
 
 
 # do pca or truncated_svn to reduce the dimensions
 #cluster.preprocess().truncated_svn(['booker_ipCountry','product_departureAirport'])
 cluster.pca(['LD_booker_payer','booker_ipCountry','product_departureAirport','booking_period'])
 
 
 

 # plot 2D scatter plots
 vis.plotData2D('LD_booker_payer','product_departureAirport','status',{'CHECKED_OK':0,'CHECKED_NOT_OK':1})

 #cluster.createCluster(['booker_ipCountry','product_departureAirport'])
