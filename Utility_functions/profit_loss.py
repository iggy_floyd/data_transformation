#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Mar 23, 2015 4:53:36 PM$"

def profit_loss_const(x,alpha):
    ''' define the profit as alpha of the x '''
    
    if (alpha>1.0): alpha =1.0
    if (alpha<0.): alpha =0.
    
    return (alpha*x,(1.-alpha)*x) # (profit,loss)
    

def profit_loss_linear(x,alpha0,incr_rate,max_alpha=0.50):
    ''' define the profit as alpha of the x. Alpha increases  with x.'''
    
    if (alpha0>1.0): alpha0 =1.0
    if (alpha0<0.): alpha0 =0.
    
    alpha = min(max_alpha,max(0.0,alpha0 + x*incr_rate)) # restriction to 50% of the profit
    return (alpha*x,(1.-alpha)*x) # (profit,loss)

    
    
def profit_loss_fromto(x,alpha0,alpha1,x0,x1):
    ''' define the profit as alpha of the x. Alpha increases  with x.'''
    
    if (alpha0>1.0): alpha0 =1.0
    if (alpha0<0.): alpha0 =0.
    
    if (alpha1>1.0): alpha1 =1.0
    if (alpha1<0.): alpha1 =0.
    
    if (x0 < x1 ):
        if (alpha0>alpha1): alpha1,alpha0 = alpha0,alpha1
    
    else:
        if (alpha1>alpha0): alpha0,alpha1 = alpha1,alpha0
    
    incr_rate = (alpha1-alpha0)/(x1-x0) if x1 != x0 else 0.
    alpha0 -=incr_rate*x0 
    
    
    return profit_loss_linear(x,alpha0,incr_rate)
    
    

    
if __name__ == "__main__":
    print "Hello World";
