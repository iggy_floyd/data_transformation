#! /usr/bin/python

'''
    The module defines utility functions to represent the value of the loos/profit in Euro.
    More details can be found in 
    http://en.wikipedia.org/wiki/Risk_aversion
    
    
'''

__author__ = "debian"
__date__ = "$Mar 23, 2015 2:32:35 PM$"

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np


def crra_utility(c,scale,theta,_min):
    """Constant relative absolute risk aversion utility function.
    
     N.B.: If theta = 1, then CRRA utility is equivalent to log utility! 
            
    """
    
    def _util(c,scale,theta):
        
        if theta != 1.0:
            #utility = ((c*scale)**(1 - theta) - 1) / (1 - theta)
            utility = ((c*scale)**(1 - theta) -1) / (1 - theta)
        else:
            utility = np.log(c*scale)
    
        return utility
    
    _min_x = 1e-2 # min loos 1cent
    _max_x = 20e3 # max loost 20K Euro
    
    _min_y = _util(_min_x,scale,theta)
    _max_y = _util(_max_x,scale,theta)
    
    #print 'max',_max_y
    #print 'min',_min_y
    _y = _util(c,scale,theta)
    #print 'y',_y
    _y =_y-_min_y + _min
    #print _y
    _y /=(_max_y-_min_y)
    #print _y
    #return min(1.0,max(_min,utility))
    return _y

def crra_utility2(c,scale,theta,_min):
    """Constant relative absolute risk aversion utility function.
    
     N.B.: If theta = 1, then CRRA utility is equivalent to log utility! 
            
    """
    
    def _util(c,scale,theta):
        
        if theta != 1.0:            
            
            utility = ((c*scale)**(1 - theta) -1) / (1 - theta)
        else:
            
            utility = np.log(c*scale)
    
        return utility
    
    return _util(c,scale,theta)


if __name__ == "__main__":
    #print 'AA',crra_utility(0.1,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro    
    #print 'BB',crra_utility(500,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro    
    #print 'CCC',crra_utility(0.1,1e-2,1.1+1e-1,1e-2)/crra_utility(100,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro
    #print 'CCC',crra_utility(100,1e-2,1.1+1e-1,1e-2)/crra_utility(500,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro
    print 'CCC',crra_utility(100,1e-2,1.1+1e-1,1e-2)/crra_utility(5000,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro
    print '2CCC',crra_utility2(50,1e-2,1.1+1e-1,1e-2)/crra_utility2(500,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro
    print '2BBB',crra_utility2(500,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro    
    print '2BBB',crra_utility2(10,1e-2,1.1+1e-1,1e-2) # loos/profit function @ 1000 Euro    
    print '3CCC',crra_utility2(4000,1,0.75,1e-2)/crra_utility2(3000,1,0.75,1e-2) # loos/profit function @ 1000 Euro
    print '3CCC',crra_utility2(400,1,0.75,1e-2)/crra_utility2(4000,1,0.75,1e-2) # loos/profit function @ 1000 Euro
    