#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 15, 2015 3:23:37 PM$"



import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np

import pandas as pd
from sklearn import preprocessing
from sklearn import cross_validation 
import Normalizer


class binary_transformer(object):
    def __init__(self,df,features,bins=20): 
     self.df = df
     self.features = features
     self.binary_enc = None 
     self.binary_data=None
     self.target = None
     self.train_cols=None
     self.bins=bins
     
    def transform_dataframe(self):
        for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]
         
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class", type(transformator)
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 #print self.df[name_feature]
                 #transformator.fit(self.df[name_feature].as_matrix()) 
                 #print data
                 def f(x):
                     print x                  
                     print transformator.transform(x)
                     return transformator.transform(x)

                 #for x in self.df[name_feature]: f(x)
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
                 #self.df[name_feature] = self.df[name_feature] .apply(lambda x: f([x]))
        return self  
 
    def  digitization(self,target):
        self.train_cols=[feature[0] for feature in self.features]
        self.train_cols.remove(target)
        self.bin_edges=[]
        for feature in self.train_cols:
            #print feature
            categ_values=self.df[feature].as_matrix()
            _, bin_edges = np.histogram(categ_values,self.bins)
            self.bin_edges +=[bin_edges]
            def f(x,bins):
                return np.digitize([x],bins)[0]
            #print bin_edges
            #print map(lambda x: f(x,bin_edges),categ_values)
            self.df[feature] = self.df[feature].apply(lambda x: f(x,bin_edges))
        return self
    
    def  digitization_value(self,value,index):
        return np.digitize([value],self.bin_edges[index])[0]
 
    def transform_dataframe_binary(self,target):
        self.train_cols=[feature[0] for feature in self.features]
        self.train_cols.remove(target)
        self.binary_enc = preprocessing.OneHotEncoder().fit(self.df[self.train_cols].as_matrix().tolist())
        self.binary_data = self.df[self.train_cols].as_matrix()
        self.binary_data=map(lambda x: list(x),self.binary_data)
        self.binary_data=self.binary_enc.transform(self.binary_data).toarray().tolist()
        self.binary_target = self.df[target].as_matrix()
        print 'binary data is ready'
        return self  
    
    
    def create_dataframe_ordinary(self,target):
        self.train_cols=[feature[0] for feature in self.features]
        self.train_cols.remove(target)
        
        self.data = self.df[self.train_cols].as_matrix().tolist()
        self.binary_target = self.df[target].as_matrix()
        print 'ordinary data is ready'
        return self  

    def transform_dataframe_binary_labelbinarizer(self,target):

        self.binary_enc = {}
        binary_enc = []
        for feature in self.train_cols:
            self.binary_enc[feature] =  preprocessing.LabelBinarizer().fit(self.df[feature].as_matrix().tolist())
            binary_enc+=[self.binary_enc[feature]]
            
        
        
        self.binary_data = self.df[self.train_cols].as_matrix().tolist()
        def _binarization(x,encoders):
            return [ encoders[i].transform([y]).flatten().tolist() for i,y in enumerate(x)]

  
        
        self.binary_data=map(lambda x: [item for sublist in _binarization(x,binary_enc) for item in sublist],self.binary_data)        
        #print self.binary_data
        self.binary_target = self.df[target].as_matrix()
        print 'labelbinary data is ready'
        return self  

    
    def write_to_file_binary(self,fileName,_min,_max):
        
        dimensionality=len(self.binary_data[0]) +1
        print dimensionality    
        targets=np.array(self.binary_target[_min:_max])
        data=np.array(self.binary_data[_min:_max])
        
        # transform data
        transf_data=[]
        transf_targets=[]
        for j,elem in enumerate(data):
            _transf_data=[]
            transf_targets += str(int(targets[j]))
            for i,val in enumerate(elem): 
                if (val > 0) : _transf_data+=['%s:%s'%(str(i+i),str(int(val)))]
            transf_data += [_transf_data]
             
        data=np.array(transf_data)
        targets=np.array(transf_targets)
        
        # save data
        np.savetxt(fileName, np.c_[targets,data], delimiter=" ", fmt="%s",header=str(dimensionality),newline='\n', comments='# ') 
        
        return self

    def _transform_and_save_binary(self,fileName,data,targets):
            dimensionality=len(self.binary_data[0]) +1
            print dimensionality
            # transform data
            transf_data=[]
            for j,elem in enumerate(data):

                _transf_data=[]
                for i,val in enumerate(elem): 
                    if (val > 0) : _transf_data+=['%s:%s'%(str(i+1),str(int(val)))]
                transf_data += [_transf_data]

            data=np.array(transf_data)
            
	    data_target=[ [str(x[0])]+x[1] for x in zip(targets.tolist(),transf_data)]	
            with open(fileName,'w') as thefile:
               for item in data_target:
                  print>>thefile, ' '.join(item)

            # save data
            #np.savetxt(fileName, np.c_[targets,data], delimiter=" ", fmt="%s",header=str(dimensionality),newline='\n', comments='# ')             
            #np.savetxt(fileName, np.c_[targets,data], delimiter=" ", fmt="%s",newline='\n', comments='# ') 
    
    
    def transform_and_save_binary(self,fileName,data,targets):
           self._transform_and_save_binary(fileName+'_test.txt',data,targets)
           return self
       
    def transform_and_save_ordinary(self,fileName,data,targets):
           self._transform_and_save_ordinary(fileName+'_test.txt',data,targets)
           return self
    
    def save_test_train_to_file_binary(self,fileName_prefix,test_size=0.1):
        X_train,X_test,y_train, y_test = cross_validation.train_test_split(self.binary_data, self.binary_target, test_size=test_size, random_state=0)
        self._transform_and_save_binary(fileName_prefix+'_train.txt',X_train,y_train)
        self._transform_and_save_binary(fileName_prefix+'_test.txt',X_test,y_test)
        return self



    def _transform_and_save_ordinary(self,fileName,data,targets):
            dimensionality=len(self.data[0]) +1
            print dimensionality
            # transform data
            transf_data=[]
            for j,elem in enumerate(data):

                _transf_data=[]
                for i,val in enumerate(elem): 
                     _transf_data+=['%s:%.3e'%(str(i+1),float(val))]
                transf_data += [_transf_data]

             
	    data_target=[ [str(x[0])]+x[1] for x in zip(targets.tolist(),transf_data)]	
            with open(fileName,'w') as thefile:
               for item in data_target:
                  print>>thefile, ' '.join(item)



    def save_test_train_to_file_ordinary(self,fileName_prefix,test_size=0.1):
        X_train,X_test,y_train, y_test = cross_validation.train_test_split(self.data, self.binary_target, test_size=test_size, random_state=0)
        self._transform_and_save_ordinary(fileName_prefix+'_train.txt',X_train,y_train)
        self._transform_and_save_ordinary(fileName_prefix+'_test.txt',X_test,y_test)
        return self


if __name__ == "__main__":


 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else -1),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('airport_dist',preprocessing.MinMaxScaler()),   


    ]
#binary_transformer=binary_transformer(dataframe[ [feature[0] for feature in features] ],features,bins=20) # not bad
#binary_transformer=binary_transformer(dataframe[ [feature[0] for feature in features] ],features,bins=60) # accepted
 binary_transformer=binary_transformer(dataframe[ [feature[0] for feature in features] ],features,bins=100) # very nice
# binary_transformer=binary_transformer(dataframe[ [feature[0] for feature in features] ],features,bins=200) # not so nice as bins=100

 binary_transformer.transform_dataframe().create_dataframe_ordinary('status').save_test_train_to_file_ordinary('sofia_ml_model_ordinary',test_size=0.1)

 binary_transformer.transform_dataframe().digitization('status') 
 binary_transformer.transform_dataframe_binary_labelbinarizer('status').save_test_train_to_file_binary('sofia_ml_model_labelbinary_low_res',test_size=0.1)
 binary_transformer.transform_dataframe_binary('status').save_test_train_to_file_binary('sofia_ml_model_binary_low_res',test_size=0.1)

