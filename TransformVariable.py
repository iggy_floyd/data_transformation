'''
	This is a TansformVariable Class designed to transform Variables
'''

from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import copy 
import TransformDataset
import Dataset
from sklearn import preprocessing
import sys
import copy

import matplotlib.pyplot as plt

class TansformVariable(object):
 '''
	TansformVariable -- to transform Variables
 '''

 def __init__(self,dataframe,name,Imputer=None,type=None,encoder=None):

        self.name = name
        self.dataframe = copy.deepcopy(dataframe[name])
        if (type is not None and not self.dataframe.isnull().any()):  #.astype(type)
            self.dataframe = pd.DataFrame(self.dataframe,dtype=type)[name]
        self.type = type
        self.imputer = Imputer
        self.encoder = encoder
        self.hist  = None
        self.bins  = None
        self.transformed = None

	pass



 def getStatistics(self):

        if (self.dataframe is None) : return None
        return (self.dataframe.min(),self.dataframe.max(),self.dataframe.mean(),self.dataframe.std())


 def toArray(self):

        if (self.dataframe is None) : return None
        return self.dataframe.as_matrix()


 def plot(self,norm=True,bins=100,Transformed=False):
        if (self.dataframe is None) : return 

        if (Transformed and self.transformed is  None ): return

        dataframe = self.dataframe
        if (Transformed): 
            dataframe=self.transformed



        
        self.hist, self.bins = np.histogram(dataframe.as_matrix(), normed=norm, bins=bins, density=True)
        if (norm): self.hist = self.hist  / self.hist.sum()

        plt.bar(self.bins[:-1], self.hist, width = 1)
        plt.xlim(min(self.bins), max(self.bins))
        plt.xlim(min(self.bins), max(self.bins))
        plt.xlabel(self.name)
        plt.ylabel('a.u.')
        plt.show() 

        return


 def fitEncoder(self,labels=None):
    
        if (self.dataframe.dtype == np.dtype('O')): 
            if (self.encoder is None): self.encoder=preprocessing.LabelEncoder()
            if (labels is None): labels=map(lambda x: x if not pd.isnull(x) else 'NaN', self.dataframe.get_values())
            self.encoder.fit(labels)
            self.transformed = copy.deepcopy(self.dataframe)
            self.transformed = pd.DataFrame(self.transformed)[self.name].apply(lambda x: self.encoder.transform(x))            
            
            

        return 
 


 def transform(self,value):
    
        if (self.encoder is not None):  

            return self.encoder.transform(value)
        
        return 
        
        


if __name__ == '__main__':


 # Test 1: check transformation of labels
 ds = Dataset.Dataset()
 ds.loadJson("fps_data_1.json") 
 df= ds.createDataFrame()
 # get all values on 'product_departureAirport'
 departureAirport = df['product_departureAirport'].get_values()

 # create transformers of labels to numeric represenation
 
 enc_lbenc=preprocessing.LabelEncoder().fit(departureAirport) 
 enc_lbbin=preprocessing.LabelBinarizer().fit(departureAirport) 
 print departureAirport[0]
 print enc_lbenc.transform( departureAirport[0])
 print enc_lbbin.transform([departureAirport[0]])

 # Test 2: StandardScaler on .LabelEncoder() codes
 departureAirport_lbenc = enc_lbenc.transform( departureAirport)
 departureAirport_lbenc = np.array([ [float(i)] for i in  departureAirport_lbenc.tolist()])
 print departureAirport_lbenc
 enc_lbenc_standscaler = preprocessing.StandardScaler().fit(departureAirport_lbenc)
 print  enc_lbenc_standscaler.transform(departureAirport_lbenc)
 print enc_lbenc_standscaler.mean_
 print enc_lbenc_standscaler.std_


 # transformation pipe
 value = departureAirport[10]
 encoded = enc_lbenc_standscaler.transform([ enc_lbenc.transform(value) ])
 print "{0}->{1}".format(value,encoded)
 
 # inverse transformation pipe
 print enc_lbenc_standscaler.inverse_transform(encoded)
 value = enc_lbenc.inverse_transform(int(enc_lbenc_standscaler.inverse_transform(encoded)))
 print "{0}->{1}".format(encoded,value)


 # Test 3: MinMaxScaler on .LabelEncoder() codes

 enc_lbenc_normalizer = preprocessing.MinMaxScaler().fit(departureAirport_lbenc)
 print  enc_lbenc_normalizer.transform(departureAirport_lbenc)
 

 # transformation pipe
 value = departureAirport[10]
 encoded = enc_lbenc_normalizer.transform([enc_lbenc.transform(value) ])
 print "{0}->{1}".format(value,encoded)

 

 # inverse transformation pipe
 print enc_lbenc_normalizer.inverse_transform(encoded)
 value = enc_lbenc.inverse_transform(int(enc_lbenc_normalizer.inverse_transform(encoded)))
 print "{0}->{1}".format(encoded,value)

 

 # Test 4: plot labels
 # df['product_departureAirport'].plot(kind='bar');  # will  get error
 # instead do
 departureAirport=df['product_departureAirport']

 D = {}
 for item in departureAirport: 
      if item not in D: D[item] = 1
      else: D[item] += 1

 
 # example taken from http://stackoverflow.com/questions/16010869/python-plot-a-bar-using-matplotlib-using-a-dictionary
 plt.bar(range(len(D)), D.values(), align='center')
 plt.xticks(range(len(D)), D.keys())
 plt.show()

 print "Most popular are MUC = {0}, FRA={1}".format(
        enc_lbenc_standscaler.transform( [enc_lbenc.transform('MUC')] ),
        enc_lbenc_standscaler.transform( [enc_lbenc.transform('FRA')] )   
        )

 # plot transformed distribution
 values = departureAirport.get_values()
 import Normalizer
 Normalizer = Normalizer.Normalizer(values).fit_decrease()
 transformed = enc_lbenc_standscaler.transform( enc_lbenc.transform(values) )
 plt.hist(transformed)
 plt.xlabel('transformed')
 plt.ylabel('Frequency')
 plt.show()

 # Test 5:  plot Fraud departureAirport
 dataFraud = ds.filterStatus('CHECKED_NOT_OK')
 dsfraud = Dataset.Dataset(dataFraud)
 df=dsfraud.createDataFrame()
 departureAirport=df['product_departureAirport']  

 D = {}
 for item in departureAirport: 
      if item not in D: D[item] = 1
      else: D[item] += 1

 
 # example taken from http://stackoverflow.com/questions/16010869/python-plot-a-bar-using-matplotlib-using-a-dictionary
 plt.bar(range(len(D)), D.values(), align='center')
 plt.xticks(range(len(D)), D.keys())
 plt.show()

 
 values = departureAirport.get_values()
 transformed = enc_lbenc_standscaler.transform( enc_lbenc.transform(values) )
 plt.hist(transformed)
 plt.xlabel('transformed')
 plt.ylabel('Frequency')
 plt.show()

 # Test 5: plot Fraud labels using own Normalizer class
 values = departureAirport.get_values()
 
 Normalizer.plot(values)
