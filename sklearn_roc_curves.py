'''
	Class to Plot different Performance plots of classifiers
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
from sklearn.metrics import roc_curve, auc,precision_recall_curve
from sklearn.cross_validation import StratifiedKFold,KFold
import PDE
import projective_likelihood
import sys
import scipy

class sklearn_roc_curves(object):
 '''
    See
    http://en.wikipedia.org/wiki/Precision_and_recall	

    Sklearn estimates TP, FP,TN,FN using discriminator > cut

 '''

 def __init__(self,scores,trues):

        self.scores=scores
        self.trues=trues
        self.fpr = None
        self.tpr = None
        self.thresholds = None
        self.roc_auc = None        
        self.prec = None
        self.recall  = None

        pass

 def calculateROC(self,weights=None):
        self.fpr,self.tpr,self.thresholds =  roc_curve(self.trues, self.scores,sample_weight=weights)
        self.roc_auc= auc(self.fpr, self.tpr, False if weights is None else True)
        
        return self

 def calculatePrecision(self,weights=None):
        self.prec,self.recall,self.thresholds =   precision_recall_curve(self.trues, self.scores,sample_weight=weights)
        return self

 def createPlotROC(self,title,label=''):
        pl.plot(self.fpr, self.tpr, lw=1, label='ROC (area = %0.2f) %s' % (self.roc_auc,label))
        pl.xlim([-0.05, 1.05])
        pl.ylim([-0.05, 1.05])
        pl.xlabel('False Positive Rate')
        pl.ylabel('True Positive Rate')
        pl.title('ROC: %s'%title)
        pl.legend(loc="lower right")

        return self

 def createPlotPrecision(self,title,label=''):
        pl.plot(self.recall, self.prec, lw=1, label='Recall_vs_Precision: %s' % (label))
        pl.xlim([-0.05, 1.05])
        pl.ylim([-0.05, 1.05])
        pl.xlabel('Recall (True Positive Rate)')
        pl.ylabel('Precision (Positive Predictive Value )')
        pl.title('Recall: %s'%title)
        pl.legend(loc="lower right")

        return self



def sklearn_roc_plot(cv,proj,y,vals):
 '''   Example  of ROC curve for the PLE with help of the sklearn '''

 # assumed fraction of frauds in all dataset
 fraud_frac=0.01

 # plot ROC with help of sklearn
 mean_tpr = 0.0
 mean_fpr = np.linspace(0, 1, 100)
 for i, (train, test) in enumerate(cv):    
    # compute pde output for this trial
    scores = np.array(map(lambda x:  proj.getVal(x), [[ pde_categ[j][2](x) for j,x in enumerate(vals[t].tolist())] for t in test]  ))        

    # apply weights to the sample
    def applyweigt(arr,y):        
        return [ y[0] if x >0 else y[1] for x in arr ]
    weights = np.apply_along_axis(applyweigt, 0, y[test],[1.-fraud_frac,fraud_frac])

    if (i==0):   label="\n N(OK)= %d, \n N(NOT_OK) = %d "% ((1.-fraud_frac)*len(y[test][y[test]>0]),(fraud_frac)*len(y[test][y[test]==0]))
    else: label=""

    # Compute ROC curve and area the curve
    roc=sklearn_roc_curves(scores,y[test]).calculateROC(weights=weights).createPlotROC("PLE > cut",
        #label="fold %d, TP+FP = %d,FN+TN = %d "% (i,len(y[test][y[test]>0]),len(y[test][y[test]==0]))
         label=label
        )
    
    
    #calculate the mean ROC
    mean_tpr += scipy.interp(mean_fpr, roc.fpr, roc.tpr)
    mean_tpr[0] = 0.0

    
   
    # plot discriminant values
    if (i>0): continue
    for k,x in enumerate(roc.thresholds.tolist()):
        if k%(len(roc.thresholds.tolist())/10 if len(roc.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([roc.fpr[k]],[roc.tpr[k]],'o',color='r')
            pl.text(roc.fpr[k]+0.01,roc.tpr[k]-0.01,'%3.2f'%x,color='r')
            #print "threshold = %f, tpr= %f, fpr = %f"%(x,roc.tpr[k],roc.fpr[k])

 

 # plot mean ROC
 mean_tpr /= len(cv)
 mean_tpr[-1] = 1.0
 mean_auc = auc(mean_fpr, mean_tpr)
 pl.plot(mean_fpr, mean_tpr, 'k--',
         label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

 # plot random ROC
 pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')

 pl.legend(loc="lower right")
 pl.show()
 return 



def sklearn_precision_plot(cv,proj,y,vals):
 ''' plot recall vs precision plot '''

 # plot Recall vs Precision
 for i, (train, test) in enumerate(cv):    
    # compute pde output for this trial
    scores = np.array(map(lambda x:  proj.getVal(x), [[ pde_categ[j][2](x) for j,x in enumerate(vals[t].tolist())] for t in test]  ))        
        

    # Compute Precisions curve
    prec=sklearn_roc_curves(scores,y[test]).calculatePrecision().createPlotPrecision("PLE > cut",
        label="fold %d"% (i)         
        )
    # plot discriminant values
    if (i>0): continue
    for k,x in enumerate(prec.thresholds.tolist()):
        if k%(len(prec.thresholds.tolist())/10 if len(prec.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([prec.recall[k]],[prec.prec[k]],'o',color='r')
            pl.text(prec.recall[k]+0.01,prec.prec[k]-0.01,'%3.2f'%x,color='r')
 
 pl.show()
 return 

def  pyroc_roc_plot(cv,proj,y,vals):
 '''plot roc with  PyRoc '''
  
 import pyroc
 rocs=[]
 labels=[]
 n_count_classes=[]
 for i, (train, test) in enumerate(cv):    
    # compute pde output for this trial
    scores = np.array(map(lambda x:  proj.getVal(x), [[ pde_categ[j][2](x) for j,x in enumerate(vals[t].tolist())] for t in test]  ))        
    y_labels = [ 'CHECKED_OK' if a >0 else 'CHECKED_NOT_OK' for a in y[test].tolist()]  
    data = zip(y[test].tolist(),scores.tolist(),y_labels)
    rocs+=[pyroc.ROCData(data)]
    labels+=['ROC fold %d'%i]
    n_count_classes +=[(len(y[test][y[test]>0]),len(y[test][y[test]==0]))]

 pyroc.plot_multiple_roc(rocs,labels=labels)


def pyroc_precision_plot(cv,proj,y,vals):
 
 import pyroc
 rocs=[]
 labels=[]
 n_count_classes=[]
 for i, (train, test) in enumerate(cv):    
    # compute pde output for this trial
    scores = np.array(map(lambda x:  proj.getVal(x), [[ pde_categ[j][2](x) for j,x in enumerate(vals[t].tolist())] for t in test]  ))        
    y_labels = [ 'CHECKED_OK' if a >0 else 'CHECKED_NOT_OK' for a in y[test].tolist()]  
    data = zip(y[test].tolist(),scores.tolist(),y_labels)
    rocs+=[pyroc.ROCData(data)]
    labels+=['ROC fold %d'%i]
    n_count_classes +=[(len(y[test][y[test]>0]),len(y[test][y[test]==0]))]
    if i == 0: break

 for x in np.arange(0.001,1.0,0.05).tolist():
    conf_matrix = rocs[0].confusion_matrix(x)
    print x, conf_matrix
    print ()
    print ()
    metrics=rocs[0].evaluateMetrics(conf_matrix,do_print=False)
    print metrics
    print ()
 return



if __name__ == '__main__':


 # read command line options and arguments
 
 import argparse
 parser = argparse.ArgumentParser(description='Plot performance curves')
 
 parser.add_argument('-sr','--sroc', help='make a ROC plot with help of the sklearn',default=None, required=False, action='store_true')
 parser.add_argument('-sp','--sprecision', help='make a Recall vs Precision plot with help of the sklearn',default=None, required=False, action='store_true')
 parser.add_argument('-pr','--pyroc', help='make a ROC plot with help of the PyROC',default=None, required=False, action='store_true')
 parser.add_argument('-pp','--precision', help='make a precision plot with help of the PyROC',default=None, required=False, action='store_true')

 args = parser.parse_args()

 


 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 pde_categ = [
    ('airport_dist',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('LD_booker_payer',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('booking_period',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('product_price',{'typepde':'adaptive','type':'float'},lambda x: float(x)),
    ('booker_valid_addr',{'typepde':'categoricalnum'},lambda x: int(x)),
    ('product_typeOfFlight',{'typepde':'categoricaltext'},lambda x: str(x)),
    ('product_airline',{'typepde':'categoricaltext'},lambda x: str(x)),
    ('product_arrivalAirport',{'typepde':'categoricaltext'},lambda x: str(x)),
    ('product_departureAirport',{'typepde':'categoricaltext'},lambda x: str(x))
 ]

 # create PDE
 pdes_ok = []   
 pdes_notok = []
 vals_ok = []
 vals_notok = []
 for categ in  pde_categ:
     pdes_ok+=[PDE.PDE(categ[0],dataframe,**categ[1]).makePDE('status','CHECKED_OK')]
     pdes_notok+=[PDE.PDE(categ[0],dataframe,**categ[1]).makePDE('status','CHECKED_NOT_OK')]
     vals_ok +=[pdes_ok[-1].vals]
     vals_notok += [pdes_notok[-1].vals]
 
 # create a test sample
 vals_ok = zip(*vals_ok)
 vals_notok = zip(*vals_notok)
 vals =  np.array(vals_ok + vals_notok)
 y = np.array([ 1 if i<len(vals_ok) else 0   for i, val in enumerate(vals)]) 
 cv = StratifiedKFold(y, n_folds=5) # cross-validation  tool
 

 
 # create PLE
 proj=projective_likelihood.projective_likelihood(pdes_ok,pdes_notok)
 #val=[7453.3885309, 1.0, 60.0, 1193.98, 15, 'scheduled', 'UL', 'CMB', 'VIE']
 #print proj.getVal(val)
 

 if ('sroc' in vars(args) and vars(args)['sroc']):  
    sklearn_roc_plot(cv,proj,y,vals)
    sys.exit(0)
 if ('sprecision' in vars(args) and vars(args)['sprecision']):  
    sklearn_precision_plot(cv,proj,y,vals)
    sys.exit(0)
 if ('pyroc' in vars(args) and vars(args)['pyroc']):  
    pyroc_roc_plot(cv,proj,y,vals)
    sys.exit(0)
 if ('precision' in vars(args) and vars(args)['precision']):  
    pyroc_precision_plot(cv,proj,y,vals)
    sys.exit(0)
 
 
 


 #for i,roc in enumerate(rocs):
 #   conf_matrix = roc.confusion_matrix(0.30)
 #   print conf_matrix
 #   print n_count_classes[i]
 #   metrics=roc.evaluateMetrics(conf_matrix,do_print=True)
 