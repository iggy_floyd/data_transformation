
# -*- coding: utf-8 -*-
#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Mar 24, 2015 4:00:16 PM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd
import  Utility_functions  as uf
import interpolate_histogram as ih
import logistic_regression_twoclass as lgtwoclass
import seaborn as  sns
import Normalizer
from sklearn import preprocessing

def get_profit(dataframe):
    vals =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,'product_price'].as_matrix()  
    return vals

def get_loss(dataframe):
    vals =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,'product_price'].as_matrix()  
    return vals

def get_profit_model_const(dataframe,alpha):
    vals =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,'product_price'].as_matrix()      
    vals = [uf.profit_loss_const(x,alpha)[0] for x in vals]
    return vals

def get_loss_model_const(dataframe,alpha):
    vals =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,'product_price'].as_matrix()  
    vals = [uf.profit_loss_const(x,alpha)[1] for x in vals]
    return vals



def get_profit_model_const_increase(dataframe,alpha0,alpha1,price0,price1):
    vals =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_OK'].index,'product_price'].as_matrix()             
    vals = [uf.profit_loss_fromto(x,alpha0,alpha1,price0,price1)[0] for x in vals]
    return vals

def get_loss_model_const_increase(dataframe,alpha0,alpha1,price0,price1):
    vals =dataframe.loc[dataframe[dataframe['status'] == 'CHECKED_NOT_OK'].index,'product_price'].as_matrix()  
    vals = [uf.profit_loss_fromto(x,alpha0,alpha1,price0,price1)[1] for x in vals]    
    return vals

if __name__ == "__main__":
    
    # Set-up some parameters and loading dataframe
    process_name='fps_fraud_classification'
    print "reading from ",process_name+'_transformed.csv',"...."
    dataframe = pd.read_csv(process_name+'_transformed.csv')
    
    # Test 1: What's is a total profit or loss? What is a benefit = profit - loss
    profits = get_profit(dataframe)
    losses = get_loss(dataframe)
    

    print '*'*50
    print 'Before applying Logistic Regression'
    print 'total Nr. of entries = %d'%(len(dataframe))
    print 'profit = %.2f (K€)'%(np.sum(profits)/1000.)
    print 'loss = %.2f (K€)'%(np.sum(losses)/1000.)
    print 'benefit = %.2f (K€)'%(np.sum(profits)/1000.-np.sum(losses)/1000.)
    print 'benefit/profit = %.2f '%((np.sum(profits)/1000.-np.sum(losses)/1000.)/(np.sum(profits)/1000.))
    print '*'*50
    print 
    
    alpha=0.10
    profits_model = get_profit_model_const(dataframe,alpha)
    losses_model = get_loss_model_const(dataframe,alpha)
    print '*'*50
    print 'Before applying Logistic Regression && using linear profit/loss model with constant profit percentage'
    print 'total Nr. of entries = %d'%(len(dataframe))
    print 'profit = %.2f (K€)'%(np.sum(profits_model)/1000.)
    print 'loss = %.2f (K€)'%(np.sum(losses_model)/1000.)
    print 'benefit = %.2f (K€)'%(np.sum(profits_model)/1000.-np.sum(losses_model)/1000.)
    print 'benefit/profit = %.2f '%((np.sum(profits_model)/1000.-np.sum(losses_model)/1000.)/(np.sum(profits_model)/1000.))
    print '*'*50
    print 
    
      
    
    
    # Test 3: create Decision Maker and apply it
    
    features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ]
    
    lg=lgtwoclass.logistic_regression_twoclass(dataframe[ [feature[0] for feature in features] ],features)
    lg.transform_dataframe()
    lg.fit('status')
    lg.create_probas_pdf('status')
    dataframe['probas'] = lg.df['probas']
    
    

 
    def decision_maker(x,proba_provider,theta=5e-3,alpha=0.10):
        ''' Decision Maker: using Decision Theory make a decision 
        (see my presentation "Application of the Decision Theory in FPS" on 04.02.2015, slide 17)
        '''
        cost_function = lambda y: uf.cara_utility(y,theta,0.02) # cost function
        profit_function = lambda y: uf.profit_loss_const(y,alpha)[0]
        loss_function = lambda y: uf.profit_loss_const(y,alpha)[1]        
        proba_ratio = proba_provider.getProbability(x['probas'])[1]/proba_provider.getProbability(x['probas'])[0]        
        cost_ratio = cost_function(profit_function(x['product_price']))/cost_function(loss_function(x['product_price']))
        return proba_ratio<=cost_ratio
    
    # add 'predict_decision' which carries the decision on the booking: 1 - accept, 0 - decline
    dataframe['predict_decision'] = dataframe.apply(lambda x: decision_maker(x,lg.pdfs_provider,alpha=alpha),axis=1)
    
    
    # dataframe selected by the decision: only accepted bookings are considered
    dataframe_selected_decision =  dataframe.loc[dataframe['predict_decision']>0,[ categ[0] for categ in lg.features]+['product_price']]
    
        
    # calculate the profit of the selected bookings
    profits_selected_decision = get_profit(dataframe_selected_decision)
    
    # calculate the losses from frauds in the selected bookings
    losses_selected_decision = get_loss(dataframe_selected_decision)
    
    print '*'*50        
    print 'After applying Logistic Regression && using Decision Theory'
    print 'total Nr. of entries = %d'%(len(dataframe_selected_decision))
    print 'profit = %.2f (K€)'%(np.sum(profits_selected_decision)/1000.)
    print 'loss = %.2f (K€)'%(np.sum(losses_selected_decision)/1000.)
    print 'benefit = %.2f (K€)'%(np.sum(profits_selected_decision)/1000.-np.sum(losses_selected_decision)/1000.)
    print 'benefit/profit = %.2f '%((np.sum(profits_selected_decision)/1000.-np.sum(losses_selected_decision)/1000.)/(np.sum(profits_selected_decision)/1000.))
    print '*'*50
    print 
    
    
    profits_model_selected_decision = get_profit_model_const(dataframe_selected_decision,alpha)
    losses_model_selected_decision = get_loss_model_const(dataframe_selected_decision,alpha)
    
    
    print '*'*50
    print 'After applying Logistic Regression && using Decision Theory && using linear profit/loss model with constant profit percentage'
    print 'total Nr. of entries = %d'%(len(dataframe_selected_decision))
    print 'profit = %.2f (K€)'%(np.sum(profits_model_selected_decision)/1000.)
    print 'loss = %.2f (K€)'%(np.sum(losses_model_selected_decision)/1000.)
    print 'benefit = %.2f (K€)'%(np.sum(profits_model_selected_decision)/1000.-np.sum(losses_model_selected_decision)/1000.)
    print 'benefit/profit = %.2f '%((np.sum(profits_model_selected_decision)/1000.-np.sum(losses_model_selected_decision)/1000.)/(np.sum(profits_model_selected_decision)/1000.))
    print '*'*50
    print 
    
    

   
    
    # Test 4: plot obtained data
    sns.set_context("poster")
    pl.figure(figsize=(15,8))
    #sns.set_context({"figure.figsize": (24, 10)})
    
    #plot data before applying MVA
    profit_margin = 0.10
    money_flow_NoPLE = {
                    'Unister_Income':np.sum(get_profit(dataframe))/1000.,
                    'Fraud_Income':np.sum(get_loss(dataframe))/1000.,
                    'Unister_Gross_Profit':np.sum(get_profit_model_const(dataframe,profit_margin))/1000.,
                    'Unister_Loss':np.sum(get_loss_model_const(dataframe,profit_margin))/1000.,
                    'Unister_Net_Profit': np.sum(get_profit_model_const(dataframe,profit_margin))/1000.- np.sum(get_loss_model_const(dataframe,profit_margin))/1000.                    
                }
                
    
    
    #sns.set_style("dark",rc=pl.rc("figure", figsize=(45, 10)))
    #sns.set_style("dark",rc=pl.rc("figure", figsize=(30, 30)))
    #sns.set_context("poster",rc={"figure.figsize": (24, 10)})
    sns.barplot(np.array(money_flow_NoPLE.keys()),np.array(money_flow_NoPLE.values()),ci=None, palette="Paired")
    
    
    label = 'money_flow_NoLR'
    pl.title('Money flow: no LR filtering, constant Net Profit Margin of %.3f \n'%(profit_margin))
    pl.xlabel('Profit/Loss distributions')
    pl.ylabel(u'Amount in K€')
    pl.savefig('plots/'+label+'.png')
    pl.show()
    
    
    label = 'money_flow_LR_v2'
    f, axes = pl.subplots(1, 2, figsize=(24, 12), sharex=True)
    
    axes[0].pie([money_flow_NoPLE['Unister_Income'],money_flow_NoPLE['Fraud_Income']],labels=['Unister_Income','Fraud_Income'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    axes[1].pie([money_flow_NoPLE['Unister_Gross_Profit'],money_flow_NoPLE['Unister_Loss']],labels=['Unister_Gross_Profit','Unister_Loss'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    pl.title('Money flow: no PLE filtering, constant Net Profit Margin of %.3f \n'%(profit_margin))        
    pl.savefig('plots/'+label+'.png')            
    pl.show()
    
    # an important macro-economical parameter
    money_flow_NoPLE['Effective_Profit_Margin']=money_flow_NoPLE['Unister_Net_Profit']/money_flow_NoPLE['Unister_Gross_Profit']
    
    
    # plot data after applying MVA with DecisionTheory (DT)
    money_flow_LR_DT = {
                    'Unister_Income':np.sum(get_profit(dataframe_selected_decision))/1000.,
                    'Fraud_Income':np.sum(get_loss(dataframe_selected_decision))/1000.,
                    'Unister_Gross_Profit':np.sum(get_profit_model_const(dataframe_selected_decision,profit_margin))/1000.,
                    'Unister_Loss':np.sum(get_loss_model_const(dataframe_selected_decision,profit_margin))/1000.,
                    'Unister_Net_Profit': np.sum(get_profit_model_const(dataframe_selected_decision,profit_margin))/1000.- np.sum(get_loss_model_const(dataframe_selected_decision,profit_margin))/1000.                    
                }
    
    
    sns.barplot(np.array(money_flow_LR_DT.keys()),np.array(money_flow_LR_DT.values()),ci=None, palette="Paired")
    
    
    label = 'money_flow_LR_DT'
    pl.title('Money flow: with Decision Theory filtering, CARA cost_function with theta=5e-3, \n constant Net Profit Margin of %.3f \n '%(profit_margin))
    pl.xlabel('Profit/Loss distributions')
    pl.ylabel(u'Amount in K€')
    pl.savefig('plots/'+label+'.png')
    pl.show()
    
    
    label = 'money_flow_LR_DT_v2'
    f, axes = pl.subplots(1, 2, figsize=(24, 12), sharex=True)
    
    axes[0].pie([money_flow_LR_DT['Unister_Income'],money_flow_LR_DT['Fraud_Income']],labels=['Unister_Income','Fraud_Income'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    axes[1].pie([money_flow_LR_DT['Unister_Gross_Profit'],money_flow_LR_DT['Unister_Loss']],labels=['Unister_Gross_Profit','Unister_Loss'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    pl.title('Money flow: with Decision Theory filtering, CARA cost_function with theta=5e-3, \n constant Net Profit Margin of %.3f \n '%(profit_margin))
    pl.savefig('plots/'+label+'.png')            
    pl.show()
    
    money_flow_LR_DT['Effective_Profit_Margin']=money_flow_LR_DT['Unister_Net_Profit']/money_flow_LR_DT['Unister_Gross_Profit']
    
    
    # plot of the effective profit margins
    label = 'money_flow_LR_Effective_Profit_Margin'    
    sns.barplot(np.array(['No Selection','DecisionRule Selection']),
            np.array(
            [money_flow_NoPLE['Effective_Profit_Margin'],
             money_flow_LR_DT['Effective_Profit_Margin'] ]
        ),ci=None, palette="Paired")
        
    pl.title('Effective Profit Margins\n')
    pl.ylabel(u'a.u.')
    pl.savefig('plots/'+label+'.png')
    pl.show()
        
     # Test 5: plot economical effect using model of constant linear increase for the profit margin
    sns.set_context("poster")
    pl.figure(figsize=(15,9))
    
    #plot data before applying MVA
    profit_margin_low = 0.10
    profit_margin_high = 3*profit_margin_low
    price_range_low = 100.
    price_range_high = 5000.
    
    money_flow_NoPLE_const_profit_increase = {
                    'Unister_Income':np.sum(get_profit(dataframe))/1000.,
                    'Fraud_Income':np.sum(get_loss(dataframe))/1000.,
                    'Unister_Gross_Profit':np.sum(get_profit_model_const_increase(dataframe,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.,
                    'Unister_Loss':np.sum(get_loss_model_const_increase(dataframe,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.,
                    'Unister_Net_Profit': np.sum(get_profit_model_const_increase(dataframe,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.- np.sum(get_loss_model_const_increase(dataframe,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.                    
                }
                
    
    
    
    
    sns.barplot(np.array(money_flow_NoPLE_const_profit_increase.keys()),np.array(money_flow_NoPLE_const_profit_increase.values()),ci=None, palette="Paired")
    
    
    label = 'money_flow_NoLR_const_profit_increase'
    pl.title('Money flow: no LR filtering, constant increase in the Net Profit Margin from \n %.3f to %.3f for prices from %2.3f to %2.3f '%(
        profit_margin_low,profit_margin_high,price_range_low,price_range_high)
        )
    pl.xlabel('Profit/Loss distributions in the model of constant increase in the Net Profit Margin ')
    pl.ylabel(u'Amount in K€')
    pl.savefig('plots/'+label+'.png')
    pl.show()    
        
        
    label = 'money_flow_NoLR_const_profit_increase_v2'
    f, axes = pl.subplots(1, 2, figsize=(24, 12), sharex=True)
    
    axes[0].pie([money_flow_NoPLE_const_profit_increase['Unister_Income'],money_flow_NoPLE_const_profit_increase['Fraud_Income']],labels=['Unister_Income','Fraud_Income'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    axes[1].pie([money_flow_NoPLE_const_profit_increase['Unister_Gross_Profit'],money_flow_NoPLE_const_profit_increase['Unister_Loss']],labels=['Unister_Gross_Profit','Unister_Loss'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    pl.title('Money flow: no LR filtering, constant increase in the Net Profit Margin from \n %.3f to %.3f for prices from %2.3f to %2.3f '%(
        profit_margin_low,profit_margin_high,price_range_low,price_range_high))        
    pl.savefig('plots/'+label+'.png')            
    pl.show()
    
    # an important macro-economical parameter
    money_flow_NoPLE_const_profit_increase['Effective_Profit_Margin']=money_flow_NoPLE_const_profit_increase['Unister_Net_Profit']/money_flow_NoPLE_const_profit_increase['Unister_Gross_Profit']
        
    
    # plot data after applying MVA with DecisionTheory (DT)
    
    money_flow_LR_DT_const_profit_increase = {
                    'Unister_Income':np.sum(get_profit(dataframe_selected_decision))/1000.,
                    'Fraud_Income':np.sum(get_loss(dataframe_selected_decision))/1000.,
                    'Unister_Gross_Profit':np.sum(get_profit_model_const_increase(dataframe_selected_decision,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.,
                    'Unister_Loss':np.sum(get_loss_model_const_increase(dataframe_selected_decision,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.,
                    'Unister_Net_Profit': np.sum(get_profit_model_const_increase(dataframe_selected_decision,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.- np.sum(get_loss_model_const_increase(dataframe_selected_decision,profit_margin_low,profit_margin_high,price_range_low,price_range_high))/1000.                    
                }
    
    
    sns.barplot(np.array(money_flow_LR_DT_const_profit_increase.keys()),np.array(money_flow_LR_DT_const_profit_increase.values()),ci=None, palette="Paired")
    
    
    label = 'money_flow_LR_DT_const_profit_increase'
    pl.title('Money flow: with Decision Theory filtering, CARA cost_function with theta=5e-3, \n constant increase in the Net Profit Margin from \n %.3f to %.3f for prices from %2.3f to %2.3f '%(
        profit_margin_low,profit_margin_high,price_range_low,price_range_high))
    pl.xlabel('Profit/Loss distributions')
    pl.ylabel(u'Amount in K€')
    pl.savefig('plots/'+label+'.png')
    pl.show()


    label = 'money_flow_LR_DT_const_profit_increase_v2'
    f, axes = pl.subplots(1, 2, figsize=(24, 12), sharex=True)
    
    axes[0].pie([money_flow_LR_DT_const_profit_increase['Unister_Income'],money_flow_LR_DT_const_profit_increase['Fraud_Income']],labels=['Unister_Income','Fraud_Income'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    axes[1].pie([money_flow_LR_DT_const_profit_increase['Unister_Gross_Profit'],money_flow_LR_DT_const_profit_increase['Unister_Loss']],labels=['Unister_Gross_Profit','Unister_Loss'],
            autopct='%1.1f%%', shadow=True, startangle=90)
    pl.title('Money flow: with Decision Theory filtering, CARA cost_function with theta=5e-3, \n constant increase in the Net Profit Margin from \n %.3f to %.3f for prices from %2.3f to %2.3f '%(
        profit_margin_low,profit_margin_high,price_range_low,price_range_high))
    pl.savefig('plots/'+label+'.png')            
    pl.show()
    
    money_flow_LR_DT_const_profit_increase['Effective_Profit_Margin']=money_flow_LR_DT_const_profit_increase['Unister_Net_Profit']/money_flow_LR_DT_const_profit_increase['Unister_Gross_Profit']


    # plot of the effective profit margins
    
    label = 'money_flow_const_profit_increase_Effective_Profit_Margin'    
    sns.barplot(np.array(['No Selection','DecisionRule Selection']),
            np.array(
            [money_flow_NoPLE_const_profit_increase['Effective_Profit_Margin'],
             money_flow_LR_DT_const_profit_increase['Effective_Profit_Margin'] ]
        ),ci=None, palette="Paired")
        
    pl.title('Effective Profit Margins\n')
    pl.ylabel(u'a.u.')
    pl.savefig('plots/'+label+'.png')
    pl.show()
    
    sys.exit(0)    
    # Test 6: plot of the 'Unister_Net_Profit' with DT selection as a function of the theta parameter in the CARA utility function
    sns.set_context("poster")
    pl.figure(figsize=(15,9))
    label = 'profit_vs_theta_LR'
    data_to_plot = []
    for theta in [1e-3, 2e-3,3e-3,4e-3,5e-3,1e-2,2e-2,5e-2,1e-1,2e-1,5e-1,10e-1,2e0,5e0,1e1]:
    #for theta in [1e-3, 2e-3]:
        
        dataframe['predict_decision'] = dataframe.apply(lambda x: decision_maker(x,lg.pdfs_provider,theta=theta,alpha=alpha),axis=1)        
        dataframe_selected_decision =  dataframe.loc[dataframe['predict_decision']>0,[ categ[0] for categ in lg.features]+['product_price']]
        profits_model_selected_decision = get_profit_model_const(dataframe_selected_decision,alpha)
        losses_model_selected_decision = get_loss_model_const(dataframe_selected_decision,alpha)
        Unister_Net_Profit=np.sum(get_profit_model_const(dataframe_selected_decision,alpha))/1000.- np.sum(get_loss_model_const(dataframe_selected_decision,alpha))/1000.
        print (theta,Unister_Net_Profit)
        data_to_plot += [(theta,Unister_Net_Profit)]

        
    dataframe_to_plot=pd.DataFrame(data_to_plot, columns=['theta','profit'])    
    args={
            'size':9, 'aspect':2,
        }            
    g=sns.factorplot('theta','profit',data=dataframe_to_plot,**args)
    g.set_xticklabels(rotation=60)    
    pl.xlabel('theta')
    pl.ylabel(u'Amount in K€')
    pl.title('Unister Net Profit as a function of the theta parameter in the CARA Util. Function')
    pl.savefig('plots/'+label+'.png')
    pl.show()
    
    