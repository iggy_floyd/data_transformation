'''
	Create Projective Likelihood
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl
import PDE
import sys


class projective_likelihood(object):
 '''
	
 '''

 def __init__(self,pdes_ok,pdes_not_ok):

        self.pdes_ok = pdes_ok
        self.pdes_not_ok = pdes_not_ok        
	pass

 def getVal (self,xvals):
    
    res = 1
    tot_pde_ok=1
    tot_pde_not_ok=1
    
    for i,x in enumerate(xvals):
        _pde_ok = self.pdes_ok[i].getPDE(x)
        _pde_not_ok = self.pdes_not_ok[i].getPDE(x)                
        
        
        #print "i=",i,"name=",self.pdes_ok[i].name,"_pde_ok =",_pde_ok,"_pde_not_ok =",_pde_not_ok 
        #if (_pde_ok != 0): 
        tot_pde_ok*=_pde_ok
        #if (_pde_not_ok != 0): 
        tot_pde_not_ok*=_pde_not_ok
        
    num = tot_pde_ok
    denom=tot_pde_ok+tot_pde_not_ok
    if (denom != 0): return num/denom
    else: return -1.


 
if __name__ == '__main__':

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 
 # Test 1: Create some pdes for projective_likelihood
 # pde 1:   'airport_dist'

 pdes_ok=[]
 pdes_notok=[]
 vals_ok =[]
 vals_notok =[]

 
 X='airport_dist'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')]
 vals_ok+=[pdes_ok[-1].vals]
 vals_notok+=[pdes_notok[-1].vals]
 
 
 # pde 2: 'LD_booker_payer'
 X='LD_booker_payer'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 2: 'booking_period'
 X='booking_period'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 2: 'product_price'
 X='product_price'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]


 '''
 # pde 3: 'traveller_first_age'
 X='traveller_first_age'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 4: 'booker_ipCountry_payer_binCountry'
 X='booker_ipCountry_payer_binCountry'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='bool',type='float').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='bool',type='float').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 
 # quite nice
 # pde 5: 'product_departureAirport'
 X='product_departureAirport'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]
 '''
 # pde 6: 'booker_valid_addr'
 X='booker_valid_addr'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 7: 'product_typeOfFlight'
 X='product_typeOfFlight'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 8: 'product_airline'
 X='product_airline'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 9: 'product_arrivalAirport'
 X='product_arrivalAirport'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 # pde 10: 'product_departureAirport'
 X='product_departureAirport'
 pdes_ok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_OK')]
 pdes_notok+=[PDE.PDE(X,dataframe,typepde='categoricaltext').makePDE('status','CHECKED_NOT_OK')]
 vals_ok +=[pdes_ok[-1].vals]
 vals_notok += [pdes_notok[-1].vals]

 
 
 
  # Test 2: create Projective 5-dim Likelihood and plot it

 proj=projective_likelihood(pdes_ok,pdes_notok)
 vals_ok = zip(*vals_ok)
 vals_notok = zip(*vals_notok)
 y_ok=map(lambda x:proj.getVal(x),vals_ok) 
 y_notok=map(lambda x:proj.getVal(x),vals_notok)
 
 
 nbins=50
 nx, xbins, ptchs = pl.hist(y_ok, bins=nbins,normed=True)
 pl.clf() 
 nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
 width = xbins[1] - xbins[0] # Width of each bin.
 #xbins_ok =  xbins[:-1]+width
 xbins_ok =  xbins
 nx_frac_ok = nx_frac

 x_ok = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
 y_ok = np.ravel(zip(nx_frac,nx_frac))
 
 
 nx, xbins, ptchs = pl.hist(y_notok, bins=nbins,normed=True)
 pl.clf() 
 nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
 nx_frac_notok = nx_frac
 width = xbins[1] - xbins[0] # Width of each bin.
 #xbins_not_ok =  xbins[:-1]+width
 xbins_not_ok =  xbins
 
 x_notok = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
 y_notok = np.ravel(zip(nx_frac,nx_frac))

 

 pl.plot(x_ok,y_ok,c='b',linestyle="dashed",label="OK")
 pl.plot(x_notok,y_notok,c='r',label="NOT_OK")
 pl.xlim(0,1)
 pl.title('Projective likelihood estimator')          
 pl.legend(loc='upper right')
 pl.show()
 
 
 # Test 3: plot Type1/Type2 errors regarding status='OK'
 import pde_roc_curves as roc
 
 plot_roc=roc.pde_roc_curves(zip(xbins_ok.tolist(),nx_frac_ok.tolist()) ,zip( xbins_not_ok.tolist(),nx_frac_notok.tolist()))

 grid,type1= plot_roc.Type1()
 grid2,type2= plot_roc.Type2(type=False)
 
 pl.plot(grid,type1,c='b',label="Type-1 error regarding status='OK' ")
 pl.plot(grid2,type2,c='r',label="Type-2 error regarding status='OK'")
 pl.title('Type-1/Type-2 errors as functions of  Projective likelihood estimator')          
 pl.legend(loc='upper right')
 pl.show()
 
 # Test 4: plot Type1/Type2 errors regarding status='NOT_OK'
 grid,type1= plot_roc.Type1(type=False)
 grid2,type2= plot_roc.Type2(type=True)

 
 pl.plot(grid,type1,c='b',label="Type-1 error regarding status='NOT_OK' ")
 pl.plot(grid2,type2,c='r',label="Type-2 error regarding status='NOT_OK'")
 pl.title('Type-1/Type-2 errors as functions of  Projective likelihood estimator')          
 pl.legend(loc='upper right')
 pl.show()

 

# Test 5: plot ROC regarding status='NOTOK' 
 tpr,fpr= plot_roc.ROC(type=True)

 # add edge points
 tpr = [0] + tpr
 fpr = [0] + fpr
 tpr += [1] 
 fpr += [1] 
 
 pl.plot(tpr,fpr,c='b',label="ROC regarding status='NOTOK' ")
 pl.title("ROC regarding status='NOTOK' of  Projective likelihood estimator")          
 pl.legend(loc='upper right')
 pl.xlabel('TPR')
 pl.ylabel('FPR')
 pl.xlim(-.1,1.1)
 pl.ylim(-.1,1.1)
 pl.show()
