#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 21, 2015 3:16:31 PM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

from sklearn.tree import DecisionTreeClassifier # random forest

from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn import cross_validation    
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
import Normalizer
import sklearn_roc_curves as skroc
import sklearn_optimizer
import time
import os.path
from sklearn.externals import joblib
import interpolate_histogram as ih
import binary_transformer as bt
from sklearn import clone
from libnb import TwoClassNB


import scipy
from sklearn.metrics import roc_curve, auc,precision_recall_curve


import dill




def run_dill_encoded(what):
        fun, args = dill.loads(what)
        return fun(*args)

def apply_async(pool, fun, args):        
        return pool.apply_async(run_dill_encoded, (dill.dumps((fun, args)),))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    



from multiprocessing import Pool, cpu_count

def applyParallel(dfGrouped, func):
        #p=Pool(cpu_count())
        p=Pool(process=2)
        ret_list = p.map(func, [(group,name) for name, group in dfGrouped])
        return pd.concat(ret_list)    

class decision_tree_sklearn(object):
    
 def __init__(self,df,features):
     self.df = df
     self.features = features
     self.binary_transformer = None
     self.probability=False
     self.train_cols=None
     pass
    
 def plot_dataframe(self):      
     sns.set_context("poster")
     self.df.hist()
     pl.show()
     return
 
 def plot_decision_surface_v3(self,feature1,feature2,prefix):
     sns.set_context("poster")
     cmap = pl.cm.RdBu
     plot_step = 0.002
     plot_step_coarser = 0.05
     plot_colors = "rb"
     
     pair = [self.train_cols.index(feature1),self.train_cols.index(feature2)]
     
     X = self.df[self.train_cols].as_matrix()[:,pair]
     Y = self.df[self.target].as_matrix()
     idx = np.arange(X.shape[0])
     np.random.shuffle(idx)
     X = X[idx]
     y = Y[idx]
     
     x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
     y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
     
     xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),\
                             np.arange(y_min, y_max, plot_step))
     
     model = clone(self.clf)
     model.fit(X, y)
     Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
     Z = Z.reshape(xx.shape)
     
     cs = pl.contourf(xx, yy, Z, cmap=cmap,alpha=0.7)
     
     
     xx_coarser, yy_coarser = np.meshgrid(np.arange(x_min, x_max, plot_step_coarser),\
                                             np.arange(y_min, y_max, plot_step_coarser))
     Z_points_coarser = model.predict(np.c_[xx_coarser.ravel(), yy_coarser.ravel()]).reshape(xx_coarser.shape)
     #print Z_points_coarser[np.where(Z_points_coarser==0)]
     cs_points = pl.scatter(xx_coarser, yy_coarser, s=15, c=Z_points_coarser, cmap=cmap, edgecolors="none")
     score = model.score(X, y)
     pl.title('score is %.3f'%score)
     for i, c in zip(xrange(2), plot_colors):
         idx = np.where(y == i)
         pl.scatter(X[idx, 0], X[idx, 1], c=c, label='NOT_OK' if i == 0 else 'OK' , cmap=cmap)
     pl.legend(loc='upper right')
     pl.savefig('plots/decision_tree_contour_'+feature1+'_'+feature2+'_v2.png', dpi=150)     
     pl.show()
     
     
 
 def plot_decision_surface_v2(self,feature1,feature2,prefix):
     sns.set_context("poster")
     cmap = pl.cm.RdYlBu
     col_vals=[]
     for col in self.train_cols:        
        col_vals+=[np.linspace(self.df2[col].min(), self.df2[col].max(), 4)[:-1]]
        
     
     
     a=np.meshgrid(*col_vals)  

     Z= self.clf.predict(np.array(zip(*[aa.ravel().tolist() for aa in a])))
     data = np.array(zip(Z,*[aa.ravel().tolist() for aa in a]))
     df = pd.DataFrame(data,columns=['predict']+self.train_cols)
     df2=df.groupby([feature1,feature2,'predict']).size()

     df3=df2.reset_index()
     df3.columns = [feature1,feature2,'predict','size']
     df4 = pd.pivot_table(df3, values=['size'], index=[feature1,feature2,'predict'])
     for col in df3['predict'].unique():
          plt_data = df4.ix[df4.index.get_level_values(2)==col]
          plt_data2 = plt_data.reset_index()
          
          plt_data2 = plt_data2.pivot(feature1,feature2,'size')
          X = plt_data2.columns.values
          Y = plt_data2.index.values
          Z = plt_data2.values
          x,y = np.meshgrid(X, Y)
          pl.title('Class %d'%col)
          pl.contourf(X,Y,Z,alpha=0.7, cmap=pl.cm.jet)     
          pl.show()
          pl.savefig('plots/decision_tree_contour_'+'%d'%col+'_'+feature1+'_'+feature2+'.png', dpi=150)

     return 

     
 def plot_decision_surface(self,feature1,feature2):
     sns.set_context("poster")
     cmap = pl.cm.RdYlBu
     
     #df = self.df2.loc[:, [feature1,feature2,'predict']].reset_index().pivot(feature1,feature2,'predict')
     df = self.df2.loc[:, [feature1,feature2,'predict']].reset_index().pivot(feature1,feature2,'predict')
     print df
     #df_pivot = pd.pivot_table
     
     #X = df.columns.levels[0].values
     X = df.columns.values
     Y = df.index.values
     Z = df.values
     xx,yy=np.meshgrid(X, Y)
     
     print "xx=",xx
     print "yy=",yy
     print "Z=",Z
     
     #nEntries=100     
     #import random    
     #choice= random.sample(range(0,len(self.df)),nEntries) # produce calculation for proba, predict plots on 2K entries 
     #X = self.df.loc[choice,[feature1]].as_matrix()
     #Y = self.df.loc[choice,[feature2]].as_matrix()
     #all=self.df.loc[choice, self.train_cols].as_matrix()
     
     #X=self.df[[feature1]].as_matrix()
     #Y=self.df[[feature2]].as_matrix()
     #all=self.df[self.train_cols].as_matrix()
     
     #preds = self.clf.predict(all)
     
     #xx, yy = np.meshgrid(X,Y)
                             
     
     #print "x=",xx
     #print "y=",yy
     #print xx.shape
     #print yy.shape
     #print preds
     #print preds.shape
     #preds = preds.reshape(xx.shape)
     
     
     pl.contourf(xx,yy,Z,alpha=0.7, cmap=pl.cm.jet)     
     pl.show()
     sys.exit(1)
     

     pl.savefig('plots/decision_tree_contour.png', dpi=150);
     
     
     # Method 2
     
     
     #numfeat = len(self.train_cols)
     #indices = np.argsort(self.clf.feature_importances_)[::-1][:numfeat]
     #pl.bar(xrange(numfeat), self.clf.feature_importances_[indices], align='center', alpha=.5)
     #pl.xticks(xrange(numfeat), self.df.columns[indices], rotation='vertical', fontsize=12)
     #pl.xlim([-1, numfeat])
     #pl.ylabel('Feature importances', fontsize=12)

     #pl.suptitle('Feature importances computed by Random Forest', fontsize=16)
     #f = pl.gcf()
     #pl.subplots_adjust(bottom=0.3)
     #f.set_size_inches(11, 8);
     #pl.show()
     #f.savefig('plot/random_forest_feature_importance.png', dpi=150);
     
 def deviance_plot(self, X_test, y_test, ax=None, label='', train_color='#2c7bb6', 
                  test_color='#d7191c', alpha=1.0, ylim=(0, 10)):
    """Deviance plot for ``est``, use ``X_test`` and ``y_test`` for test error. """
    est=self.clf
    
    n_estimators = len(est.estimators_)
    test_dev = np.empty(n_estimators)

    for i, pred in enumerate(est.staged_predict(X_test)):
       test_dev[i] = est.loss_(y_test, pred)

    if ax is None:
        fig = plt.figure(figsize=FIGSIZE)
        ax = plt.gca()
        
    ax.plot(np.arange(n_estimators) + 1, test_dev, color=test_color, label='Test %s' % label, 
             linewidth=2, alpha=alpha)
    ax.plot(np.arange(n_estimators) + 1, est.train_score_, color=train_color, 
             label='Train %s' % label, linewidth=2, alpha=alpha)
    ax.set_ylabel('Error')
    ax.set_xlabel('n_estimators')
    ax.set_ylim(ylim)
    return test_dev, ax

 #def plot_partial_feature_importance(self,features,n_cols=2):
 #    from sklearn.ensemble.partial_dependence import plot_partial_dependence
 #    sns.set_context("poster")
 #    # Method 1
 #    
 #    FIGSIZE = (16, 7)
 #    fig, axs = plot_partial_dependence(self.clf, self.df[self.train_cols].as_matrix(), features, feature_names=self.df[self.train_cols].columns, 
 #                                  n_cols=n_cols, figsize=FIGSIZE)
 #    pl.show()
 #    pl.savefig('plots/random_forest_partial_feature_importance.png', dpi=150)
     
 
 def transform_dataframe(self):
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]         
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 #self.df[name_feature].apply(lambda x: transformator(x))

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
    return self  
 
 def fit(self,target,clf=None,probability=True):
     self.train_cols=[feature[0] for feature in self.features]
     self.train_cols.remove(target)
     self.probability=True
     self.target=target

     start = time.time()     
     print'features to train:', self.train_cols
     self.clf = DecisionTreeClassifier(random_state=1) if clf is None else clf
     
     self.clf.fit(self.df[self.train_cols].as_matrix().tolist(),self.df[target].as_matrix().tolist())     
     
     print 'fit is done using %.3f sec'%(time.time()-start)
      
     return self
     
 def validation(self,target,doSklearnAcc=True):
     

     print 'validation is started...'     
     def pred_accuracy(clf,X_test,y_test):
         print 'doing prediction...'
         pred=clf.predict(X_test)             
         print 'Prediction accuracy on the sample (of %d) : %.4f' %(len(y_test), (1 - (1. / len(y_test) * sum( pred != y_test ))))
         return  (1 - (1. / len(y_test) * sum( pred != y_test )))
     
     start = time.time()
     acc=pred_accuracy(self.clf,self.df[self.train_cols].as_matrix(),self.df[target].as_matrix())
     print 'validation is done using %.3f sec'%(time.time()-start)

     if (not doSklearnAcc): return (acc,-1.)
     # a sklearn validation tool
     _,X_test,_, y_test = cross_validation.train_test_split(self.df[self.train_cols].as_matrix(), self.df[target].as_matrix(), test_size=0.1, random_state=0)
     scores = cross_validation.cross_val_score(self.clf, X_test, y_test, cv=3, n_jobs=1,verbose=2)
     #scores = cross_validation.cross_val_score(self.clf, self.binary_transformer.binary_data, self.binary_transformer.binary_target, cv=3, n_jobs=1,verbose=2)
     print scores
     print("Accuracy of SVM with the sklearn validation tool: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
     return (acc,scores.mean())   



 def validation_v2(self,X_test,y_test):
	start = time.time()
        y_true, y_pred = y_test, self.clf.predict(X_test)
        print (classification_report(y_true, y_pred))
        print 'validation is done using %.3f sec'%(time.time()-start)
        
        
 def create_probas_pdf(self,target):
     nEntries=500
     avg_time = []
     import random    
     choice= random.sample(range(0,len(self.df)),nEntries) # produce calculation for proba, predict plots on 2K entries    

     i=[0]
     def proba_maker(x,proba_provider,i,avg_time):
         start = time.time()
         probas=proba_provider.clf.predict_proba([x[proba_provider.train_cols].as_matrix().tolist()])
         end=time.time()-start
         predict=proba_provider.clf.predict([x[proba_provider.train_cols].as_matrix().tolist()])
         #new_data = pd.Series(
         #               {   
         #                   'probas':probas.tolist()[0][1],
         #                   'probas_OK':probas.tolist()[0][1],
         #                   'probas_NOTOK':probas.tolist()[0][0],
         #                   'predict':predict[0]
         #               }
         #           )
         
         print i[0], probas,x[proba_provider.target],predict
         i[0]+=1         
         #return (probas.tolist()[0][1],probas.tolist()[0][0])
         #return new_data
         avg_time +=[end]
         return probas.tolist()[0][1],probas.tolist()[0][1],probas.tolist()[0][0],predict[0]
     
     
     #self.df['probas'] = self.df.loc[choice,:].apply(lambda x: proba_maker(x,self,i)[0],axis=1)
     self.df2 = self.df.loc[choice,:]
     #pd.concat([self.df2,self.df.loc[choice,:].apply(lambda x: proba_maker(x,self,i),axis=1)],axis=1)
     #pd.concat([self.df2,self.df.loc[choice,:].apply(lambda x: proba_maker(x,self,i),axis=1)])
     #self.df2.merge(self.df2.apply(lambda x: proba_maker(x,self,i),axis=1), 
     #       left_index=True, right_index=True)
     
     
     self.df2['probas'],self.df2['probas_OK'],self.df2['probas_NOTOK'], self.df2['predict'] = \
        zip(*self.df2.apply(lambda x: proba_maker(x,self,i,avg_time),axis=1))
     
     print "average calculation time is %.3f"%(np.average(avg_time))
     
     #self.proba_vals_class1=self.df.loc[self.df[self.df[target] == 1].index,['probas']].as_matrix()
     #self.proba_vals_class0=self.df.loc[self.df[self.df[target] == 0].index,['probas']].as_matrix()
    
     self.proba_vals_class1=self.df2.loc[self.df2[self.df2[target] == 1].index,['probas']].as_matrix()
     self.proba_vals_class0=self.df2.loc[self.df2[self.df2[target] == 0].index,['probas']].as_matrix()
    
     
     #def proba_maker(x,proba_provider):
      #   probas=proba_provider.clf.predict_proba([x[proba_provider.train_cols].as_matrix().tolist()])     
      #   print  probas, x[proba_provider.target]
      #   return (probas.tolist()[0][1],probas.tolist()[0][0])

# multithreaded apply function for pandas DataFrame: requires a lot of CPU and Memory !
     #def df_transform_parallel(df,name):
     #   print "job with  name ", name, 'started...'
     #   df['probas'] = df.apply(lambda x: proba_maker(x,self)[0],axis=1)
     #   print "job with  name ", name, 'finished...'
     #   return df
    
     #pool=Pool(processes=1)
     #b=self.df.groupby(self.df.index)
     #import random    
     #choice= random.sample(range(0,len(b)),100) # produce proba plots on 2K entries    
     #results = [apply_async(pool,df_transform_parallel,(group,name)) for name, group in self.df.groupby(self.df.index) if name in choice ]
     ##results = [apply_async(pool,df_transform_parallel,(group,name)) for name, group in self.df.groupby(self.df.index) ]
     #res=[]
     #for job in results:
     #   print "job is waiting to be finished..."
     #   a= job.get()
     #   res+=[a]
     #self.df2=pd.concat(res)

     #self.proba_vals_class1=self.df.loc[self.df[self.df2[target] == 1].index,['probas']].as_matrix()
     #self.proba_vals_class0=self.df.loc[self.df[self.df2[target] == 0].index,['probas']].as_matrix()
     
     counts1, bins1 = np.histogram(self.proba_vals_class1, bins=50)
     counts0, bins0 = np.histogram(self.proba_vals_class0, bins=50)
     
     hi_1 = ih.histogram_interpolate(counts1,bins1).create_interpolation()
     hi_0 = ih.histogram_interpolate(counts0,bins0).create_interpolation()
     self.pdfs_provider = ih.TwoClassPLEProbability(hi_1,hi_0)
     
     return self

def sklearn_roc_plot(cv,scores,y,filename,weights=None):
 '''   Example  of ROC curve for the Classifier with help of the sklearn '''

 # assumed fraction of frauds in all dataset
 fraud_frac=0.01

 # plot ROC with help of sklearn
 mean_tpr = 0.0
 mean_fpr = np.linspace(0, 1, 100)
 for i, (train, test) in enumerate(cv):    
    
    # apply weights to the sample
    def applyweigt(arr,y):        
        return [ y[1] if x >0 else y[0] for x in arr ]
        
    weights = np.apply_along_axis(applyweigt, 0, y[test],[fraud_frac,1.-fraud_frac]) if (weights is None) else \
        np.apply_along_axis(applyweigt, 0, y[test],weights)

    if (i==0):   label="\n N(OK)= %d, \n N(NOT_OK) = %d "% ((1.-fraud_frac)*len(y[test][y[test]>0]),(fraud_frac)*len(y[test][y[test]==0]))
    else: label=""

    # Compute ROC curve and area the curve
    roc=skroc.sklearn_roc_curves(scores[test],y[test]).calculateROC(weights=weights).createPlotROC("PLE > cut",
        #label="fold %d, TP+FP = %d,FN+TN = %d "% (i,len(y[test][y[test]>0]),len(y[test][y[test]==0]))
         label=label
        )
    
    
    #calculate the mean ROC
    mean_tpr += scipy.interp(mean_fpr, roc.fpr, roc.tpr)
    mean_tpr[0] = 0.0

    
   
    # plot discriminant values
    if (i>0): continue
    for k,x in enumerate(roc.thresholds.tolist()):
        if k%(len(roc.thresholds.tolist())/10 if len(roc.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([roc.fpr[k]],[roc.tpr[k]],'o',color='r')
            pl.text(roc.fpr[k]+0.01,roc.tpr[k]-0.01,'%3.2f'%x,color='r')
            #print "threshold = %f, tpr= %f, fpr = %f"%(x,roc.tpr[k],roc.fpr[k])

 

 # plot mean ROC
 mean_tpr /= len(cv)
 mean_tpr[-1] = 1.0
 mean_auc = auc(mean_fpr, mean_tpr)
 pl.plot(mean_fpr, mean_tpr, 'k--',
         label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

 # plot random ROC
 pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')

 pl.legend(loc="lower right")
 pl.savefig('plots/'+filename+'.png')
 pl.show()
 return 


def sklearn_precision_plot(cv,scores,y,filename,weights=None):    
 ''' plot recall vs precision plot '''

# assumed fraction of frauds in all dataset
 fraud_frac=0.01 
 for i, (train, test) in enumerate(cv):    

    # Compute Precisions curve
    def applyweigt(arr,y):        
        return [ y[1] if x >0 else y[0] for x in arr ]
    weights = np.apply_along_axis(applyweigt, 0, y[test],[fraud_frac,1.-fraud_frac]) if (weights is None) else \
        np.apply_along_axis(applyweigt, 0, y[test],weights)


    prec=skroc.sklearn_roc_curves(scores[test],y[test]).calculatePrecision(weights=weights).createPlotPrecision("PLE > cut",
        label="fold %d"% (i)         
        )
    # plot discriminant values
    if (not (i==1)): continue
    for k,x in enumerate(prec.thresholds.tolist()):
        if k%(len(prec.thresholds.tolist())/10 if len(prec.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([prec.recall[k]],[prec.prec[k]],'o',color='r')
            pl.text(prec.recall[k]+0.01,prec.prec[k]-0.01,'%3.2f'%x,color='r')
 
 pl.savefig('plots/'+filename+'.png') 
 pl.show()
 
 return 



def isolate_and_plot(combos,value_name,variable_name,group_name,label,transformer=None):

 if group_name is None:
    grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name], aggfunc=np.mean)
 else:   
    grouped = pd.pivot_table(combos, values=[value_name], index=[variable_name, group_name], aggfunc=np.mean)


 colors = 'rbgyrbgy'
 if (group_name):
  for col in sorted(combos[group_name].unique()):
   plt_data = grouped.ix[grouped.index.get_level_values(1)==col]
   pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],  color=colors[int(col)])
 else:
   plt_data = grouped.ix[grouped.index.get_level_values(0)]
   pl.plot(plt_data.index.get_level_values(0), plt_data[value_name],   color=colors[int(0)])
 if (transformer is not None):     
     print transformer
     print variable_name
     #print plt_data.index.get_level_values(0)
     #print [ transformer.inverse_transform([i]) for i in plt_data.index.get_level_values(0) ]
     tcks = sorted(plt_data.index.get_level_values(0))
     lbls =  [ transformer.inverse_transform([i])[0] for i in tcks ]
     #pl.xticks( plt_data.index.get_level_values(0), [ transformer.inverse_transform([i])[0] for i in plt_data.index.get_level_values(0) ],rotation=60)
     pl.xticks( tcks,lbls,rotation=60)
     
 pl.xlabel(variable_name)
 pl.ylabel("P(%s)"%value_name)
 if (group_name): pl.legend([str(i) for i in range(int(combos[group_name].min()),int(combos[group_name].max()+1))], loc='upper left', title=group_name)
 pl.title("Prob(%s) isolating %s variable and group %s"%(value_name,variable_name,group_name))

 pl.savefig('plots/'+label+'.png')
 pl.show()

if __name__ == "__main__":
 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    ('booker_age',preprocessing.StandardScaler()), # bad 
    ('product_price',preprocessing.MinMaxScaler()), # bad 
    ('airport_dist',preprocessing.MinMaxScaler()),    

    
    ]
 
 renew = True
 doValidation=False
 
 
 if not(os.path.exists('decision_tree_sklearn_saved.pkl')) or renew:
    rf=decision_tree_sklearn(dataframe[ [feature[0] for feature in features] ],features)   
 # plot numerical data
 #rf.plot_dataframe()

 # transform to numerical data and plot again
    rf.transform_dataframe()
    
 #svc.plot_dataframe()
 
 # fit the model 
    rf.fit('status',probability=False)
 
 
 
 
 
 # print the validation information
    acc1,acc2=rf.validation('status',doSklearnAcc=True) if doValidation else  (None,None)    
    _,X_test,_, y_test = cross_validation.train_test_split(rf.df[rf.train_cols].as_matrix(), rf.df['status'].as_matrix(), test_size=0.1, random_state=0)    
    if (doValidation):  rf.validation_v2(X_test,y_test)
    start=time.time()
    print 'dumping RF...'
    
    with open('decision_tree_sklearn_saved.pkl', 'wb') as fid:
        dill.dump(rf, fid)   
    print 'RF has been dumped... with time %.3f'%( time.time()-start)
 else:
    start=time.time()
    print 'loading is  started..'    
    with open('decision_tree_sklearn_saved.pkl', 'rb') as fid:
       rf=dill.load(fid)   
    print 'RF has been loaded... with time %.3f'%( time.time()-start)
    print 'start valiadtion of version 2...'
    _,X_test,_, y_test = cross_validation.train_test_split(rf.df[rf.train_cols].as_matrix(), rf.df['status'].as_matrix(), test_size=0.1, random_state=0)    
    if (doValidation):  rf.validation_v2(X_test,y_test)

 # plot the feature importance    
 #rf.plot_partial_feature_importance(['airport_dist','product_arrivalAirport',('booker_valid_addr','booker_age')],n_cols=2)
 #rf.plot_feature_importance()
 
 
 
 """Deviance plot for RandomForesr, use ``X_test`` and ``y_test`` for test error. """
 #_,X_test,_, y_test = cross_validation.train_test_split(rf.df[rf.train_cols].as_matrix(), rf.df['status'].as_matrix(), test_size=0.1, random_state=0)    
 #rf.deviance_plot(X_test,y_test)
 #pl.show()
 #sys.exit()
 # plot deviance
 prefix = 'decision_tree_sklearn_'
 
 
 
 
 
 rf.probability = True
 
 if (rf.probability):
    rf.create_probas_pdf('status')
    
    if (True): pl.figure(figsize=(15,8))
    transformers = dict((x[0],x[1]) for x in rf.features)
 
    for feature in rf.train_cols:
        if (True):    transformer = transformers[feature]
        else:   transformer = None
        label = 'probas_ok_status_'+feature+'_distr'
        #isolate_and_plot(rf.df2,'probas_OK',feature,'status',prefix+label,transformer)       
        label = 'probas_ok_'+feature+'_distr'
        #isolate_and_plot(rf.df2,'probas_OK',feature,None,prefix+label,transformer)
 
 
    
    
    rf.plot_decision_surface_v2('airport_dist','product_price',prefix)
    rf.plot_decision_surface_v2('product_arrivalAirport','product_price',prefix)
    rf.plot_decision_surface_v3('airport_dist','product_price',prefix)  
    rf.plot_decision_surface_v3('product_arrivalAirport','product_price',prefix)

    # create probabilites: works only when probas are switched on, i.e. svc.fit('status',probability=True)
    #def proba_maker(x,proba_provider):
    #     probas=proba_provider.clf.predict_proba([x[proba_provider.train_cols].as_matrix().tolist()])         
    #     return (probas.tolist()[0][1],probas.tolist()[0][0])

 
 
    #rf.df['probas_OK'] = rf.df.apply(lambda x: proba_maker(x,rf)[0],axis=1)
    #rf.df['probas_NOTOK'] = rf.df.apply(lambda x: proba_maker(x,rf)[1],axis=1)
    

      # plot probabilites
    sns.set_context("poster")
    label = 'probas_all_distr'
    b, g, r, p = sns.color_palette("muted", 4)
 
 
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK for OK',hist=False, axlabel='probability')
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK for OK',hist=False, axlabel='probability')
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK for NOT_OK',hist=False, axlabel='probability')
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK for NOT_OK',hist=False, axlabel='probability') 
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
 
  
 # plot probabilites
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_ok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 # plot probabilites
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_notok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
 
 
 
 # calculate weights (needed for correct type-1/type-2 error results)
 weights = {
        0:float(len(rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,['status']]))/float(len(rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,['status']])),
        1:1
    }

  # estimate perfomance: type1/ type2 errors etc
 pos = rf.df2.loc[rf.df2[rf.df2['predict'] == 1].index,['status']]
 true_pos = pos.loc[pos[pos['status'] == 1].index]
 false_neg = pos.loc[pos[pos['status'] == 0].index]

 neg = rf.df2.loc[rf.df2[rf.df2['predict'] == 0].index,['status']]
 true_neg = neg.loc[neg[neg['status'] == 0].index]
 false_pos = neg.loc[neg[neg['status'] == 1].index]

 if (False): apply_weights=True
 #if ('weights' in vars(args) and vars(args)['weights']):   apply_weights=True
 else:   apply_weights=False
 
 
 if (not apply_weights):
    print "found rates", {    'true_pos':len(true_pos),
            'false_pos':len(false_pos),
            'true_neg':len(true_neg),
            'false_neg':len(false_neg),
 
        }

    performance = {
 
        'prec':float( len(true_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'type1':float( len(false_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'tpr':float( len(true_pos) )/float(  len(true_pos) +   len(false_neg) ),
        'npv':float( len(true_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'type2':float( len(false_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'fpr':float( len(false_pos) )/float(  len(true_neg) +   len(false_pos) ),
 
    }
 else:
     print "found rates", {    'true_pos':len(true_pos)*weights[1],
            'false_pos':len(false_pos)*weights[1],
            'true_neg':len(true_neg)*weights[0],
            'false_neg':len(false_neg)*weights[0],
 
        }

     performance = {
 
        'prec':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'type1':float( len(false_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'tpr':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_neg)*weights[0] ),
        'npv':float( len(true_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'type2':float( len(false_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'fpr':float( len(false_pos)*weights[1] )/float(  len(true_neg)*weights[0] +   len(false_pos)*weights[1] ),
 
    }
 print performance
 #if ('perfomance_diagram' in vars(args) and vars(args)['perfomance_diagram']) or ('all' in vars(args) and vars(args)['all']):  
 sns.barplot(np.array(performance.keys()),np.array(performance.values()),ci=None, palette="Paired", hline=.1)
 label = 'perfomance'
 pl.savefig('plots/'+prefix+label+'.png')
 pl.show()
 
 
 # make some performance plot: ROC 
 vals_ok=rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,rf.train_cols].as_matrix().tolist()
 vals_notok =rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,rf.train_cols].as_matrix().tolist()
 
 
  # create a test sample
 vals =  np.array(vals_ok + vals_notok)
 y = np.array([ 1 if i<len(vals_ok) else 0   for i, val in enumerate(vals)]) 
 cv = StratifiedKFold(y, n_folds=5) # cross-validation  tool
 scores = np.array(
         rf.df2.loc[rf.df2[rf.df2['status'] == 1].index,'probas_OK'].as_matrix().tolist() +
         rf.df2.loc[rf.df2[rf.df2['status'] == 0].index,'probas_OK'].as_matrix().tolist() 
         
         )
         
  # plot ROC
 #if ('perfomance_ROC' in vars(args) and vars(args)['perfomance_ROC']) or ('all' in vars(args) and vars(args)['all']):  
 label = 'perfomance_ROC'
 sklearn_roc_plot(cv,scores,y,prefix+label,[weights[0],weights[1]] if apply_weights else None)
 
 # plot precision 
 #if ('perfomance_precision' in vars(args) and vars(args)['perfomance_precision']) or ('all' in vars(args) and vars(args)['all']):  
 label = 'perfomance_precision'
 sklearn_precision_plot(cv,scores,y,prefix+label,[weights[0],weights[1]] if apply_weights else None)
        