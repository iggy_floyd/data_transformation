'''
	This is a Dataset Class designed to read data from json files and provide a DataFrame
'''

from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import pickle

class Dataset(object):
 '''
	Dataset -- to process data from json files
 '''

 def __init__(self,dataset=None,dataframe=None):
	''' nothing to do '''
	self.dataset = dataset
        self.dataframe = dataframe
        self.storename = None
        self.store = None

	pass

 def loadJson(self,filename):
	''' read json data from the filename '''

	self.dataset = read_fps_data(filename)

	return self

 def listStatuses(self):
	''' get list of possible statuses in the bookings '''
	if (not self.dataset): return set()

	else: return list(set([item['status'] for item in self.dataset]))

	pass

 def filterStatus(self,status): 
	''' performs initial filtering '''

	if (not self.dataset): return []
	a = [item for item in self.dataset if item['status'] == status]
	return a

 def createDataFrame(self,dataset=None):
        ''' create a pandas DataFrame from  an array-like dataset of dictionaries obtained from json file '''
        
        if (dataset is None):
            if (self.dataset): self.dataframe = pd.DataFrame.from_dict(self.dataset)
        else: self.dataframe = pd.DataFrame.from_dict(dataset)

        
        # some transformation of the data
        if (self.dataframe is not None):
            
            # 1) join 'params' of the bookings in json as a new columns in DataFrame
            df=self.dataframe['params'].to_dict()
            df = [ value for key, value in df.iteritems() ]            
            df = pd.DataFrame.from_dict(df)
            params_opt = list(df.columns.values)
            self.dataframe = pd.merge(self.dataframe,df,left_index=True, right_index=True, how='outer')
            # 2) remove unneeded 'params'
            self.dataframe = self.dataframe.drop('params',1)
            
            # 3) repeat 1) --2) for any category found in 'params': 'booker', 'payer' etc            
            for categ in params_opt:
                df=self.dataframe[categ].to_dict()
                df = [ value for key, value in df.iteritems() ]
                df = pd.DataFrame.from_dict(df)
                column_names = list(df.columns.values)                
                column_names = [ categ +'_'+ str(i) for i in column_names]
                df.columns =  column_names
                self.dataframe = pd.merge(self.dataframe,df,left_index=True, right_index=True, how='outer')
                self.dataframe = self.dataframe.drop(categ,1)

        return self.dataframe

 def storeDataFrame(self,storename,dataframe=None):
        ''' store datataframe '''
        
        if (dataframe is None): dataframe = self.dataframe 
        if (dataframe is None): return

        if (self.store is None):
            self.store =  pd.HDFStore(storename+'.h5')
            #self.store ={storename:self.dataframe} 
        
        self.store.put('df',self.dataframe)
        #with open(storename+'.pkl', "wb") as f:
        #    s= pickle.dump(self.store,f)
        return self.store #s

 
 def storeAttribute(self,storename,attributeName, attribute):
        ''' save attribute in the store '''
        
        if (dataframe is None): dataframe = self.dataframe 
        if (dataframe is None): return

        if (self.store is None):
            #s=self.storeDataFrame(storename)
            #self.store[attributeName]=attribute
            #with open(storename+'.pkl', "wb") as f:
            #    s= pickle.dump(self.store,f)
             
            self.store =  pd.HDFStore(storename+'.h5')
            self.store.put('df',self.dataframe)
            

        
        
        setattr(self.store.get_storer('df').attrs,attributeName,attribute)
        self.store.close()
        return #s


 def loadDataFrame(self,storename):
        ''' load datataframe '''
        
        
        if (self.store is None): return 
        
        return self.store.get('df')
        


def loadAttribute(self,attributeName):
        ''' load attribute '''
        
        
        if (self.store is None): return 
        if hasttr(self.store.get_storer('df').attrs,attributeName): return getattr(self.store.get_storer('df').attrs,attributeName)
        
        return 
        

    


if __name__ == '__main__':

 '''
 #Test 1: import data from a json file
 ds = Dataset()
 data=ds.loadJson("fps_data_1.json").dataset
 counter=0
 for item in data:
  print item
  counter+=1
  if counter>10: break

 # Test 2 # list the statuses
 statuses = ds.listStatuses()
 print statuses


 # Test 3: filtering
# data = ds.filterStatus(statuses[0])
 data = ds.filterStatus('CHECKED_NOT_OK')
 counter=0
 for item in data:
  print item
  counter+=1
  if counter>10: break



 # Test 4: booking period
 counter=0
 for item in data:
  timestamp=item['datetime']
  departureDate=item['params']['product']['departureDate']  
  print timestamp,  departureDate
  print TimeConvert.TimeConvert.difference_days(timestamp.split()[0],departureDate,format='%d.%m.%Y')  
  counter+=1
  if counter>10: break

 
 # Test 5: create a pandas DataFrame
 df=ds.createDataFrame()
 print df.head()
 #print df.tail()

 # Test 6: print 'payer' information from DataFrame
 payer_info=list(df.columns.values)
 print payer_info
 payer_info=filter(lambda x: 'payer' in x, payer_info)
 print df[payer_info].head()

# Test 6.2: print 'booker' information from DataFrame
 payer_info=list(df.columns.values)
 print payer_info
 payer_info=filter(lambda x: 'booker' in x, payer_info)
 print df[payer_info].head()


# Test 7: print 'product' information from DataFrame
 product_info=list(df.columns.values)
 product_info=filter(lambda x: 'product' in x, payer_info)
 print df[product_info].head()

# Test 8: Add booking period to DataFrame
#f = lambda x: TimeConvert.TimeConvert.difference_days(x['booker_bookingDate'].split()[0],x['product_departureDate'],format='%d.%m.%Y')  
f = lambda x: TimeConvert.TimeConvert.difference_days(x['datetime'].split()[0],x['product_departureDate'],format='%d.%m.%Y') 
df2 = df.apply(f,axis=1)
ds.dataframe['booking_period'] = df2
print ds.dataframe.head()

# Test 9: Add Levenshtein-Distance between family names of booker and payer 
# first, get levenshtein calculator and prepare it for use:
# git clone https://github.com/ztane/python-Levenshtein.git
# cd python-Levenshtein;
# python setup.py build
# cp build/lib.linux-i686-2.7/Levenshtein/_levenshtein.so  Levenshtein/
# following suggestion of http://stackoverflow.com/questions/10968309/how-to-import-python-module-from-so-file
# touch Levenshtein/_levenshtein.py and add def __bootstrap__(): ....
# mv Levenshtein/ ..
# rm -rf python-Levenshtein/
# we are ready to start 
import Levenshtein as pyLD
from   Levenshtein import StringMatcher
from  Levenshtein._levenshtein import *
print StringMatcher.StringMatcher(None,'Igor','Igor.2').distance() # test of the matcher

# we will use the similarity instead of the distance
def LD(x):
    if x['booker_lastName'] == None  or len(x['booker_lastName'] ) == 0 or pd.isnull(x['booker_lastName']) : return 1.0
    if x['payer_lastName']  == None  or len(x['payer_lastName'] ) == 0 or pd.isnull(x['payer_lastName']): return 1.0
    return 1.0-StringMatcher.StringMatcher(None,x['booker_lastName'],x['payer_lastName']).ratio() 

df2 = ds.dataframe.apply(LD,axis=1)
ds.dataframe['LD_booker_payer'] = df2
print ds.dataframe.head()


# Test 10: Add the number of travellers to the DataFrame 
def f2(x):
  a=dict((key,value) for key, value in x.iteritems() if 'traveller_' in key and value != None)
  return len(a)   

df2 = ds.dataframe.apply(f2,axis=1)
ds.dataframe['Number_of_travellers'] = df2
print ds.dataframe[['traveller_0','traveller_1','Number_of_travellers']].head()

# Test 11: Add the case when IPCountry  != BinCountry of the booker and payer to the DataFrame
from StringIO import StringIO
import prettytable    

# look at some data
df2=ds.dataframe[['booker_ipCountry','booker_country','payer_binCountry','booker_bookingDate']]
output = StringIO()
df2.to_csv(output)
output.seek(0)
pt = prettytable.from_csv(output)
print >> open("country.log","w"), pt   ## for debugging

def f3(x):
    if x['booker_ipCountry'] is None or pd.isnull(x['booker_ipCountry']): return np.nan
    if x['booker_country'] is None or pd.isnull(x['booker_country']) : return np.nan  
    return int(x['booker_ipCountry'] == x['booker_country'])
   
def f4(x):
    if x['booker_country'] is None  or pd.isnull(x['booker_country'])  : return np.nan
    if x['payer_binCountry'] is None or pd.isnull(x['payer_binCountry']) : return np.nan  
    return int(x['booker_country'] == x['payer_binCountry'])

def f5(x):
    if x['booker_ipCountry']  is None or pd.isnull(x['booker_ipCountry'])  : return np.nan
    if x['payer_binCountry']  is None  or pd.isnull(x['payer_binCountry']): return np.nan 
    return int(x['booker_ipCountry'] == x['payer_binCountry'])


df3 = ds.dataframe.apply(f3,axis=1)
df4 = ds.dataframe.apply(f4,axis=1)
df5 = ds.dataframe.apply(f5,axis=1)
ds.dataframe['booker_ipCountry_country'] = df3
ds.dataframe['booker_country_payer_binCountry'] = df4
ds.dataframe['booker_ipCountry_payer_binCountry'] = df5

# look at some data
df2=ds.dataframe[['booker_ipCountry','booker_country','payer_binCountry','booker_ipCountry_country','booker_country_payer_binCountry','booking_period']]
output = StringIO()
df2.to_csv(output)
output.seek(0)
pt = prettytable.from_csv(output)
print >> open("country2.log","w"), pt   ## for debugging

print df2.head(100)


# Test 12: Add route information like departureAirport -- arrivalAirport etc to the DataFrame
# look at some data
df2=ds.dataframe[['product_departureAirport','product_departureAirportGeo','product_arrivalAirport','product_arrivalAirportGeo']]
output = StringIO()
df2.to_csv(output)
output.seek(0)
pt = prettytable.from_csv(output)
print >> open("flights.log","w"), pt   ## for debugging


def f6(x):
    if x['product_departureAirport'] is None or pd.isnull(x['product_departureAirport']): return np.nan
    if x['product_arrivalAirport'] is None or pd.isnull(x['product_arrivalAirport']) : return np.nan  
    return str(x['product_departureAirport']) + '-' + str(x['product_arrivalAirport'])

df6 = ds.dataframe.apply(f6,axis=1)
ds.dataframe['route'] = df6
df2=ds.dataframe[['product_departureAirport','product_arrivalAirport','route']]
output = StringIO()
df2.to_csv(output)
output.seek(0)
pt = prettytable.from_csv(output)
print >> open("flights2.log","w"), pt   ## for debugging


# Test 13: store/load DataFrame and some attribute
ds = Dataset()
data=ds.loadJson("fps_data_1.json").dataset
df=ds.createDataFrame()
print df.describe()

ds.storeDataFrame('fps')
#df=ds.loadDataFrame()
#print df.describe()

# save some attribute
# get all values on 'product_departureAirport'
#departureAirport = df['product_departureAirport'].get_values()

# create transformer of labels to numeric represenation
#from sklearn import preprocessing
#enc=preprocessing.LabelEncoder().fit(departureAirport)

# store transformer
#df.storeAttribute('fps','LabelTransformer_'+'product_departureAirport', enc)

# load transformer and transform
#enc=df.loadAttribute('LabelTransformer_'+'product_departureAirport')
#print enc.transform(df['product_departureAirport'].get_values())
'''

