
'''
    How to get the probability density function from the histogram in python?
    
'''


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np

import scipy.interpolate as sci
import random
import matplotlib.pyplot as pl
import pandas as pd
import PDE
import projective_likelihood
import traceback
from sklearn.neighbors import KernelDensity
import seaborn as  sns


class histogram_interpolate2(object):
    
    def __init__(self,vals,typepde,nsteps):
        self.vals = vals
        self.typepde = typepde
        self.nsteps = nsteps
        

    @staticmethod
    def simpson(f,x,h):
        return (f(x) + 4*f(x + h/2) + f(x+h))/6.0
 
    @staticmethod
    def left_rect(f,x,h):
        return f(x)
    @staticmethod
    def integrate( f, a, b, steps, meth):
        import __builtin__
        h = float(b-a)/steps
        ival = h * __builtin__.sum([meth(f, a+i*h, h) for i in range(steps)])
        return ival  

    @staticmethod
    def integrate_scipy(f, a, b):
        from scipy import integrate
        return integrate.romberg(f,a,b)
    
    
    
    def takeClosest(self,myList, myNumber):
        """
        Assumes myList is sorted. Returns closest value to myNumber.

        If two numbers are equally close, return the smallest number.
        """
        from bisect import bisect_left
        pos = bisect_left(myList, myNumber)
        if pos == 0:
            return myList[0]
        if pos == len(myList):
            return myList[-1]
        before = myList[pos - 1]
        after = myList[pos]
        if after - myNumber < myNumber - before:
            return after
        else:
            return before  

    def pdf_at_point(self,x,x_grid,pdf):
        x=self.takeClosest(x_grid,x)
        return pdf[x_grid.index(x)]

    def kde(self,x, x_grid, bandwidth=0.5, **kwargs):

        """Kernel Density Estimation with Scikit-learn"""
        kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
        kde_skl.fit(x[:, np.newaxis])
        # score_samples() returns the log-likelihood of the samples
        log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
        return np.exp(log_pdf)


     # taken from http://toyoizumilab.brain.riken.jp/hideaki/res/code/sskernel.py
    def kde_adaptive(self, x, x_grid):
        import sskernel
        ret = sskernel.sskernel(x,tin=x_grid, bootstrap=True)
        return ret['y']

    def normalize(self,minx,maxx,x_grid,pdf):
        f = lambda x: self.pdf_at_point(x,x_grid,pdf)
        intgrl= histogram_interpolate2.integrate(f,minx,maxx,len(x_grid),histogram_interpolate2.simpson)
        return map(lambda x: float(x)/intgrl,pdf) if intgrl !=0 else pdf 


    def create_interpolation(self,X_grid=None):

         kdes={
             'adaptive': None,
             'small': None,
             'big': None,
             'standard': None
         }
         if self.typepde  in kdes:
            if X_grid is None:
                minx=min(self.vals)
                maxx=max(self.vals)
                if (minx == maxx): minx = float(minx)/self.nsteps
                X_grid= np.linspace(float(minx),float(maxx), self.nsteps)        

         try:
            kdes = {
                'adaptive': self.kde_adaptive(self.vals,X_grid),
                'small':    self.kde(self.vals,X_grid, bandwidth=0.05),  # a small bandwith
                'big':      self.kde(self.vals,X_grid, bandwidth=1.5),   #  a big bandwith,to decrease fluctuations
                'standard': self.kde(self.vals,X_grid, bandwidth=0.5)  # a standard  bandwith
            }               
            pdf = kdes[self.typepde] 
            self.X_grid = X_grid.tolist()
            self.pdf=self.normalize(min(self.vals),max(self.vals),self.X_grid,pdf)

         except Exception, err: 
            print traceback.format_exc()
            print "something wrong with kde"           
            return None

         return self

    def get_interploated(self,x):
        return self.pdf_at_point(x,self.X_grid,self.pdf)


    def plot(self,filename=None):
        sns.set_context("poster")
        xnew = np.linspace(min(self.X_grid ), max( self.X_grid ) , 100*len( self.X_grid ))   
        
        f = lambda x: self.pdf_at_point(x,self.X_grid,self.pdf)
        
        print 'sanity check: integral of the interpolated histogram',histogram_interpolate2.integrate(f,min(self.X_grid),max(self.X_grid),len(self.X_grid),histogram_interpolate2.simpson)
        fig=pl.figure(figsize=(15,8))                
        pl.hist(self.vals,bins=100,normed=True,label='an original histogram scaled to unity')        
        a=pl.plot(xnew,[ self.get_interploated(x) for x in xnew],label='an interpolated histogram')
        pl.legend(loc='upper right')
        
        if (filename): pl.savefig('plots/'+filename+'.png')
        pl.show()
        return  a


    def create_inverse_interpolation(self):
        self.x = np.cumsum(self.pdf)*1./np.sum(self.pdf) # normalization to unity
        self.y = np.array(np.array(self.X_grid)[range(len(self.x))])
        self.inverse_interpolate = sci.interp1d(self.x,self.y,kind='cubic')
        
        return self


    def generate_random(self,count):
        b = np.zeros(count)
        for i in range(count):
            u = random.uniform( min(self.x),max(self.x) )
            b[i] = self.inverse_interpolate( u )
        return b

    





if __name__ == '__main__':
    
    prefix = 'interpolate_histogram_'
    sns.set_context("poster")
    label = 'normal_dist'
    # Test 1 : create normal distribution and test it
    normal =  np.random.normal(size=1000)
    hi = histogram_interpolate2(normal,'adaptive',100).create_interpolation()
    hi.plot(filename=prefix+label)
    
    pl.show()

 
    # Test 2: generate a random value according to the histogram
    label='sample_norm'
    histo = hi.create_inverse_interpolation().generate_random(10000)    
    pl.hist(histo)
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
     # Test 3: create PLE and interpolate it
    import interpolate_histogram as ih
    ple = ih.ple_creator().create_ple().create_ple_pdf(50)
    
    label='ple_dist_ok'
    hi_ok=histogram_interpolate2(np.array(ple.ple_vals_ok),'adaptive',300).create_interpolation()
    ple_inter_ok=hi_ok.plot(filename=prefix+label)
    print "hi_ok is %.3f at %.3f"%(hi_ok.get_interploated(0.99),0.99)

    hi_notok=histogram_interpolate2(np.array(ple.ple_vals_notok),'adaptive',300).create_interpolation()
    label='ple_dist_notok'
    ple_inter_notok=hi_notok.plot(filename=prefix+label)
    print "hi_notok is %.3f at %.3f"%(hi_notok.get_interploated(0.99),0.99)

    # Test 4: generate PLE for OK and NOTOK statuses
    histo_ok = hi_ok.create_inverse_interpolation().generate_random(10000)
    histo_notok = hi_notok.create_inverse_interpolation().generate_random(10000)
    pl.hist(histo_ok,bins=ple.x_ok,color='b')
    pl.hist(histo_notok,bins=ple.x_notok,color='r')
    label='sample_ple'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
    # Test 4.2: plot interpolated PLE for OK and NOTOK statuses
    
    pl.setp(ple_inter_ok,color='b')
    pl.setp(ple_inter_notok,color='r')
    ax = pl.gca()
    #print ple_inter_ok
    ax.add_line(ple_inter_ok[0])
    ax.add_line(ple_inter_notok[0])
    label='interpolated_ple'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
    
    
     # Test 5: calculate the probabilities for 2-class ple
    twoclassple = ih.TwoClassPLEProbability(hi_ok,hi_notok)
    for ple_val in ple.x_ok:
        print 'ple value = %.3f, Proba(OK)=%.3f,Proba(NOTOK)=%.3f'%(ple_val,twoclassple.getProbability(ple_val)[0],twoclassple.getProbability(ple_val)[1])

    # Test 6: plot the ratio of probabilites in the range of ple output:[0.1,0.90]
    ples = np.linspace(0.01,0.99,100)
    ratios = np.array([twoclassple.getProbability(ple)[0] for ple in ples])/ np.array([twoclassple.getProbability(ple)[1] for ple in ples])
    pl.plot(ples,ratios)
    label='ple_two_class_prob_ratios'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
    # Test 7: plot the  probabilites in the range of ple output:[0.1,0.90]
    ples = np.linspace(0.01,0.99,100)
    ok_probas = np.array([twoclassple.getProbability(ple)[0] for ple in ples])
    notok_probas=np.array([twoclassple.getProbability(ple)[1] for ple in ples])
    pl.plot(ples,ok_probas,color='b')
    pl.plot(ples,notok_probas,color='r')
    label='ple_two_class_probas'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
    
    