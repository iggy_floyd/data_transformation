#!/usr/bin/env python
# -*- coding: Utf-8 -*-

###########################################
__app__ = "GetProxy"
__version__ = "0.4"
__author__ = "MatToufoutu"
# Inspired from darkc0de's ProxyHarvest
###########################################
#TODO: write elite list to file periodically
#TODO: try to limit the proxy amount to check, with equivalent results

import socket, re, urllib2, httplib
from os import system, getcwd, remove
from sys import exit as sysexit

timeout = 5
proxyfile = "proxylist.txt"

socket.setdefaulttimeout(timeout)
regex_ip = re.compile(r"(?:\d{1,3}\.){3}\d{1,3}")
regex_proxy = re.compile(r"(?:\d{1,3}\.){3}\d{1,3}:\d{1,5}")

def write2file(proxy):
    ofile = open(proxyfile, 'a')
    ofile.write(proxy+'\n')
    ofile.close()

class GetProxies:
    """Get/sort/check proxies collected from various websites"""
    def __init__(self):
        self.trueIP = self.getTrueIP()
        print("[+] True IP address: %s"% self.trueIP)
        self.codeenlist = []
        self.elitelist = []
        self.anonlist = []
        self.transplist = []
        self.otherlist = []

    def getTrueIP(self):
        """Get the machine's true IP address"""
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')]
	return opener.open('http://myip.dnsdynamic.org/').read()
#        return opener.open('http://www.whatismyip.com/automation/n09230945.asp').read()

    def check(self, proxy):
        """Check if <proxy> is anonymous"""
        re_pxtype = re.compile(r'You are using (?:<b>)?<font color="#[a-f0-9]{6}">(?: <b>)?(.+ proxy)(?:</b>)?</font>')
        re_ip = re.compile(r'IP detected: <font color="#[a-f0-9]{6}">((\d{1,3}\.){3}\d{1,3})')
        handler = urllib2.ProxyHandler({'http':proxy})
        opener = urllib2.build_opener(handler)
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')]
        try:
            html = opener.open('http://checker.samair.ru/').read()
            result = re.search(re_pxtype, html)
            if result:
                pxtype = result.groups()[0]
                ip = re.search(re_ip, html).groups()[0]
                if pxtype == 'high-anonymous (elite) proxy':
                    if ip == self.trueIP:
                        if __name__ == "__main__":
                            print(" - %s > TRANSPARENT" % proxy)
                        self.transplist.append(proxy)
                    else:
                        if __name__ == "__main__":
                            print(" - %s > ELITE" % proxy)
                        self.elitelist.append(proxy)
                        write2file(proxy)
                elif pxtype == 'anonymous proxy':
                    if __name__ == "__main__":
                        print(" - %s > ANONYMOUS" % proxy)
                    self.anonlist.append(proxy)
                else:
                    if "CoDeeN" in html:
                        if __name__ == "__main__":
                            print(" - %s > CODEEN" % proxy)
                        self.codeenlist.append(proxy.split(":")[0])
                    else:
                        if __name__ == "__main__":
                            print(" - %s > UNDEFINED" % proxy)
                        self.otherlist.append(proxy)
            opener.close()
        except KeyboardInterrupt:
            opener.close()
            sysexit(0)
        except:
            opener.close()
            pass

    def get_Codeen(self, retried=False):
        """Get a list of CoDeeN proxies"""
        try:
            if __name__ == "__main__":
                print(" - Gathering a list of known CoDeeN proxies...")
            opener = urllib2.build_opener()
            html = opener.open('http://fall.cs.princeton.edu/codeen/tabulator.cgi?table=table_all').read()
            proxies = re.findall(regex_ip, html)
            for proxy in proxies:
                self.codeenlist.append(proxy)
            opener.close()
            return self.codeenlist
        except (urllib2.URLError, socket.timeout):
            if retried:
                print(" - Site unavailable: Aborting")
                opener.close()
                return []
            else:
                print(" - Site unavailable: Retrying")
                opener.close()
                self.get_Codeen(True)

    def getfrom_Multi(self):
        """Get a list from various websites providing small proxy lists"""
        sites = []
        proxylist = []
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')]
        for site in sites:
            try:
                if __name__ == "__main__":
                    print(" - Gathering proxy list from %s" % site)
                html = opener.open(site).read()
                proxylist.extend(re.findall(regex_proxy, html))
            except (urllib2.URLError, socket.timeout):
                print(" - Site unavailable: Aborting")
                pass
        opener.close()
        return proxylist

    def getfrom_Ipadress(self, retried=False):
        """Get a proxy list from http://www.ip-adress.com/"""
        if __name__ == "__main__":
            print(" - Gathering proxy list from http://www.ip-adress.com/proxy_list/")
        proxylist = []
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')]
        try:
            html = opener.open('http://www.ip-adress.com/proxy_list/').read()
            proxylist.extend(re.findall(regex_proxy, html))
            opener.close()
            return proxylist
        except (urllib2.URLError, socket.timeout):
            if retried:
                print(" - Site unavailable: Aborting")
                opener.close()
                return []
            else:
                print(" - Site unavailable: Retrying")
                opener.close()
                self.getfrom_Ipadress(True)

    def getfrom_Samair(self, retried=False):
        """Get a proxy list from http://samair.ru/"""
        if __name__ == "__main__":
            print(" - Gathering proxy list from http://www.samair.ru/proxy/")
        hackregex1 = re.compile(r'(([a-z])=(\d);)+')
        hackregex2 = re.compile(r'((\d{1,3}\.){3}\d{1,3})<script type="text/javascript">document\.write\(":"((\+[a-z])+)\)')
        token, maxpages = 1, 20
        proxylist = []
        try:
            while token <= maxpages:
                opener = urllib2.build_opener()
                opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')]
                if token < 10:
                    html = opener.open('http://www.samair.ru/proxy/proxy-0%d.htm' % token).read()
                else:
                    html = opener.open('http://www.samair.ru/proxy/proxy-%d.htm' % token).read()
                hackdict = dict([x.split("=") for x in re.search(hackregex1, html).group().split(";")][:-1])
                iplist = [(x[0],x[2].split("+")[1:]) for x in re.findall(hackregex2, html)]
                for ip, letters in iplist:
                    ip += ":"
                    for letter in letters:
                        ip += hackdict[letter]
                    proxylist.append(ip)
                token += 1
                opener.close()
            return proxylist
        except (urllib2.URLError, socket.timeout):
            if retried:
                print(" - Site unavailable: Aborting")
                opener.close()
                return []
            else:
                print(" - Site unavailable: Retrying")
                opener.close()
                self.getfrom_Samair(True)

    def getfrom_Proxylist(self, retried=False):
        """Get a proxy list from http://www.proxylist.net"""
        if __name__ == "__main__":
            print(" - Gathering proxy list from http://www.proxylist.net")
        baseurl = "http://www.proxylist.net/list/0/0/3/0/"
        token, maxpages = 0, 15
        proxylist = []
        try:
            while token <= maxpages:
                opener = urllib2.build_opener()
                opener.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.5) Gecko/20091106 Shiretoko/3.5.5')]
                html = opener.open(baseurl+str(token)).read()
                proxylist.extend(re.findall(regex_proxy, html))
                token += 1
                opener.close()
            return proxylist
        except (urllib2.URLError, socket.timeout):
            if retried:
                print(" - Site unavailable: Aborting")
                opener.close()
                return []
            else:
                print(" - Site unavailable: Retrying")
                opener.close()
                self.getfrom_Proxylist(True)

    def getAllProxies(self):
        """Get a proxy list from all possible websites"""
        getfuncs = [self.getfrom_Multi, self.getfrom_Ipadress,
                    self.getfrom_Samair, self.getfrom_Proxylist]
        proxylist = []
        codeenlist = self.get_Codeen()
        for get_proxy in getfuncs:
            try:
                proxylist.extend(get_proxy())
            except TypeError:
                pass
        return proxylist


if __name__ == "__main__":
    try:
        system('clear')
        print(76*"#")
        print("\t\t\t[ GetProxy List Maker v%s ]\n" % __version__)
        print(76*"#"+"\n")
        collector = GetProxies()
        print("[+] Downloading proxy lists")
        proxylist1 = collector.getAllProxies()
        proxylist = []
        for proxy in proxylist1:
            if proxy not in proxylist:
                proxylist.append(proxy)
        for proxy in proxylist:
            if proxy in collector.codeenlist:
                proxylist.remove(proxy)
        print(" - Total proxies: %d\n[+] Checking proxies" % len(proxylist))
	
	f = open('proxy_list.txt', 'w')
	for item in proxylist:
	  print>>f, item
	sysexit(0)
        for proxy in proxylist:
            collector.check(proxy)
        print("[+] FINISHED !")
        print(" - Total elite proxies: %d" % len(collector.elitelist))
        print(" - List written to %s/proxylist.txt\n" % getcwd())
        sysexit(0)
    except KeyboardInterrupt:
        remove(proxyfile)
        sysexit(0)
