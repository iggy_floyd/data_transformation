from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import copy 
import TransformDataset
import Dataset
from sklearn import preprocessing
import matplotlib.pyplot as pl
import Cluster
from sklearn.neighbors import KernelDensity
import random


 # taken from  https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
def kde(x, x_grid, bandwidth=0.2, **kwargs):

    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)



def plot(x):

 for X in x:
	 X_grid= np.linspace(min(X),max(X), 1000)
	 pdf = kde(X,X_grid, bandwidth=0.2)
	 pl.plot(X_grid,pdf)
 pl.show()


if __name__ == '__main__':

 x = np.array([random.gauss(3,1) for _ in range(400)])
 y = np.array([random.gauss(4,2) for _ in range(400)])
 plot([x,y])
