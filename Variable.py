'''
	This is a Variable Class designed to store some columns from DataFrame
'''

from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import copy 
import TransformDataset
import Dataset
from sklearn import preprocessing


import matplotlib.pyplot as plt

class Variable(object):
 '''
	Variable -- to extract one column  from DataFrame
 '''

 def __init__(self,dataframe,name,Imputer=None,type=None,encoder=None):

        self.name = name
        self.dataframe = copy.deepcopy(dataframe[name])
        if (type is not None and not self.dataframe.isnull().any()):  #.astype(type)
            self.dataframe = pd.DataFrame(self.dataframe,dtype=type)[name]
        self.type = type
        self.imputer = Imputer
        self.encoder = encoder
        self.hist  = None
        self.bins  = None
        self.transformed = None

	pass



 def getStatistics(self):

        if (self.dataframe is None) : return None
        return (self.dataframe.min(),self.dataframe.max(),self.dataframe.mean(),self.dataframe.std())


 def toArray(self):

        if (self.dataframe is None) : return None
        return self.dataframe.as_matrix()


 def plot(self,norm=True,bins=100,Transformed=False):
        if (self.dataframe is None) : return 

        if (Transformed and self.transformed is  None ): return

        dataframe = self.dataframe
        if (Transformed): 
            dataframe=self.transformed
       
        self.hist, self.bins = np.histogram(dataframe.as_matrix(), normed=norm, bins=bins, density=True)
        if (norm): self.hist = self.hist  / self.hist.sum()

        plt.bar(self.bins[:-1], self.hist, width = 1)
        plt.xlim(min(self.bins), max(self.bins))
        plt.xlim(min(self.bins), max(self.bins))
        plt.xlabel(self.name)
        plt.ylabel('a.u.')
        plt.show() 

        return


 def fitEncoder(self,labels=None):
    
        if (self.dataframe.dtype == np.dtype('O')): 
            if (self.encoder is None): self.encoder=preprocessing.LabelEncoder()
            if (labels is None): labels=map(lambda x: x if not pd.isnull(x) else 'NaN', self.dataframe.get_values())
            self.encoder.fit(labels)
            self.transformed = copy.deepcopy(self.dataframe)
            self.transformed = pd.DataFrame(self.transformed)[self.name].apply(lambda x: self.encoder.transform(x))            
            
            

        return 
 


 def transform(self,value):
    
        if (self.encoder is not None):  

            return self.encoder.transform(value)
        
        return 
        
        


if __name__ == '__main__':


 # Test 1: check extraction of the variables
 ds = Dataset.Dataset()
 ds.loadJson("fps_data_1.json") 
 df= ds.createDataFrame()
 var = Variable(df,'booker_lastName')
 df = TransformDataset.TransformDataset(df).addBooking_period().addLevenshtein().addNumberTravellers().addCountryMatches().addRoute().dataframe
 #var = Variable(df,'booker_ipCountry_country')


 # Test 2: change the type and print summary
 var = Variable(df,'booker_ipCountry_country',type=np.dtype(np.int))
 print var.dataframe.describe()
 var = Variable(df,'booking_period',type='int')
 print var.dataframe.describe()

 # Test 3: get Statistics
 print "The variable {0} statistics: {1}".format(var.name,(var.getStatistics()))


 # Test 4: to test Label Transformers
 var = Variable(df,'route')
 var.fitEncoder()
 labels = var.toArray().tolist()
 #print "Standard Categorization of labels: {0} --> {1}".format(labels[0],  var.transform([labels[0]]))
 #print "Standard Categorization of labels: {0} --> {1}".format(labels[1],  var.transform([labels[1]]))
 

 #var = Variable(df,'route',None,None,encoder=preprocessing.LabelBinarizer())
 #var.fitEncoder()
 #labels = var.toArray().tolist()
 #print "LabelBinarizer of labels: {0} --> {1}".format(labels[0],  var.transform([labels[0]]))
 #print "LabelBinarizer of labels: {0} --> {1}".format(labels[1],  var.transform([labels[1]]))
 

 # Test 5: plot
 var.plot(Transformed=True)
 var = Variable(df,'booking_period',type='int')
 var.plot()