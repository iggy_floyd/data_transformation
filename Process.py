'''
	This is a Process Class designed to process the classifier chain
'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9


from read_json import *

from read_json import *
import TimeConvert
import pandas as pd
import numpy as np
import copy 
import TransformDataset
import Dataset
from sklearn import preprocessing
import matplotlib.pyplot as pl
import Cluster
from sklearn.neighbors import KernelDensity
import Normalizer
import sys
import pickle
import os.path
import logging


 


class Process(object):
    '''
	Process -- to process the whole chain
    '''

    

    def __init__(self,process_name=None):
        self.dataframe = None
        self.transformedframe = None	
        self.preprocessedframe = None	
        self.process_name = process_name if (process_name) and (len(process_name)>0) else 'fps_fraud_classification'
        self.preprocessing = None
        # list of interesting data columns
        self.tosave=[
                    'status',
                    'booker_ipCountry',
                    'booker_country',
                    'payer_binCountry',
                    'product_departureAirport',
                    'product_arrivalAirport',
                    'booker_email_country',
                    'booker_email_provider', 'booker_ipCountry_country', 'booker_country_payer_binCountry','booker_ipCountry_payer_binCountry',
                    'LD_booker_payer','booking_period','Number_of_travellers','isHit','score','route',
                    'product_price','is_zipcode','is_birthday','is_phone','is_email','booker_age','traveller_first_age',
                    'traveller_avg_age','traveller_total_age','LD_booker_first_traveller','LD_booker_all_travellers',  'product_airline',
                    'product_typeOfFlight',
                    'booker_valid_ip','booker_valid_addr','airport_dist','booker_depairport_dist','booker_arrairport_dist'
           ]


        # set-up logging
        LOG_FILENAME = 'history.log'
        logging.basicConfig(filename=LOG_FILENAME,
                            filemode='a',
                            format='+'*100 + '\n'+
                                   '|%(asctime)s|%(levelname)s|%(message)s \n' +
                                   '-'*100,
                            datefmt='%d.%m.%Y %H:%M:%S',
                            level=logging.DEBUG)
        # new levels of the logging                            
        logging.UPDATE = 15
        logging.BACKUP = 16
        logging.addLevelName(logging.UPDATE, "UPDATE")
        logging.addLevelName(logging.BACKUP, "BACKUP")
        logging.Logger.update = lambda inst, msg, *args, **kwargs: inst.log(logging.UPDATE, msg, *args, **kwargs)
        logging.update = lambda msg, *args, **kwargs: logging.log(logging.UPDATE, msg, *args, **kwargs)
        logging.Logger.backup = lambda inst, msg, *args, **kwargs: inst.log(logging.BACKUP, msg, *args, **kwargs)
        logging.backup = lambda msg, *args, **kwargs: logging.log(logging.BACKUP, msg, *args, **kwargs)
        pass


    def run_dataframe_creation(self,file_json=''):
        if self.dataframe is None:
            if (file_json) and (len(file_json)>0): 
                ds = Dataset.Dataset() # create dataset
                ds.loadJson(file_json)  # load data 
                self.dataframe =  ds.createDataFrame() # create dataframe
                if (self.dataframe is None): raise ValueError("Something wrong with dataframe")            
            df=self.dataframe
            df = df.drop(df[df.status == 'DENIED_PAYMENTERROR'].index) # drop not needed rows
            df = df.drop(df[df.status == 'DENIED_DOUBLE'].index) # drop not needed rows
            df = df.drop(df[df.status == 'DENIED_NOULA'].index) # drop not needed rows
            df = df.drop(df[df.status == 'OPEN'].index) # drop not needed rows
            df = df.drop(df[df.status == 'DENIED_OTHER'].index) # drop not needed rows

        
    def run_dataframe_transformation(self,file_json='',renew=False):

        if not(os.path.exists(self.process_name+'_transformed.csv')) or renew:

           if self.dataframe is None:
            if (file_json) and (len(file_json)>0): 
                ds = Dataset.Dataset() # create dataset
                ds.loadJson(file_json)  # load data 
                self.dataframe =  ds.createDataFrame() # create dataframe
                if (self.dataframe is None):  raise ValueError("Something wrong with dataframe")                    
           df=self.dataframe
           df = df.drop(df[df.status == 'DENIED_PAYMENTERROR'].index) # drop not needed rows
           df = df.drop(df[df.status == 'DENIED_DOUBLE'].index) # drop not needed rows
           df = df.drop(df[df.status == 'DENIED_NOULA'].index) # drop not needed rows
           df = df.drop(df[df.status == 'OPEN'].index) # drop not needed rows
           df = df.drop(df[df.status == 'DENIED_OTHER'].index) # drop not needed rows
           
                
            # transform dataframe
           print "Transformation starts"
           td = TransformDataset.TransformDataset(df)            
           td.addIsEmail().addIsPhone().addIsBithday().addIsZipCode().addBookerAge().addProductPrice()
           td.addFirstTravellerAge().addAverageTravellerAge()
           td.addTotalTravellerAge().addLevenshteinFirstTraveller()
           td.addLevenshteinAllTraveller() # some categories should be added before 'Imputer'
           td.addValidation()  # might be time consuming
           td.imputtingColumns([
                'booker_ipCountry', 
                'booker_country',
                'payer_binCountry',
                'product_departureAirport',
                'product_arrivalAirport',
                'product_airline',
                'product_typeOfFlight',
           ]).addBooking_period().addLevenshtein().addNumberTravellers().addCountryMatches().addRoute().addBookerEmailInformation()
           #repeate for missing values in email information
           td.imputtingColumns(['booker_email_country','booker_email_provider'])
           print "Modification has been finished"
           
           # save to file
           self.transformedframe =  pd.DataFrame(td.dataframe)[self.tosave]
           self.transformedframe.to_csv(self.process_name+'_transformed.csv', index=False)
        else:
           print "reading from ",self.process_name+'_transformed.csv',"...."
           self.transformedframe = pd.read_csv(self.process_name+'_transformed.csv',na_values=[' '],keep_default_na = False)
        
 

    def run_variable_preprocessing(self,processing, renew = False):
    
        if not(os.path.exists(self.process_name+'_preprocessed.csv')) or renew:
        
            if self.transformedframe is None: 
                raise ValueError("please to  'run_dataframe_transformation' first! ")
            
            # create a manager for clustering the data           
            #from StringIO import StringIO
            #import prettytable    
            #output = StringIO()
            #dataframe=self.transformedframe['booker_ipCountry']
            #dataframe.to_csv(output, sep='\t', encoding='utf-8')
            #output.seek(0)
            #pt = prettytable.from_csv(output)            
            #print >> open("cluster_data_orig.log","w"), pt   ## for debugging
            #print self.transformedframe['booker_ipCountry'].head()
            #print "Cluster"
            cluster = Cluster.Cluster(self.transformedframe,processing)
            cluster.preprocess()
            print "Preprocess has been finished"
            # save to files
            self.preprocessedframe = cluster.dataframe            
            self.preprocessedframe.to_csv(self.process_name+'_preprocessed.csv', index=False)
            self.preprocessing = processing
            with open(self.process_name+'_processing.pkl', 'wb') as f:
                pickle.dump(self.preprocessing,f)                        
        else:
            print "reading from ",self.process_name+'_preprocessed.csv',"...."
            self.preprocessedframe = pd.read_csv(self.process_name+'_preprocessed.csv')
            with open(self.process_name+'_processing.pkl', 'rb') as f:
                self.preprocessing=pickle.load(f)                        


    def write2log(self,type='BACKUP',msg='Some action was done'):
        ''' make the writting to the log file about some action,
            i.e backuping or updating 
        '''
        # writting to log file        
        getattr(logging,type.lower())(msg)
        
        pass


    def backup(self,msg,doBackupTransformed=True,doBackupPreProcessed=True):
        ''' do a backup of the dataframes '''
        import datetime
        import shutil
        
        # get a suffix for the backup file name
        suffix =  datetime.datetime.now().strftime("_%H_%M_%d_%m_%Y")
        # logging
        self.write2log(type='BACKUP',msg=msg)
        # backup
        try:
            if doBackupTransformed:        shutil.copy(self.process_name+'_transformed.csv', self.process_name+'_transformed'+suffix+'.csv.bak')
            if doBackupPreProcessed:        shutil.copy(self.process_name+'_preprocessed.csv', self.process_name+'_preprocessed'+suffix+'.csv.bak')
        except shutil.Error as e:
            print('Error: %s' % e)
        pass


    def update_transformed(self,log_msg,features=[],imputting_before=[],imputting_after=[], transformers=[]):
        ''' do an update of the transformed dataframe.
            preprocessed dataframe should simply renewed via `run_variable_preprocessing()` 
        '''

        # first, backup transformed dataframe
        self.write2log(type='UPDATE',msg=log_msg)
        self.backup(msg='Backup transformed dataframe before its updating...',doBackupTransformed=True,doBackupPreProcessed=False)

                
        
        print "Update starts"        
        td = TransformDataset.TransformDataset(self.dataframe)
        
        
        # imputting before, if it's needed
        if (len(imputting_before)>0):       td.imputtingColumns(imputting_before)
        # transform dataframe
        for transform in transformers:
            getattr(td,transform)()
        # imputting after, if it's needed
        if (len(imputting_after)>0):       td.imputtingColumns(imputting_after)        
        transformedframe =  pd.DataFrame(td.dataframe)[features]
        # adding transformed features
        for feature in features:
            self.transformedframe[feature] = transformedframe[feature] 
        
        # saving
        self.transformedframe.to_csv(self.process_name+'_transformed.csv', index=False)
        
        pass
    
    
    
        
    

if __name__ == '__main__':

 filename = sys.argv[1]  if len(sys.argv)>1 else "fps_data_3.json"
 renew =   bool(int(sys.argv[2])    if len(sys.argv)>2 else 0)
 redopreprocess =   bool(int(sys.argv[3])    if len(sys.argv)>3 else 0)
 update =   bool(int(sys.argv[4])    if len(sys.argv)>4 else 0)




 # Step 0: create Process Manager 
 manager=Process() 
 print "Process '%s' is started"%manager.process_name


 # Step 1: create a dataframe
 manager.run_dataframe_creation(filename)

 # Step 2: create  a transformed dataframe
 if (renew): print "Transformation of the DataFrame will be redone"
 manager.run_dataframe_transformation(filename,renew=renew)

 # Step 3: preprocessing of variables
 # create a processing pipeline
 if (redopreprocess): print "Preprocessing of the DataFrame will be redone"
 processing = [
    ('booker_ipCountry',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    ('product_departureAirport',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    ('product_arrivalAirport',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    ('booker_email_country',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer  
    ('booker_email_provider',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer    
    #('booker_email_provider',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), # sklearn transformers
    ('route',[Normalizer.NormalizerWrapper(type='normal')]), # my own transformer
    ('product_airline',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), 
    ('product_typeOfFlight',[preprocessing.LabelEncoder(),preprocessing.StandardScaler()]), 
 ]
 
 manager.run_variable_preprocessing(processing=processing,renew=redopreprocess)

 if (update):     
    print "Update of the transformed DataFrame will be redone"
    print "Instructions of the updating will be read from the file '%s'"%('update_transformed.py')
    execfile('update_transformed.py')
