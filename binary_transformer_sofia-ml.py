#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 15, 2015 3:23:37 PM$"



import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np

import pandas as pd
from sklearn import preprocessing
from sklearn import cross_validation 
#import Normalizer


class binary_transformer_sofia_ml(object):
    def __init__(self,df,features): 
     self.df = df
     self.features = features
     self.binary_enc = None
     self.binary_data=None
     self.target = None
     
    def label_encoders(self,input_variable_names):
        input_variable_types = map(lambda x: 'string' if 'object' in self.df[x].ftypes else self.df[x].ftypes,input_variable_names )
        self.labelencoders = {}
        for i,x in enumerate(input_variable_types):
        ## here transform string label to numeric labels
            if 'string' in x:
                transformer=preprocessing.LabelEncoder()
            else:    
                transformer=preprocessing.MinMaxScaler()
            values =  map(lambda x: x if x!=np.nan else 'NaN' ,self.df[input_variable_names[i]].get_values())
            self.labelencoders[input_variable_names[i]] =transformer
            self.labelencoders[input_variable_names[i]].fit(values)
            #if ( type(transformer)==type(preprocessing.LabelEncoder())): print input_variable_names[i],len(transformer.classes_)
            
            self.df[input_variable_names[i]] =  self.df[input_variable_names[i]].apply(lambda x: self.labelencoders[input_variable_names[i]].transform([x])[0])
        

        return self
 
    def transform_dataframe_binary(self,target):
        binary_train_col=[]   
        for feature in self.features:
            name_feature = feature[0]
            transformator = feature[1]
            if (name_feature in target):
                if transformator:
                    if ('function' in str(type(transformator))):                      
                        self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
          
                    elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):          
                        transformator.fit_transform(self.df[name_feature].as_matrix()) 
                        self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
            else: 
             binary_train_col +=[name_feature]

        self.label_encoders(binary_train_col)
        self.binary_enc = preprocessing.OneHotEncoder().fit(self.df[binary_train_col].as_matrix().tolist())
        self.binary_data = self.df[binary_train_col].as_matrix()
        self.binary_data=map(lambda x: list(x),self.binary_data)
        self.binary_data=self.binary_enc.transform(self.binary_data).toarray().tolist()
        self.binary_target = self.df[target].as_matrix()
        print 'binary data is ready'
        return self  

    
    def write_to_file_binary(self,fileName,_min,_max):
        
        dimensionality=len(self.binary_data[0]) +1
        print dimensionality    
        targets=np.array(self.binary_target[_min:_max])
        data=np.array(self.binary_data[_min:_max])
        
        # transform data
        transf_data=[]
        transf_targets=[]
        for j,elem in enumerate(data):
            _transf_data=[]
            transf_targets += str(int(targets[j]))
            for i,val in enumerate(elem): 
                if (val > 0) : _transf_data+=['%s:%s'%(str(i+i),str(int(val)))]
            transf_data += [_transf_data]
             
        data=np.array(transf_data)
        targets=np.array(transf_targets)
        
        # save data
        np.savetxt(fileName, np.c_[targets,data], delimiter=" ", fmt="%s",header=str(dimensionality),newline='\n', comments='# ') 
        
        return self

    def transform_and_save(self,fileName,data,targets):
            
            # transform data
            transf_data=[]
            for j,elem in enumerate(data):
                _transf_data=[]
                for i,val in enumerate(elem): 
                    if (val > 0) : _transf_data+=['%s:%s'%(str(i+1),str(int(val)))]
                transf_data += [_transf_data]
             
            
            data=np.array(transf_data)
            # save data
            #np.savetxt(fileName, np.c_[targets,data], delimiter=" ", fmt="%s",header=str(dimensionality),newline='\n', comments='# ') 
            np.savetxt(fileName, np.c_[targets,data], delimiter=" ", fmt="%s",newline='\n', comments='# ') 
    
    def save_test_train_to_file_binary(self,fileName_prefix,test_size=0.1):
        
        dimensionality=len(self.binary_data[0]) +1
        X_train,X_test,y_train, y_test = cross_validation.train_test_split(self.binary_data, self.binary_target, test_size=test_size, random_state=0)
         
        
 
        
        self.transform_and_save(fileName_prefix+'_train.txt',X_train,y_train)
        self.transform_and_save(fileName_prefix+'_test.txt',X_test,y_test)
        return self

if __name__ == "__main__":


 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else -1),
    ('LD_booker_payer',None),
    ('traveller_avg_age',None),  # !!!
    ('booker_valid_addr',None), # perhaps bad !!!
    ('booking_period',None), # !!!
    ('LD_booker_all_travellers',None), # test !!!
    ('LD_booker_first_traveller',None), # bad
    ('product_typeOfFlight',None), # perhaps bad !!!
    ('product_arrivalAirport',None), # !!!
    ('product_departureAirport',None), # !!!   
    ('booker_age',None), # bad 
    ('product_price',None), # bad 
    ('airport_dist',None),    
    
    ]
 binary_transformer=binary_transformer_sofia_ml(dataframe[ [feature[0] for feature in features] ],features)
 binary_transformer.transform_dataframe_binary('status')
# binary_transformer.write_to_file('test.txt',1,3)
# binary_transformer.save_test_train_to_file_binary('sofia_ml_model_binary',test_size=0.1)
