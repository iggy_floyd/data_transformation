#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Apr 14, 2015 12:02:30 PM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd

from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn import cross_validation    
import sys
import seaborn as  sns
from sklearn import preprocessing
from sklearn.metrics import classification_report
import Normalizer
import sklearn_roc_curves as skroc
import sklearn_optimizer
import time
import os.path
from sklearn.externals import joblib
import interpolate_histogram as ih
import binary_transformer as bt

import scipy
from sklearn.metrics import roc_curve, auc,precision_recall_curve


import dill
    

def run_dill_encoded(what):
        fun, args = dill.loads(what)
        return fun(*args)

def apply_async(pool, fun, args):        
        return pool.apply_async(run_dill_encoded, (dill.dumps((fun, args)),))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    



from multiprocessing import Pool, cpu_count

def applyParallel(dfGrouped, func):
        #p=Pool(cpu_count())
        p=Pool(process=2)
        ret_list = p.map(func, [(group,name) for name, group in dfGrouped])
        return pd.concat(ret_list)    


class nb_gaussian_twoclass_sklearn(object):
    
 def __init__(self,df,features):
     self.df = df
     self.features = features
     self.binary_transformer = None
     self.probability=False
     self.train_cols=None
     pass
    
 def plot_dataframe(self): 
     self.df.hist()
     pl.show()
     return
 
 def transform_dataframe(self):
    for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]         
         if transformator:
             if ('function' in str(type(transformator))):     
                 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator(x))
                 #self.df[name_feature].apply(lambda x: transformator(x))

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 print "class"
                 transformator.fit_transform(self.df[name_feature].as_matrix()) 
                 self.df[name_feature] = self.df[name_feature].apply(lambda x: transformator.transform([x])[0])
    return self  
 
 def fit(self,target,clf=None,probability=True):
     self.train_cols=[feature[0] for feature in self.features]
     self.train_cols.remove(target)
     self.probability=True

     start = time.time()     
     print'features to train:', self.train_cols
     self.clf = GaussianNB() if clf is None else clf
     self.clf.fit(self.df[self.train_cols].as_matrix().tolist(),self.df[target].as_matrix().tolist())     
     print 'fit is done using %.3f sec'%(time.time()-start)
      
     return self

 def validation(self,target,doSklearnAcc=True):
     

     print 'validation is started...'     
     def pred_accuracy(clf,X_test,y_test):
         print 'doing prediction...'
         pred=clf.predict(X_test)             
         print 'Prediction accuracy on the sample (of %d) : %.4f' %(len(y_test), (1 - (1. / len(y_test) * sum( pred != y_test ))))
         return  (1 - (1. / len(y_test) * sum( pred != y_test )))
     
     start = time.time()
     acc=pred_accuracy(self.clf,self.df[self.train_cols].as_matrix(),self.df[target].as_matrix())
     print 'validation is done using %.3f sec'%(time.time()-start)

     if (not doSklearnAcc): return (acc,-1.)
     # a sklearn validation tool
     _,X_test,_, y_test = cross_validation.train_test_split(self.df[self.train_cols].as_matrix(), self.df[target].as_matrix(), test_size=0.1, random_state=0)
     scores = cross_validation.cross_val_score(self.clf, X_test, y_test, cv=3, n_jobs=1,verbose=2)
     #scores = cross_validation.cross_val_score(self.clf, self.binary_transformer.binary_data, self.binary_transformer.binary_target, cv=3, n_jobs=1,verbose=2)
     print scores
     print("Accuracy of SVM with the sklearn validation tool: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
     return (acc,scores.mean())

 def validation_v2(self,X_test,y_test):             
	start = time.time()
        y_true, y_pred = y_test, self.clf.predict(X_test)
        print (classification_report(y_true, y_pred))
        print 'validation is done using %.3f sec'%(time.time()-start)

 def create_probas_pdf(self,target):

     def proba_maker(x,proba_provider):
         probas=proba_provider.clf.predict_proba([x[proba_provider.train_cols].as_matrix().tolist()])         
         return (probas.tolist()[0][1],probas.tolist()[0][0])


     self.df['probas'] = self.df.apply(lambda x: proba_maker(x,self)[0],axis=1)
     self.proba_vals_class1=self.df.loc[self.df[self.df[target] == 1].index,['probas']].as_matrix()
     self.proba_vals_class0=self.df.loc[self.df[self.df[target] == 0].index,['probas']].as_matrix()
     counts1, bins1 = np.histogram(self.proba_vals_class1, bins=50)
     counts0, bins0 = np.histogram(self.proba_vals_class0, bins=50)
     
     hi_1 = ih.histogram_interpolate(counts1,bins1).create_interpolation()
     hi_0 = ih.histogram_interpolate(counts0,bins0).create_interpolation()
     self.pdfs_provider = ih.TwoClassPLEProbability(hi_1,hi_0)
     
     return self


def sklearn_roc_plot(cv,scores,y,filename,weights=None):
 '''   Example  of ROC curve for the Classifier with help of the sklearn '''

 # assumed fraction of frauds in all dataset
 fraud_frac=0.01

 # plot ROC with help of sklearn
 mean_tpr = 0.0
 mean_fpr = np.linspace(0, 1, 100)
 for i, (train, test) in enumerate(cv):    
    
    # apply weights to the sample
    def applyweigt(arr,y):        
        return [ y[1] if x >0 else y[0] for x in arr ]
        
    weights = np.apply_along_axis(applyweigt, 0, y[test],[fraud_frac,1.-fraud_frac]) if (weights is None) else \
        np.apply_along_axis(applyweigt, 0, y[test],weights)

    if (i==0):   label="\n N(OK)= %d, \n N(NOT_OK) = %d "% ((1.-fraud_frac)*len(y[test][y[test]>0]),(fraud_frac)*len(y[test][y[test]==0]))
    else: label=""

    # Compute ROC curve and area the curve
    roc=skroc.sklearn_roc_curves(scores[test],y[test]).calculateROC(weights=weights).createPlotROC("PLE > cut",
        #label="fold %d, TP+FP = %d,FN+TN = %d "% (i,len(y[test][y[test]>0]),len(y[test][y[test]==0]))
         label=label
        )
    
    
    #calculate the mean ROC
    mean_tpr += scipy.interp(mean_fpr, roc.fpr, roc.tpr)
    mean_tpr[0] = 0.0

    
   
    # plot discriminant values
    if (i>0): continue
    for k,x in enumerate(roc.thresholds.tolist()):
        if k%(len(roc.thresholds.tolist())/10 if len(roc.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([roc.fpr[k]],[roc.tpr[k]],'o',color='r')
            pl.text(roc.fpr[k]+0.01,roc.tpr[k]-0.01,'%3.2f'%x,color='r')
            #print "threshold = %f, tpr= %f, fpr = %f"%(x,roc.tpr[k],roc.fpr[k])

 

 # plot mean ROC
 mean_tpr /= len(cv)
 mean_tpr[-1] = 1.0
 mean_auc = auc(mean_fpr, mean_tpr)
 pl.plot(mean_fpr, mean_tpr, 'k--',
         label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

 # plot random ROC
 pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')

 pl.legend(loc="lower right")
 pl.savefig('plots/'+filename+'.png')
 pl.show()
 return 


def sklearn_precision_plot(cv,scores,y,filename,weights=None):
 ''' plot recall vs precision plot '''

# assumed fraction of frauds in all dataset
 fraud_frac=0.01 
 for i, (train, test) in enumerate(cv):    

    # Compute Precisions curve
    def applyweigt(arr,y):        
        return [ y[1] if x >0 else y[0] for x in arr ]
    weights = np.apply_along_axis(applyweigt, 0, y[test],[fraud_frac,1.-fraud_frac]) if (weights is None) else \
        np.apply_along_axis(applyweigt, 0, y[test],weights)


    prec=skroc.sklearn_roc_curves(scores[test],y[test]).calculatePrecision(weights=weights).createPlotPrecision("PLE > cut",
        label="fold %d"% (i)         
        )
    # plot discriminant values
    if (not (i==1)): continue
    for k,x in enumerate(prec.thresholds.tolist()):
        if k%(len(prec.thresholds.tolist())/10 if len(prec.thresholds.tolist()) >10 else 1) == 0:  # plot each 10th
            pl.plot([prec.recall[k]],[prec.prec[k]],'o',color='r')
            pl.text(prec.recall[k]+0.01,prec.prec[k]-0.01,'%3.2f'%x,color='r')
 
 pl.savefig('plots/'+filename+'.png') 
 pl.show()
 
 return 



if __name__ == "__main__":
    
    
  # read command line options and arguments
 import argparse
 parser = argparse.ArgumentParser(description='Two-Class SVM Regressor')
 

 parser.add_argument('-all','--all', help='make all plots',default=None, required=False, action='store_true')
 args = parser.parse_args()

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 print len(dataframe)
 features = [
    ('status', lambda x: 1 if x=='CHECKED_OK' else 0),
    #('LD_booker_payer',None),
    #('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    #('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    #('booking_period',preprocessing.MinMaxScaler()), # !!!
    #('LD_booker_all_travellers',None), # test !!!
    #('LD_booker_first_traveller',None), # bad
    #('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    #('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    #('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    #('booker_age',preprocessing.StandardScaler()), # bad 
    #('product_price',preprocessing.MinMaxScaler()), # bad 
    #('airport_dist',preprocessing.MinMaxScaler()),    

    
    #('LD_booker_payer',None),
    ('LD_booker_payer',None),
    ('traveller_avg_age',Normalizer.NormalizerWrapper(type='normal')),  # !!!
    ('booker_valid_addr',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('booking_period',preprocessing.MinMaxScaler()), # !!!
    #('LD_booker_all_travellers',None), # test !!!
    #('LD_booker_first_traveller',None), # bad
    ('LD_booker_all_travellers',Normalizer.NormalizerWrapper(type='normal')), # test !!!
    ('LD_booker_first_traveller',Normalizer.NormalizerWrapper(type='normal')), # bad
    ('product_typeOfFlight',Normalizer.NormalizerWrapper(type='normal')), # perhaps bad !!!
    ('product_arrivalAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!
    ('product_departureAirport',Normalizer.NormalizerWrapper(type='normal')), # !!!

    
    ]
    
 
 
 renew = True
 doValidation=True
 
 
 if not(os.path.exists('nb_gaussian_twoclass_sklearn_saved.pkl')) or renew:
 # Class doing C-Support Vector Classification
    nb=nb_gaussian_twoclass_sklearn(dataframe[ [feature[0] for feature in features] ],features)   
 # plot numerical data
   #nb.plot_dataframe()

 # transform to numerical data and plot again
    nb.transform_dataframe()
    
 #svc.plot_dataframe()
 
 # fit the model 
    nb.fit('status',probability=False)
 
 
 
 
 # print the validation information
    #acc1,acc2=nb.validation('status',doSklearnAcc=False) if doValidation else  (None,None)
    acc1,acc2=nb.validation('status',doSklearnAcc=True) if doValidation else  (None,None)
    start=time.time()
    print 'dumping NB...'
    #joblib.dump(svc,'svm_twoclass_sklearn_saved.pkl')
    with open('nb_gaussian_twoclass_sklearn_saved.pkl', 'wb') as fid:
        dill.dump(nb, fid)   
    print 'NB has been dumped... with time %.3f'%( time.time()-start)
    
 else:
    start=time.time()
    print 'loading is  started..'
    #svc = joblib.load('svm_twoclass_sklearn_saved.pkl')
    with open('nb_bernoulli_twoclass_sklearn_saved.pkl', 'rb') as fid:
       nb=dill.load(fid)   
    print 'NB has been loaded... with time %.3f'%( time.time()-start)
    print 'start valiadtion of version 2...'
    _,X_test,_, y_test = cross_validation.train_test_split(nb.df[nb.train_cols].as_matrix(), nb.df['status'].as_matrix(), test_size=0.1, random_state=0)    
    if (doValidation):  nb.validation_v2(X_test,y_test)
    
 prefix = 'NB_gaussian_sklearn_twoclass_'
 
 nb.probability = True
 
 if (nb.probability):
    nb.create_probas_pdf('status')
 
 # create probabilites: works only when probas are switched on, i.e. svc.fit('status',probability=True)
    def proba_maker(x,proba_provider):
         probas=proba_provider.clf.predict_proba([x[proba_provider.train_cols].as_matrix().tolist()])         
         return (probas.tolist()[0][1],probas.tolist()[0][0])

 
 
    nb.df['probas_OK'] = nb.df.apply(lambda x: proba_maker(x,nb)[0],axis=1)
    nb.df['probas_NOTOK'] = nb.df.apply(lambda x: proba_maker(x,nb)[1],axis=1)
    

      # plot probabilites
    sns.set_context("poster")
    
    label = 'probas_all_distr'
    b, g, r, p = sns.color_palette("muted", 4)
 
 
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK for OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK for OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK for NOT_OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK for NOT_OK',hist=False, axlabel='probability') 
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
 
  
 # plot probabilites
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_OK']].as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_OK']].as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_ok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 # plot probabilites
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_NOTOK']].as_matrix(),color=g,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_NOTOK']].as_matrix(),color=p,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_notok_distr'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
    
    def proba_maker_v2(x,proba_provider):
     return (proba_provider.getProbability(x['probas'])[0],proba_provider.getProbability(x['probas'])[1])
     
 
    nb.df['probas_OK_v2'] = nb.df.apply(lambda x: proba_maker_v2(x,nb.pdfs_provider)[0],axis=1)
    nb.df['probas_NOTOK_v2'] = nb.df.apply(lambda x: proba_maker_v2(x,nb.pdfs_provider)[1],axis=1)
 
    label = 'probas_all_distr_v2'
    
 
 
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_OK_v2']].as_matrix(),color=b,label='CHECKED_OK for OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_OK_v2']].as_matrix(),color=r,label='CHECKED_NOT_OK for OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_NOTOK_v2']].as_matrix(),color=g,label='CHECKED_OK for NOT_OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_NOTOK_v2']].as_matrix(),color=p,label='CHECKED_NOT_OK for NOT_OK',hist=False, axlabel='probability') 
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
     
 # plot probabilites
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_OK_v2']].as_matrix(),color=b,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_OK_v2']].as_matrix(),color=r,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_ok_distr_v2'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()

 # plot probabilites
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 1].index,['probas_NOTOK_v2']].as_matrix(),color=g,label='CHECKED_OK',hist=False, axlabel='probability')
    sns.distplot(nb.df.loc[nb.df[nb.df['status'] == 0].index,['probas_NOTOK_v2']].as_matrix(),color=p,label='CHECKED_NOT_OK',hist=False, axlabel='probability')
    label = 'probas_notok_distr_v2'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show() 

   
 
 # make a classifier
 def nb_classifier(proba_provider,x):     
      predict=proba_provider.clf.predict([x[proba_provider.train_cols].as_matrix().tolist()])    
      return int(predict[0]> 0)
     
      
  
 # perform predictions
 nb.df['predict'] = nb.df.apply(lambda x: nb_classifier(nb,x),axis=1)
 

 
 # calculate weights (needed for correct type-1/type-2 error results)
 weights = {
        0:float(len(nb.df.loc[nb.df[nb.df['status'] == 1].index,['status']]))/float(len(nb.df.loc[nb.df[nb.df['status'] == 0].index,['status']])),
        1:1
    }

  # estimate perfomance: type1/ type2 errors etc
 pos = nb.df.loc[nb.df[nb.df['predict'] == 1].index,['status']]
 true_pos = pos.loc[pos[pos['status'] == 1].index]
 false_neg = pos.loc[pos[pos['status'] == 0].index]

 neg = nb.df.loc[nb.df[nb.df['predict'] == 0].index,['status']]
 true_neg = neg.loc[neg[neg['status'] == 0].index]
 false_pos = neg.loc[neg[neg['status'] == 1].index]

 
 if (False): apply_weights=True
 #if ('weights' in vars(args) and vars(args)['weights']):   apply_weights=True
 else:   apply_weights=False
 
 
 if (not apply_weights):
    print "found rates", {    'true_pos':len(true_pos),
            'false_pos':len(false_pos),
            'true_neg':len(true_neg),
            'false_neg':len(false_neg),
 
        }

    performance = {
 
        'prec':float( len(true_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'type1':float( len(false_pos) )/float(  len(true_pos) +   len(false_pos) ),
        'tpr':float( len(true_pos) )/float(  len(true_pos) +   len(false_neg) ),
        'npv':float( len(true_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'type2':float( len(false_neg) )/float(  len(true_neg) +   len(false_neg) ),
        'fpr':float( len(false_pos) )/float(  len(true_neg) +   len(false_pos) ),
 
    }
 else:
     print "found rates", {    'true_pos':len(true_pos)*weights[1],
            'false_pos':len(false_pos)*weights[1],
            'true_neg':len(true_neg)*weights[0],
            'false_neg':len(false_neg)*weights[0],
 
        }

     performance = {
 
        'prec':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'type1':float( len(false_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_pos)*weights[1] ),
        'tpr':float( len(true_pos)*weights[1] )/float(  len(true_pos)*weights[1] +   len(false_neg)*weights[0] ),
        'npv':float( len(true_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'type2':float( len(false_neg)*weights[0] )/float(  len(true_neg)*weights[0] +   len(false_neg)*weights[0] ),
        'fpr':float( len(false_pos)*weights[1] )/float(  len(true_neg)*weights[0] +   len(false_pos)*weights[1] ),
 
    }
 print performance
 #if ('perfomance_diagram' in vars(args) and vars(args)['perfomance_diagram']) or ('all' in vars(args) and vars(args)['all']):  
 sns.barplot(np.array(performance.keys()),np.array(performance.values()),ci=None, palette="Paired", hline=.1)
 label = 'perfomance'
 pl.savefig('plots/'+prefix+label+'.png')
 pl.show()


 # make some performance plot: ROC 
 vals_ok=nb.df.loc[nb.df[nb.df['status'] == 1].index,nb.train_cols].as_matrix().tolist()
 vals_notok =nb.df.loc[nb.df[nb.df['status'] == 0].index,nb.train_cols].as_matrix().tolist()
 
 
  # create a test sample
 vals =  np.array(vals_ok + vals_notok)
 y = np.array([ 1 if i<len(vals_ok) else 0   for i, val in enumerate(vals)]) 
 cv = StratifiedKFold(y, n_folds=5) # cross-validation  tool
 scores = np.array(
         #nb.df.loc[nb.df[nb.df['status'] == 1].index,'probas_OK_v2'].as_matrix().tolist() +
         #nb.df.loc[nb.df[nb.df['status'] == 0].index,'probas_OK_v2'].as_matrix().tolist() 
         nb.df.loc[nb.df[nb.df['status'] == 1].index,'probas_OK'].as_matrix().tolist() +
         nb.df.loc[nb.df[nb.df['status'] == 0].index,'probas_OK'].as_matrix().tolist() 
         
         )
 
 
 # plot ROC
 #if ('perfomance_ROC' in vars(args) and vars(args)['perfomance_ROC']) or ('all' in vars(args) and vars(args)['all']):  
 label = 'perfomance_ROC'
 sklearn_roc_plot(cv,scores,y,prefix+label,[weights[0],weights[1]] if apply_weights else None)
 
 # plot precision 
 #if ('perfomance_precision' in vars(args) and vars(args)['perfomance_precision']) or ('all' in vars(args) and vars(args)['all']):  
 label = 'perfomance_precision'
 sklearn_precision_plot(cv,scores,y,prefix+label,[weights[0],weights[1]] if apply_weights else None)
 
